//
//  notifyvisitors.h
//  notifyvisitors
//
//  Created by Siddharth Gupta on 22/01/16.
//  Copyright © 2016 notifyvisitors. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AdSupport/AdSupport.h>
#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
typedef void(^nvCompletion)(BOOL);
//extern void nv_SetDebug(BOOL nvdebug);
extern NSString * NVInAppViewConroller;

#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
#import <UserNotifications/UserNotifications.h>
@interface notifyvisitors : NSObject <UNUserNotificationCenterDelegate>

//<< iOS 10 Push notification delegate and service extension methods


+(NSDictionary *)GetmediaInfo:(NSDictionary *)userInfo;
+(void)willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler;
+(void)didReceiveNotificationResponse:(UNNotificationResponse *)response;
+(NSMutableDictionary *)PushNotificationActionDataFromResponse:(UNNotificationResponse *)response AutoRedirectOtherApps:(BOOL)autoRedirect;
+(void)GetAttachmentDataForFileUrl:(NSString *)urlString withFileType:(NSString *)type completionHandler:(void(^)(UNNotificationAttachment *))completionHandler;
+(void)didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void(^)(UIBackgroundFetchResult))completionHandler;
#else


// iOS 10 Push notification delegate and service extension methods>>


@interface notifyvisitors : NSObject
#endif

/* <<<  AppDelegate Mehods to Inisilize sdk    */


+(void)Initialize:(NSString *)nvMode;
+(void)RegisterPushWithDelegate:(id)delegate App:(UIApplication *)application launchOptions:(NSDictionary *)launchOptions;
+(void)DidRegisteredNotification:(UIApplication *)application deviceToken:(NSData *)deviceToken;
+(void)didReceiveRemoteNotificationWithUserInfofor_iOS7orBelow:(NSDictionary *)userInfo;
+(void)didReceiveRemoteNotificationWithUserInfo:(NSDictionary *)userInfo;
/*                       OR                        */
+(NSMutableDictionary *)PushNotificationActionDataFromUserInfo:(NSDictionary *)userinfo;

// <<<  Deep linking

+(void)OpenUrlWithApplication:(UIApplication *)application Url:(NSURL *)url;
/*                       OR                        */
+(NSMutableDictionary*)OpenUrlGetDataWithApplication:(UIApplication *)application Url:(NSURL *)url;

//  Deep linking  >>>


// AppDelegate Mehods to Inisilize sdk >>>


+(void)Show:(NSMutableDictionary *)UserTokens CustomRule:(NSMutableDictionary *)customRule;
+(void)UserIdentifier:(NSString *)userID UserParams:(NSMutableDictionary *)UserParams;
+(void)checkSetupComplete:(NSTimer *)timer;
+(void)scrollViewDidScroll:(UIScrollView *)scrollView;
+(BOOL)isScrolled:(NSString *)NotificationID;
+(void)trackEvents:(NSString *)event_name Attributes:(NSMutableDictionary *)attributes lifetimeValue:(NSString *)ltv Scope:(int)scope;

+(void)schedulePushNotificationwithNotificationID:(NSString *)NID Tag:(NSString *)tag TimeinSecond:(NSString *)time Title:(NSString *)title  Message:(NSString *)message URL:(NSString *)url  Icon:(NSString *)icon;

+(void)checkFetchClickComplete:(NSTimer *)timer;
//+(NSMutableDictionary *)GetNotifyvisitorsPushNotificationData:(UIApplication *)application window:(UIWindow*)window Notification:(NSDictionary *)userInfo;
+(void)NotifyVisitorsNotificationCentre;
+(NSInteger)GetUnreadPushNotification;
+(UIViewController*) topMostController;
+(NSMutableArray *)GetNotificationCentreData;
+(void)NotifyVisitorsGeofencing;
+(void)NotifyVisitorsGeofencingReceivedNotificationWithApplication:(UIApplication *)application window:(UIWindow *)window didReceiveGeofencingNotification:(UILocalNotification *)notification;
+(void)HandleLocalNotifications:(UILocalNotification *)notification;

+(void)NotifyVisitorsGeofencingApplicationDidEnterBackground;
+(void)NotifyVisitorsGeofencingapplicationDidBecomeActive;
+(void)DismissAllNotifyvisitorsInAppNotifications;
+(void)StopInAppNotifications;
@end
