/**
 * Copyright (c) 2015-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */

#import "AppDelegate.h"

#import <React/RCTBundleURLProvider.h>
#import <React/RCTRootView.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import "notifyvisitors.h"
#import "SplashScreen.h" 

//@import PayooSDK;
//@import PayooServiceSDK;


// PAYU SDK Required Properties
static NSString * const merchantID = @"746";
static NSString * const secretKey = @"8a77de405da7967d5af5113c73fe51d8";

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
  
  NSString *nvMode = nil;
  #if DEBUG
    nvMode = @"debug";
  #else
    nvMode = @"live";
  #endif
  
  [notifyvisitors Initialize:nvMode];
  
  [notifyvisitors RegisterPushWithDelegate:self App:application launchOptions:launchOptions];
   
  
  
  NSURL *jsCodeLocation;
  
  jsCodeLocation = [[RCTBundleURLProvider sharedSettings] jsBundleURLForBundleRoot:@"index" fallbackResource:nil];
  
  RCTRootView *rootView = [[RCTRootView alloc] initWithBundleURL:jsCodeLocation
                                                      moduleName:@"NativeRenderingEngine"
                                               initialProperties:nil
                                                   launchOptions:launchOptions];
  rootView.backgroundColor = [[UIColor alloc] initWithRed:1.0f green:1.0f blue:1.0f alpha:1];
  [[FBSDKApplicationDelegate sharedInstance] application:application
                           didFinishLaunchingWithOptions:launchOptions];
  self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
  UIViewController *rootViewController = [UIViewController new];
  rootViewController.view = rootView;
  self.window.rootViewController = rootViewController;
  [self.window makeKeyAndVisible];
  
  // Payoo SDK configuration
  //[PayooServiceSDKConfiguration setWithMerchantId:merchantID secretKey:secretKey];
  //[PayooServiceSDKConfiguration setWithEnvironment:PayooServiceEnvironmentDevelopment];


  [Fabric with:@[[Crashlytics class]]];

  [SplashScreen show];
  return YES;
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
            options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options {
  
  BOOL handled = [[FBSDKApplicationDelegate sharedInstance] application:application
                                                                openURL:url
                                                      sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                                                             annotation:options[UIApplicationOpenURLOptionsAnnotationKey]
                  ];
  
  //BOOL payuSDKHandled = [Payoo application:application open: url options:options];
  
  return handled;
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
  [FBSDKAppEvents activateApp];
}

-(void)application: (UIApplication*)
application didRegisterForRemoteNotificationsWithDeviceToken: (NSData *)deviceToken  {
  
  NSString *deviceTokenString = [[[[deviceToken description]
                                    stringByReplacingOccurrencesOfString: @"<" withString: @""]
                                   stringByReplacingOccurrencesOfString: @">" withString: @""]
                                  stringByReplacingOccurrencesOfString: @" " withString: @""];
  
  
  [[NSUserDefaults standardUserDefaults] setObject:deviceTokenString forKey:@"deviceToken"];
  [[NSUserDefaults standardUserDefaults] synchronize];
  
  NSString *deviceID =  [[NSUserDefaults standardUserDefaults] stringForKey:@"deviceToken"];
  NSLog(@"%@",deviceID);

  
  [notifyvisitors DidRegisteredNotification:application deviceToken:deviceToken];
  
}
-(void)application: (UIApplication*)application didFailToRegisterForRemoteNotificationsWithError: (NSError *)error {
 
  NSLog(@"Error:%@",error);  }
-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo                 {
  [notifyvisitors didReceiveRemoteNotificationWithUserInfo:userInfo];
}

-(BOOL)application:(UIApplication*)application openURL:(NSURL*)url sourceApplication:(NSString*)sourceApplication annotation:(id)annotation {
  [notifyvisitors OpenUrlWithApplication:application Url:url];
    return YES;
  
}

# pragma mark UNNotificationCenter Delegate Methods

- (void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler {
  
  [notifyvisitors willPresentNotification:notification withCompletionHandler:completionHandler];
}
- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)())completionHandler {
  
  [notifyvisitors didReceiveNotificationResponse:response];
  
}
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
  
  [notifyvisitors didReceiveRemoteNotification:userInfo fetchCompletionHandler:completionHandler];
}


@end
