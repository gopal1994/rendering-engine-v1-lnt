/**
 *
 * Login
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';

import injectSaga from '../../utils/injectSaga';
import saga from './saga';

import { makeSelectAppDetails, makeSelectUserDetails, makeSelectUserLanguage } from '../../containers/AppDetails/selectors';

import * as appActions from '../../containers/AppDetails/actions';

import LoginContainer from './LoginContainer';

export class Login extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (

      <LoginContainer {...this.props} />

    );
  }
}

Login.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  appDetails: makeSelectAppDetails(),
  userDetails: makeSelectUserDetails(),
  userLanguage: makeSelectUserLanguage(),
});


function mapDispatchToProps(dispatch) {
  return {
    getRequest: (reqObject) => dispatch(appActions.getRequest(reqObject)),
    postRequest: (reqObject) => dispatch(appActions.postRequest(reqObject)),
    setUserDetails: (data) => dispatch(appActions.setUserDetails(data)),
    dispatch,
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);
const withSaga = injectSaga({ key: 'login', saga });

export default compose(
  withSaga,
  withConnect,
)(Login);
