import { colors, fonts, sizes, lineHeight } from '../../configs/styleVars';

export const wrapperStyle = { paddingLeft: sizes.appPaddingLeft, paddingRight: sizes.appPaddingRight, justifyContent: 'flex-start', paddingBottom: 20, backgroundColor: colors.white, flex: 1 };

export const headerStyle = { color: colors.headerColor, marginTop: 40, marginBottom: 20, fontSize: fonts.mainHeader, ...fonts.getFontFamilyWeight('bold') };

export const hintStyle = { marginTop: 15, marginBottom: 15, textAlign: 'center', lineHeight: 28 };

export const checkboxWrapper = { flexDirection: 'row', flex: 1, alignContent: 'flex-start' };

export const checkboxTextWrapper = { flexDirection: 'row', flex: 1, alignContent: 'flex-start', flexWrap: 'wrap', marginVertical: 13, lineHeight: lineHeight.body };

export const tncLink = { color: colors.labelColor, textDecorationLine: 'underline' };
