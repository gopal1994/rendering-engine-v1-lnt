import appVars from '../../configs/styleVars';
import styled from 'styled-components';
const colors = appVars.colors,
  sizes = appVars.sizes;

const LoginWrapper = styled.div`
  background: #FFF;
  margin: 30px auto;
  box-shadow: 0 0 10px rgba(0,0,0,0.5);
  padding: 50px;
  margin-top: 70px;
`;

export default LoginWrapper;
