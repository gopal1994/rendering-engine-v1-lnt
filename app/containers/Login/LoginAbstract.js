/**
 *
 * Login
 *
 */

import React from 'react';
import * as validationFns from '../../utilities/validations/validationFns';

export default class LoginAbstract extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);
    var token = window.location.hash.substr(1);
    if(token){
      this.state = {
        isMobileNumberValid: true,
        invalidMobileError: null,
        mobileNo: null,
        otp: null,
        checked: false,
        showTermsAndConditions: false,
        otpGenerated: false,
        isValidOtp: false,
        isTokenFlow:true,
        token: token
      };
    }else{
      this.state = {
        isMobileNumberValid: true,
        invalidMobileError: null,
        mobileNo: null,
        otp: null,
        checked: false,
        showTermsAndConditions: false,
        otpGenerated: false,
        isValidOtp: false,
      };
    }

    if(token){
      //call generate otp for token api
      this.generateTokenOtp();
    }
    console.log(this.state);
    this.handleMobileChange = this.handleMobileChange.bind(this);
  }

  checkLoginValidation = (value) => {
    const isValidObj = validationFns.getValidationObj('mobileNumber', value);
    // console.log('isValidObj: ', isValidObj);
    this.setState({ isMobileNumberValid: isValidObj.isValid });
    this.setState({ invalidMobileError: isValidObj.errorMessage });
  }

  checkOtpValidation = (value) => {
    const isValidObj = validationFns.getValidationObj('otp', value);
    // console.log(isValidObj);
    this.setState({ isValidOtp: isValidObj.isValid });
    this.setState({ invalidOtpError: isValidObj.errorMessage });
  }

  handleMobileChange = (e) => {
    this.setState({ mobileNo: e.target.value });
    const value = e.target.value;
    // Check the mobile validation and set the state
    window.setTimeout(() => this.checkLoginValidation(value));
  }

  handleOtpChange = (e) => {
    this.setState({ otp: e.target.value });
    const value = e.target.value;
    // Check the otp validation and set state
    window.setTimeout(() => this.checkOtpValidation(value));
  }

  triggerLogin = () => {
    const mobileNo = this.state.mobileNo;
    // Issue a request if login details are valid
    if (this.state.isMobileNumberValid) {
      const loginReq = {
        params: {
          mobile: this.state.mobileNo,
          otp: this.state.otp,
          // otp:common.encryptSHA(this.state.otp)
        },
        successCb: (response) => {
          const resObj = response.data;
          // Setup the basic User/Token/Session Details
          // Call the saga service
          this.props.setUserDetails({
            ...this.props.userDetails,
            bpm_token: resObj.access_token,
            refresh_token: resObj.refresh_token,
            mobileNumber: this.state.mobileNo,
          });
        },
        errorCb: () => {
          console.log('OTP failed');
        },
      };

      this.props.postRequest({ key: 'validateOtp', data: loginReq });
    }
  }

  generateOtp = (callback) => {
    const mobileNo = this.state.mobileNo;
    // Issue a request if login details are valid
    if (this.state.isMobileNumberValid) {
      const loginReq = {
        params: { mobile: this.state.mobileNo },
        successCb: (response) => {
          this.showOtpField();
          // Setup the basic User/Token/Session Details
          // Call the saga service
          if (callback && typeof callback === 'function') {
            callback();
          }
        },
        errorCb: () => {
          console.debug('OTP failed to generate');
        },
      };

      this.props.postRequest({ key: 'generateOtp', data: loginReq });
    }
  }
  generateTokenOtp = (callback) => {
    // Issue a request if token is available
    if (this.state.token) {
      const loginReq = {
        params: { token: this.state.token },
        successCb: (response) => {
          // Setup the basic User/Token/Session Details
          // Call the saga service
          if (callback && typeof callback === 'function') {
            callback();
          }
        },
        errorCb: () => {
          console.debug('OTP failed to generate');
        },
      };

      this.props.postRequest({ key: 'generateTokenOtp', data: loginReq });
    }
  }


  triggerTokenLogin = () => {
    const mobileNo = this.state.token;
    // Issue a request if login details are valid
    if (this.state.token) {
      const loginReq = {
        params: {
          token: this.state.token,
          otp: this.state.otp,
        },
        successCb: (response) => {
          const resObj = response.data;
          // Setup the basic User/Token/Session Details
          // Call the saga service
          this.props.setUserDetails({
            ...this.props.userDetails,
            bpm_token: resObj.access_token,
            refresh_token: resObj.refresh_token,
            mobileNumber: this.state.mobileNo,
          });
        },
        errorCb: () => {
          console.log('OTP failed');
        },
      };

      this.props.postRequest({ key: 'validateTokenOtp', data: loginReq });
    }
  }


  render() {
    return (
      null
    );
  }
}
