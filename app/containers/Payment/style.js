import { colors, fonts } from '../../configs/styleVars';

export const payListStyle = { paddingLeft: 15, paddingRight: 15 };

export const buttonWrapperStyle = {
  flexDirection: 'row',
  alignItems: 'center',
  justifyContent: 'space-between',
  marginLeft: 30,
  marginRight: 30,
  marginTop: 20,
};

export const buttonFontStyle = { fontSize: fonts.subHeadingSize, ...fonts.getFontFamilyWeight(), color: colors.primaryBGColor };

export const centerListStyle = { alignItems: 'center' };

export const paidStatusStyle = { color: colors.primaryBGColor };

export const dueStatusStyle = { color: colors.errorColor };

export const midSectionStyle = { flexDirection: 'row', alignItems: 'flex-start', justifyContent: 'space-between' };

export const listItemStyle = { paddingTop: 5, paddingBottom: 5 };
