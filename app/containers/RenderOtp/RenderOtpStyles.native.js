import { colors, globalColors, fonts } from '../../configs/styleVars';
export const wrapperStyles = {
  flex: 1,
  backgroundColor: colors.white,
};

export const inputProps = {
  keyboardType: 'numeric',
  errorColor: colors.errorColor,
  tintColor: colors.primaryBGColor,
};

export const topViewStyles = {
  style: {
    paddingBottom: 30,
    paddingTop: 50,
    alignItems: 'center',
  },
};

export const bottomViewStyles = {
  style: {
    paddingTop: 80,
    paddingBottom: 30,
  },
};

export const midViewStyles = {
  style: {
    marginBottom: 40,
    alignItems: 'center',
  },
};

export const headingStyles = {
  style: {
    color: colors.lighterColor,
    fontSize: fonts.mainHeadingSize,
    ...fonts.getFontFamilyWeight(),
    textAlign: 'center',
    paddingBottom: 10,
    ...fonts.getFontFamilyWeight(),
  },
};


export const paraStyles = {
  style: {
    fontSize: fonts.paraSize,
    color: colors.lighterColor,
    textAlign: 'center',
    ...fonts.getFontFamilyWeight(),
  },
};
export const topParaStyles = {
  style: {
    ...fonts.getFontFamilyWeight(),
    fontSize: fonts.body,
    color: colors.black,
  },
};

export const topLabelStyles = {
  style: {
    color: colors.grey96,
    fontSize: fonts.labelSize,
    ...fonts.getFontFamilyWeight(),
  },
};

export const linkStyles = {
  style: {
    color: colors.primaryBGColor,
    fontSize: fonts.body,
    ...fonts.getFontFamilyWeight('bold'),
    textAlign: 'center',
    marginTop: 10,
  },
};

export const disableLinkStyles = {
  style: {
    ...linkStyles.style,
    color: colors.primaryBGColor,
  },
};

export const hintStyles = {
  ...paraStyles.style,
  paddingBottom: 20,
  color: globalColors.darkGray,
};

export const timerStyles = {
  color: colors.black,
  ...fonts.getFontFamilyWeight('bold'),
  fontSize: fonts.body,
};

export const codeInputStyle = { ...fonts.getFontFamilyWeight('bold'), fontSize: fonts.h3, color: colors.black, borderRadius: 3 };

export const intervalStyle = { alignItems: 'center', paddingTop: 80 };
