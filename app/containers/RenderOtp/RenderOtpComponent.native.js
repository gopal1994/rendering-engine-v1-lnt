/**
 *
 * otp Native
 *
 */


import React from 'react';
import { View, Text, NativeModules, ScrollView, Platform } from 'react-native';

import CodeInput from 'react-native-confirmation-code-input';
import { AnimatedCircularProgress } from 'react-native-circular-progress';
import RenderOtpAbstract from './RenderOtpAbstract';
import RenderButton from '../../components/RenderButton';
import RenderError from '../../components/RenderError/';
import { colors, globalColors, fonts } from '../../configs/styleVars';
import * as styles from './RenderOtpStyles.native';
import { strings } from '../../../locales/i18n';
export default class RenderOtpComponent extends RenderOtpAbstract { // eslint-disable-line react/prefer-stateless-function

  // setDeviceId = () => {
  //   const self = this;
  //   NativeModules.Utilities.getDeviceToken((deviceId) => {
  //     console.debug('deviceId isssss ###', deviceId);
  //     self.setState({ deviceId });
  //   });
  // };

  handleOtpChange = (otp) => {
    this.setState({ otp });
    const isOtpValid = this.checkOtpValidation(otp);
    if (isOtpValid) {
      const deviceType = Platform.OS;
      this.submitOtp(otp, deviceType);
    }
  }

  navigateToJourney = () => {
    this.props.navigation.navigate('RenderJourneyNavigation');
  }

  // TODO: create separate component for Code input
  render() {
    const errorMessage = this.state.invalidOtpError ? (<RenderError errorMessage={this.state.invalidOtpError} />) : null;
    const topParaStyles = {};
    const topLabelStyles = {};
    topParaStyles.style = { ...styles.paraStyles.style, ...styles.topParaStyles.style };
    topLabelStyles.style = { ...styles.paraStyles.style, ...styles.topLabelStyles.style };
    const linkStyles = this.state.resendEnabled ? styles.linkStyles : styles.disableLinkStyles;
    const { intervalCounter } = this.state;
    return (
      <ScrollView
        keyboardShouldPersistTaps="always"
        style={styles.wrapperStyles}
      >
        <View
          {...styles.topViewStyles}
        >
          <Text {...topParaStyles}>{strings('login.enterOtp')}</Text>
          <Text {...topParaStyles}>{this.props.userDetails.mobileNumber}</Text>
        </View>
        <View {...styles.midViewStyles} >
          <CodeInput
            keyboardType="numeric"
            codeLength={4}
            space={6}
            size={75}
            inputPosition="center"
            activeColor={globalColors.lightGray}
            inactiveColor={globalColors.lightGray}
            onFulfill={(code) => this.handleOtpChange(code)}
            codeInputStyle={styles.codeInputStyle}
          />
        </View>
        <View style={{ alignItems: 'center' }}>
          {errorMessage}
        </View>
        {
          intervalCounter ?
            <View style={styles.intervalStyle}>
              <Text style={styles.hintStyles}>{strings('login.otpExpireIn')}</Text>
              <AnimatedCircularProgress
                size={75}
                width={5}
                fill={Math.floor((intervalCounter / this.OTP_VALID_TIME) * 100)}
                tintColor={colors.fillColor}
                backgroundColor={colors.white}
                prefill={100}
                rotation={0}
              >
                {() => (
                  <Text style={styles.timerStyles}>
                    { `${intervalCounter}s` }
                  </Text>)
            }
              </AnimatedCircularProgress>
            </View>
           : <View
             {...styles.bottomViewStyles}
           >
             <Text {...styles.paraStyles}>{strings('login.didNotReciveOtp')}</Text>
             <Text {...linkStyles} onPress={this.resendOtp}>{strings('login.resendOtp')}</Text>
             {/* {
            (!this.state.resendEnabled && this.state.resendCounter < 3)
              ? (<Text {...styles.paraStyles}>Please wait for {intervalCounter} seconds.</Text>)
              : null
          } */}
           </View>
        }
      </ScrollView>
    );
  }
}

