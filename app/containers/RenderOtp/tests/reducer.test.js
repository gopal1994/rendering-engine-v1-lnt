
import { fromJS } from 'immutable';
import renderOtpReducer from '../reducer';

describe('renderOtpReducer', () => {
  it('returns the initial state', () => {
    expect(renderOtpReducer(undefined, {})).toEqual(fromJS({}));
  });
});
