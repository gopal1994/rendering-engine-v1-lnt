/**
 *
 * LeadListView
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import injectSaga from '../../utils/injectSaga';
import injectReducer from '../../utils/injectReducer';
import makeSelectLeadListView from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';

import { makeSelectAppDetails, makeSelectUserDetails } from '../AppDetails/selectors';
import * as appActions from '../AppDetails/actions';

import LeadListContainer from './LeadListContainer';

export class LeadListView extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (<LeadListContainer {...this.props} />);
  }
}

LeadListView.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  leadlistview: makeSelectLeadListView(),
  userDetails: makeSelectUserDetails(),
});

function mapDispatchToProps(dispatch) {
  return {
    getRequest: (reqObject) => dispatch(appActions.getRequest(reqObject)),
    postRequest: (reqObject) => dispatch(appActions.postRequest(reqObject)),
    setUserDetails: (data) => dispatch(appActions.setUserDetails(data)),
    dispatch,
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'leadListView', reducer });
const withSaga = injectSaga({ key: 'leadListView', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(LeadListView);
