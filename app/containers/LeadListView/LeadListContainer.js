/**
 *
 * Lead List Container web
 *
 */

import React from 'react';
import LeadListAbstract from './LeadListAbstract';
import LeadTable from './LeadTable';


export default class LeadListContainer extends LeadListAbstract { // eslint-disable-line react/prefer-stateless-function

  render() {
    return (
      <LeadTable
        onRefresh={() => this.getLeadsByKey('inProcessLeads')}
        data={this.state.inProcessLeads}
        paginationOptions={this.state.paginationOptions}
        searchText={this.state.searchText}
        searchDisabled
        onSubmitSearchText={(searchText) => this.fetchSearchResult('inProcessLeads', searchText)}
        additionalKeyset={{ pd1Location: 'PD1 Location', scheduledDateTime: 'Schedule' }}
        onPress={this.activateJourney}
        updateScheduleDateTime={(leadIds, dateTime) => { this.updateScheduleDateTimePerTable(leadIds, dateTime, 'inProcessLeads'); }}
        onPressNext={() => { this.fetchNextPage('inProcessLeads'); }}
        onPressPrev={() => { this.fetchPrevPage('inProcessLeads'); }}
      />);
  }
}
