
import * as common from '../../utilities/commonUtils';

export const getParsedMeta = (meta, formData = {}) => {
  let conditionalValues = {};
  if (meta.conditionalMeta) {
    Object.keys(meta.conditionalMeta).forEach((metaKey) => {
      if (metaKey !== 'validator' && meta.conditionalMeta[metaKey] && meta.conditionalMeta[metaKey].trim()) {
        meta[metaKey] = common.parseEval(meta.conditionalMeta[metaKey].trim(), formData);
      }
    });
  }

  return {...meta};
}