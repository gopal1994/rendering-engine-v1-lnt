/*
This service contains the props passed to RenderStep's components
based upon their form_type
*/

import * as styleConfig from '../../configs/styleConfig';
import * as common from '../../utilities/commonUtils';
import { appConst, layoutConfig } from '../../configs/appConfig';
import * as DocumentMap from '../../utilities/documentMap';
import styledComponents from '../../configs/styledComponents';
import { getParsedMeta } from './helper';



// Render behaviour of components
// This return an object in compliance with Material UI setting of input text
export function getInputBasicRenderObj(renderData) {
  const obj = {};
  const inputRenderSetting = styleConfig.inputRenderSetting;
  obj.floatingLabelText = inputRenderSetting.floatingText && renderData.name;

  const meta = renderData.metaData;
  if (meta.required || meta.required === 'true') {
    obj.floatingLabelText = styledComponents.getStyledRequiredLabel(obj.floatingLabelText);
  }

  return obj;
}

// Render behaviour of components
// This return an object in compliance with Material UI setting of input text
export function getDropdownBasicRenderObj(renderData) {
  const obj = {};
  const renderSetting = styleConfig.dropdownRenderSetting;

  obj.hintText = renderSetting.hintText && renderData.name;
  // obj.floatingLabelFixed = renderSetting.floatingLabelFixed;
  obj.floatingLabelText = renderSetting.floatingText && renderData.name;

  const meta = renderData.metaData;
  if (meta.required || meta.required === 'true') {
    obj.floatingLabelText = styledComponents.getStyledRequiredLabel(obj.floatingLabelText);
  }

  return obj;
}

const getInputFieldProps = (data, elmId, defaultValue, formData) => {
  // basicRenderObj contains the coniguration for hintText, floatingLabelText etc etc
  // This is as per the app style setting for input texts
  const basicRenderObj = getInputBasicRenderObj(data);
  const meta = getParsedMeta({...data.metaData, default: defaultValue}, formData),
    formType = meta.form_type;

  if (formType === 'number') { 
    meta.default = meta.default !== '' ? Number(meta.default) : meta.default;
  }
  const propObj = {
    ...basicRenderObj,
    id: `ku-text-${elmId}`,
    value: meta.default,
    disabled: meta.disabled,
    type: meta.isCurrency ? '' : formType,
    hintText: meta.hintText,
    multiLine: !!meta.multiline,
    rowsMax: meta.rowsMax,
    autoCapitalize: !!meta.autoCapitalize,
    textOnly: meta.textOnly,
  };
  return propObj;
};

const getHiddenFieldProps = (data, elmId, defaultValue, formData) => {
  // This is as per the app style setting for input texts
  const meta = getParsedMeta({...data.metaData, default: defaultValue}, formData),
    formType = meta.form_type;
  const propObj = {
    id: `ku-hidden-${elmId}`,
    value: meta.default,
    disabled: meta.disabled,
  };
  return propObj;
};

const getCheckboxProps = (data, elmId, defaultValue, formData) => {
  // TODO: drive the default settings from a config
  const meta = getParsedMeta({...data.metaData, default: defaultValue}, formData),
    formType = meta.form_type;

  meta.default = meta.default === 'false' || meta.default === '' ? false : meta.default;
  const propObj = {
    id: `ku-checkbox-${elmId}`,
    checked: !!meta.default,
    disabled: meta.disabled,
    label: data.name,
  };
  
  return propObj;
};

const getRadioGroupProps = (data, elmId, defaultValue, formData) => {
  // This is as per the app style setting for input texts
  // TODO: drive the default settings from a config
  const meta = getParsedMeta({...data.metaData, default: defaultValue}, formData),
    formType = meta.form_type;
  const propObj = {
    radioGroupProp: {
      id: `ku-radio-${elmId}`,
      valueSelected: meta.default,
      cardsPerRow: meta.cardsPerRow || (layoutConfig.defaultRadioCardsWrapperWidth / layoutConfig.defaultRadioCardsWidth),
    },
    radioButtonProp: {
      disabled: meta.disabled,
    },
  };
  return propObj;
};

const getDatepickerProps = (data, elmId, defaultValue, formData) => {
  // basicRenderObj contains the coniguration for hintText, floatingLabelText etc etc
  // This is as per the app style setting for input texts
  // TODO: drive the default settings from a config
  const obj = {};
  const inputRenderSetting = styleConfig.inputRenderSetting;
  obj.floatingLabelText = inputRenderSetting.floatingText && data.name;

  const meta = getParsedMeta({...data.metaData, default: defaultValue}, formData);
  if (meta.required || meta.required === 'true') {
    obj.floatingLabelText = styledComponents.getStyledRequiredLabel(obj.floatingLabelText);
  }
  const basicRenderObj = obj;
  const maxAge = meta.maxAge;
  const minAge = meta.minAge;
  const minDate = meta.minDate;
  const maxDate = meta.maxDate;
  const validator = meta.validator;
  const dateStr = meta.default ? common.unformatDateStr(meta.default) : '';
  const isValidDate = common.isValidDate(dateStr);
  const propObj = {
    ...basicRenderObj,
    id: `ku-datepicker-${elmId}`,
    defaultDate: isValidDate ? new Date(dateStr) : null,
    value: meta.default ? new Date(dateStr) : null,
    disabled: meta.disabled,
    label: data.name,
    formatDate: common.formatDefaultDate,
    autoOk: true,
    container: 'dialog',
    isDateRemovable: !!meta.isDateRemovable,
  };
  if (validator.includes('date')) {
    if (maxDate !== undefined) {
      propObj.maxDate = common.getExpiryDate(maxDate);
    }
    if (minDate !== undefined) {
      propObj.minDate = common.getExpiryDate(minDate);
    }
  } else {
    if (maxAge !== undefined) {
      propObj.minDate = common.getDateFromAge(maxAge);
    }
    if (minAge !== undefined) {
      propObj.maxDate = common.getDateFromAge(minAge);
    }
  }
  return propObj;
};


const getDropdownProps = (data, elmId, defaultValue, formData) => {
  // TODO: drive the default settings from a config
  const meta = getParsedMeta({...data.metaData, default: defaultValue}, formData),
    formType = meta.form_type;
  const basicRenderObj = getDropdownBasicRenderObj(data);
  const propObj = {
    ...basicRenderObj,
    id: `ku-dropdown-${elmId}`,
    value: meta.default,
    disabled: meta.disabled,
  };
  return propObj;
};

const getSliderProps = (data, elmId, defaultValue, formData) => {
  // TODO: drive the default settings from a config
  const meta = getParsedMeta({...data.metaData, default: defaultValue}, formData),
    formType = meta.form_type;
  let isIrregular = meta.isStepIrregluar,
    valArr,
    step,
    sliderStepIndex;
  if (isIrregular) {
    valArr = meta.valArr;
    step = 1;
    defaultValue = meta.default || valArr[0];
    sliderStepIndex = valArr.indexOf(meta.default);
  } else {
    defaultValue = meta.default ? Number(meta.default) : meta.min * meta.step;
  }

  const propObj = {
    id: `ku-slider-${elmId}`,
    value: defaultValue,
    disabled: meta.disabled,
    max: !isIrregular ? meta.max * meta.step : valArr.length - 1,
    min: !isIrregular ? meta.min * meta.step : 0,
    step: !isIrregular ? meta.step : 1,
    isCurrency: meta.isCurrency,
    valArr,
    isStepIrregluar: isIrregular,
    sliderStepIndex,
  };
  return propObj;
};

const getUploadProps = (data, elmId, defaultValue, formData) => {
  // TODO: drive the default settings from a config
  const meta = getParsedMeta({...data.metaData, default: defaultValue}, formData),
    formType = meta.form_type,
    docType = DocumentMap.getDocumentType(elmId);
  const propObj = {
    multiple: !!meta.isMultiple,
    value: meta.default,
    accept: meta.filetypes || 'application/pdf,image/png,image/jpeg', // TODO: move this to a constant config
    docSource: meta.source ? meta.source : 'fileManager',
    docType: meta.docType,
  };
  return propObj;
};

const getAutoCompleteProps = (data, elmId, defaultValue, formData) => {
  // basicRenderObj contains the coniguration for hintText, floatingLabelText etc etc
  // This is as per the app style setting for input texts
  const basicRenderObj = getInputBasicRenderObj(data);
  const meta = getParsedMeta({...data.metaData, default: defaultValue}, formData),
    formType = meta.form_type;
  const propObj = {
    ...basicRenderObj,
    id: `ku-autocomplete-${elmId}`,
    value: meta.default,
    disabled: meta.disabled,
    type: formType,
    allowKeyInput: meta.allowKeyInput,
  };
  return propObj;
};

const getDownloadProps = (data, elmId, defaultValue, formData) => {
  const meta = getParsedMeta({...data.metaData, default: defaultValue}, formData);

  const propObj = {
    url: meta.default,
    label: data.name,
    docType: meta.docType,
  };
  return propObj;
};

const getHeadingProps = (data, elmId, defaultValue, formData) => {
  const meta = getParsedMeta({...data.metaData, default: defaultValue}, formData);
  const propObj = {
    label: data.name,
    type: meta.headingType,
    value: meta.default,
  };

  return propObj;
};

const getAccordionProps = (data, elmId, defaultValue, formData) => {
  const meta = getParsedMeta({...data.metaData, default: defaultValue}, formData);
  const propObj = {
    label: data.name,
    imageUrl: meta.imageUrl,
  };

  return propObj;
};

const getRadioAccordionProps = (data, elmId, defaultValue, formData) => {
  const meta = getParsedMeta({...data.metaData, default: defaultValue}, formData);
  const propObj = {
    label: data.name,
    valueHolder: meta.valueHolder,
    imageUrl: meta.imageUrl,
  };

  return propObj;
};

const getGeotrackingProps = (data, elmId, defaultValue, formData) => {
  const basicRenderObj = {
    label: data.label,
  };
  const meta = getParsedMeta({...data.metaData, default: defaultValue}, formData);
  const propObj = {
    ...basicRenderObj,
    value: meta.default,
    hintText: meta.hintText,
  };
  return propObj;
};

const getIconProps = (data, elmId, defaultValue, editModeState) => {
  const meta = data.metaData;
  const basicRenderObj = getInputBasicRenderObj(data, editModeState);
  const propObj = {
    label: basicRenderObj.floatingLabelText,
    type: meta.type,
    //disabled: isReadMode
  };

  return propObj;
}

const getExpressionProps = (data, elmId, defaultValue, formData) => {
  const meta = getParsedMeta({...data.metaData, default: defaultValue}, formData);
  const propObj = {

  };

  return propObj;
}


export default {
  text: getInputFieldProps,
  rest: getInputFieldProps,
  number: getInputFieldProps,
  hidden: getHiddenFieldProps,
  checkbox: getCheckboxProps,
  radio: getRadioGroupProps,
  datepicker: getDatepickerProps,
  dropdown: getDropdownProps,
  'multiselect-dropdown': getDropdownProps,
  'rest-dropdown': getDropdownProps,
  slider: getSliderProps,
  upload: getUploadProps,
  download: getDownloadProps,
  autocomplete: getAutoCompleteProps,
  elasticAutocomplete: getAutoCompleteProps,

  heading: getHeadingProps,
  accordion: getAccordionProps,
  radioAccordion: getRadioAccordionProps,
  locator: getGeotrackingProps,
  icon: getIconProps,
  expression: getExpressionProps,
};
