/*
 * RenderStep Messages
 *
 * This contains all the text for the RenderStep component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.RenderStep.header',
    defaultMessage: 'This is RenderStep container !',
  },
});
