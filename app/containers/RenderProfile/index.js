/**
 *
 * RenderProfile
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
// import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import injectSaga from '../../utils/injectSaga';
import injectReducer from '../../utils/injectReducer';
// import reducer from './reducer';
import saga from './saga';
// import messages from './messages';
import * as renderProfileActions from './actions';
import RenderProfileComponent from './RenderProfileComponent';
import makeSelectRenderJourney from '../RenderJourney/selectors';
import { makeSelectUserDetails } from '../AppDetails/selectors';
import * as appActions from '../AppDetails/actions';

import reducer from '../RenderJourney/reducer';

export class RenderProfile extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <RenderProfileComponent {...this.props} />
    );
  }
}

RenderProfile.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  renderjourney: makeSelectRenderJourney(),
  userDetails: makeSelectUserDetails(),
});

function mapDispatchToProps(dispatch) {
  return {
    initRenderProfileService: (renderObj) => dispatch(renderProfileActions.initRenderProfileService(renderObj)),
    getRequest: (reqObject) => dispatch(appActions.getRequest(reqObject)),
    postRequest: (reqObject) => dispatch(appActions.postRequest(reqObject)),
    setPopupData: (data) => dispatch(appActions.setPopupData(data)),
    dispatch,
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'renderJourney', reducer });
// const withReducerRenderJourney = injectReducer({ key: 'renderJourney', reducer: renderJourneyReducer });
const withSaga = injectSaga({ key: 'renderProfile', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(RenderProfile);
