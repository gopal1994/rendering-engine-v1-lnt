/*
 * RenderProfile Messages
 *
 * This contains all the text for the RenderProfile component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.RenderProfile.header',
    defaultMessage: 'This is RenderProfile container !',
  },
});
