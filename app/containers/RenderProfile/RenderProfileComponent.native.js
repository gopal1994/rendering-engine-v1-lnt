/**
*
* RenderProfile
*
*/

import React from 'react';
import { View, KeyboardAvoidingView, Platform } from 'react-native';
import RenderProfileAbstract from './RenderProfileAbstract';
import RenderJourney from '../RenderJourney';
import { ModalOverlay } from '../../components/RenderStaticComponents';
import SelfieConfirmation from '../../components/SelfieConfirmation';
import RenderPopup from '../../components/RenderPopup';
import Loader from '../../components/Loader';

export default class RenderProfileComponent extends RenderProfileAbstract { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);
    this.state = {
      openCamera: false,
      showPopup: '',
      loaderState: {},
    };
  }

  onModalShow = () => {
    this.triggerInitiate();
  }

  toggleRetry = (value) => {
    this.setState({ openCamera: value });
  }

  getPopInfo = () => {
    let showPopup = false;
    let popData = {};
    switch (this.state.showPopup) {
      case 'success':
        showPopup = true;
        popData = this.getSuccessPopUpdata();
        break;
      case 'failure':
        showPopup = true;
        popData = this.getErrorPopUpdata();
        break;

      default:
        break;
    }
    return [showPopup, popData];
  }

  render() {
    const { showForm, hideForm } = this.props;
    const [showPopup, popData] = this.getPopInfo();
    const scrollBehave = {};
    if (Platform.OS === 'ios') {
      scrollBehave.behavior = 'padding';
    }
    return (
      <ModalOverlay
        onBack={hideForm}
        isVisible={showForm}
        headerType={'white'}
        onModalShow={this.onModalShow}
      >
        {/* <Text style={styles.modalTitle}>{'Vấn đề giải ngân và thanh toán'}</Text> */}
        <KeyboardAvoidingView
          style={{ flex: 1 }}
          {...scrollBehave}
        >
          <View style={{ flex: 1 }}>
            { showForm ? <RenderJourney journeyType={'POST_JOURNEY'} /> : null }
          </View>
          <View style={{ paddingHorizontal: 20, paddingBottom: 20 }}>
            <SelfieConfirmation
              openCamera={this.state.openCamera}
              onSubmit={this.submitData}
              toggleRetry={this.toggleRetry}
              successPopData={this.getSuccessPopUpdata()}
              errorPopData={this.getErrorPopUpdata()}
              showPopup={this.state.showPopup}
            />
            <RenderPopup
              showPopup={showPopup}
              {...popData}
            />
            <Loader loaderStatus={this.state.loaderState} />
          </View>
        </KeyboardAvoidingView>
      </ModalOverlay>
    );
  }
}
