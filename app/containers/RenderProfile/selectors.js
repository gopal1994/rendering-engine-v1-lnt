import { createSelector } from 'reselect';

/**
 * Direct selector to the renderProfile state domain
 */
const selectRenderProfileDomain = (state) => state.get('renderProfile');

/**
 * Other specific selectors
 */


/**
 * Default selector used by RenderProfile
 */

const makeSelectRenderProfile = () => createSelector(
  selectRenderProfileDomain,
  (substate) => substate.toJS()
);


export default makeSelectRenderProfile;
export {
  selectRenderProfileDomain,
};

