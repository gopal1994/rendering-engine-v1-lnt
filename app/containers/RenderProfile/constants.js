/*
 *
 * RenderProfile constants
 *
 */

export const DEFAULT_ACTION = 'app/RenderProfile/DEFAULT_ACTION';
export const INIT_RENDER_PROFILE_SERVICE = 'app/RenderProfile/INIT_RENDER_PROFILE_SERVICE';
