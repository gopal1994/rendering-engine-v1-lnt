import { sizes, colors, fonts } from '../../configs/styleVars';

export const wrapperStyle = { paddingLeft: sizes.appPaddingLeft, paddingRight: sizes.appPaddingRight, justifyContent: 'flex-start', paddingBottom: 20, backgroundColor: colors.white, flex: 1 };

export const headerStyle = { color: colors.headerColor, marginTop: 40, marginBottom: 20, fontSize: fonts.h1, ...fonts.getFontFamilyWeight('bold') };
