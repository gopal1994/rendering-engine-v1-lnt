import { colors, fonts, lineHeight } from '../../configs/styleVars';

export const headingStyle = {
  fontSize: 20,
  lineHeight: lineHeight.h3,
  color: colors.black,
  marginTop: 15,
  marginBottom: 15,
  ...fonts.getFontFamilyWeight('bold'),
  textAlign: 'center',
};

export const bodyText = {
  fontSize: 16,
  lineHeight: lineHeight.description,
  color: colors.black,
  marginTop: 15,
  marginBottom: 15,
  textAlign: 'center',
  ...fonts.getFontFamilyWeight(),
};
