/**
*
* RenderUpload
*
*/

import React from 'react';
import { View, ScrollView, NativeModules, Platform, Dimensions } from 'react-native';
import Text from '../../../components/RenderText';

import RenderButton from '../../../components/RenderButton/';
import RenderPreview from '../../../components/RenderPreview/';

import { headingStyle, bodyText } from '../styles';
import styleVars from '../../../configs/styleVars';
import * as styles from '../../../components/RenderUpload/RenderUploadStyles';
import { permissionConst, getClientKey } from '../../../configs/appConstants';
import * as NativeMethods from '../../../components/RenderUpload/RenderUploadHelper';
import * as sessionUtils from '../../../utilities/sessionUtils';

import { strings } from '../../../../locales/i18n';

import { setNationalIdErrorMessage } from './subJourneyService';
const frontType = 'FRONT_ID';
const backType = 'BACK_ID';

export default class RenderNationalIdSubJourney extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);
    this.state = {
      nationalIdConfig: this.props.config,
      isFrontIdSubmitted: false,
      isFrontIdUploaded: false,
      frontIDLocal: null,
      frontIDURL: null,
      isBackIdSubmitted: false,
      isBackIdUploaded: false,
      backIDLocal: null,
      backIDURL: null,
      currentValue: null,
      retryUpload: false,
    };
  }

  componentDidMount() {
    console.log('calling capturePhoto from componentDidMount', frontType);
    this.capturePhoto(frontType);
  }

  setLocalImg = (type, file) => {
    if (type === frontType) {
      this.setState({ frontIDLocal: file });
    } else {
      this.setState({ backIDLocal: file });
    }
  }

  frontUploadSuccessCb = (data) => {
    this.setState({
      isFrontIdUploaded: true,
      frontIDURL: data.dmsURL,
      isFrontIdSubmitted: true,
    });
  }

  backUploadSuccessCb = (data) => {
    this.setState({
      isBackIdUploaded: true,
      backIDURL: data.dmsURL,
      isBackIdSubmitted: true,
    });
  }


  submitStep = (type) => {
    const file = (type === frontType) ? this.state.frontIDLocal : this.state.backIDLocal;
    this.triggerUpload(file, type);
  }

  frontIdSubmitSuccessCb = (data) => {
    const self = this;
    window.setTimeout(() => {
      self.setState({
        isFrontIdUploaded: true,
        frontIDURL: data.dmsURL,
        isFrontIdSubmitted: true,
        retryUpload: false,
      }, () => {
        self.capturePhoto(backType);
      });
    });
  }

  backIdSubmitSuccessCb = (data) => {
    const self = this;
    window.setTimeout(() => {
      self.setState({
        isBackIdUploaded: true,
        backIDURL: data.dmsURL,
        isBackIdSubmitted: true,
        retryUpload: false,
      }, () => {
        self.verifyNationalId(self.state.nationalIdConfig.verify);
      });
    });
  }

  successUploadCb = (data, type) => {
    // this.setState({retryUpload: false});
    if (type === frontType) {
      this.frontIdSubmitSuccessCb(data);
    } else if (type === backType) {
      this.backIdSubmitSuccessCb(data);
    }
  }

  errorUploadCb = (type) => {
    if (type === frontType) {
      this.setState({ isFrontIdUploaded: false, frontIDURL: null, frontIDLocal: null, retryUpload: true });
    } else if (type === backType) {
      this.setState({ isBackIdUploaded: false, backIDURL: null, backIDLocal: null, retryUpload: true });
    }
  }

  buildFileData = (targetFile) => ({
    uri: targetFile.uri,
    name: targetFile.fileName,
    size: targetFile.fileSize,
    type: targetFile.type,
  })

  // Config here is respective file's object in the meta
  _buildRequestParams = (file, config) => {
    const fd = new FormData();
    fd.append('image', { ...this.buildFileData(file) });
    fd.append('fileType', config.fileType);
    fd.append('docID', config.docID);
    fd.append('moduleID', config.moduleID);
    fd.append('renameFile', config.renameFile);
    fd.append('appID', config.appID || this.props.formData.applicationId);
    // processInstanceId
    fd.append('id', this.props.userDetails.processInstanceId);

    return fd;
  }

  _buildRequestObject(file, config, type) {
    const self = this;
    const reqData = {
      url: config.api && config.api.url,
      params: this._buildRequestParams(file, config),
      userDetails: this.props.userDetails,
      contentType: 'multipart/form-data',
      successCb: (response) => {
        console.debug('got response from the uplaod', response);
        const data = response.data;
        if (data.dmsURL) {
          self.successUploadCb(data, type);
        } else if (data.error) {
          self.errorUploadCb(type);
        }
      },
      errorCb: () => {
        self.errorUploadCb(type);
      },
    };
    return reqData;
  }

  triggerUpload = (file, type) => {
    const config = (type === frontType) ? this.state.nationalIdConfig.frontId : this.state.nationalIdConfig.backId;

    const method = config.api && config.api.method;
    const reqObj = this._buildRequestObject(file, config, type);

    console.debug('whta is reqObj', reqObj);
    if (method === 'POST') {
      this.props.postRequest({ key: null, data: reqObj });
    } else {
      this.props.getRequest({ key: null, data: reqObj });
    }
  }

  // getPermission = async () => {
  //   return NativeMethods.getPermission();
  // }

  capturePhoto = async (type) => {
    const self = this;
    console.debug('inside capturePhoto for', type);
    // const permissionStatus = await NativeMethods.getPermission();

    NativeMethods.getPermission().then((permissionStatus) => {
      const docType = 'CARD';
      let topText,
        bottomText;
      if (type === frontType) {
        topText = strings('subJourney.frontIdTopText');
        bottomText = strings('subJourney.frontIdBottomText');
      }
      if (type === backType) {
        topText = strings('subJourney.backIdTopText');
        bottomText = strings('subJourney.backIdBottomText');
      }
      if (permissionStatus === permissionConst.GRANTED) {
        const { appid, appkey } = getClientKey('hypervergeSnap');
        NativeModules.CameraModule.initHyperverge(appid, appkey, 'AsiaPacific', 'FACEID');
        NativeModules.CameraModule.openCamera(docType, topText, bottomText, (res) => {
          if (res) {
            const file = JSON.parse(res);
            const uri = Platform.OS === 'ios' ? file.uri : `file://${file.path}`;
            file.uri = uri;
            self.setLocalImg(type, file);
          }
        });
      } else {
        console.log('Camera permission denied');
      }
    });
  }

  buildFormDataJsonString = (data) => {
    let obj,
      self = this;
    if (data.status) {
      obj = {
        verified: true,
        frontDMSURL: self.state.frontIDURL,
        backDMSURL: self.state.backIDURL,
      };
      return JSON.stringify(obj);
    }
    if (data.retryReasonCode && Array.isArray(data.retryReasonCode)) {
      setNationalIdErrorMessage(data.retryReasonCode);
      return false;
    }
    return false;
  }

  buildVerifyReqObj = (config) => {
    const self = this;
    const reqData = {
      isLongReq: true,
      loaderMessage: strings('subJourney.verifyNationalIdLoader'),
      url: config.api && config.api.url,
      params: { id: this.props.userDetails.processInstanceId },
      userDetails: this.props.userDetails,
      successCb: (response) => {
        console.debug('got response from the verify', response);
        const data = response.data;
        // setFormData

        self.props.updateFormData(self.props.subJourneyId, self.buildFormDataJsonString(data));
        self.props.validateAndUpdateRender(self.buildFormDataJsonString(data), self.props.subJourneyId);
        if (data.error && data.error === 1) {
          // when the error-code is 1 then max retry for the verification has been exhausted
          // hence the loan has been rejected
          // hence the back end task has been closed so trigger the initiate again
          self.props.refreshJourney();
          // trigger initiate
        }
        // exit
        self.props.exit();
      },
      errorCb: () => {
        console.debug('something went wrong in national id verify');
      },
    };

    return reqData;
  }

  verifyNationalId = (config) => {
    const self = this;
    let method = config.api && config.api.method,
      reqObj = this.buildVerifyReqObj(config);

    if (method === 'POST') {
      this.props.postRequest({ key: null, data: reqObj });
    } else {
      this.props.getRequest({ key: null, data: reqObj });
    }
  }


  getActionBlock = (type) => {
    const renderActions = null;
    return (
      <View style={{ alignSelf: 'flex-end' }}>
        <RenderButton
          text={strings('subJourney.submitText')}
          onPress={() => this.submitStep(type)}
          type={'primary'}
          {...styles.btnProps}
        />
        <RenderButton
          text={strings('subJourney.reTakeText')}
          onPress={() => this.capturePhoto(type)}
          type={'secondary'}
          {...styles.btnProps}
        />
      </View>
    );
  }

  render() {
    let renderBlock = null;
    let actionBlock = null;
    const messageBlock = (<Text style={bodyText}>{strings('subJourney.instructionText')}</Text>);
    let previewBlock = null;
    let retryBlock = null;

    let headingBlock = null;
    if (this.state.retryUpload) {
      retryBlock = (<Text style={bodyText}>{strings('subJourney.retryText')}</Text>);
    }

    const previewStyles = { height: (Dimensions.get('window').width - 60) * 0.625, marginTop: 25 };

    if (!this.state.isFrontIdSubmitted) {
      headingBlock = (<Text style={headingStyle}>{strings('subJourney.nationalIdFrontHeading')}</Text>);
      if (!this.state.frontIDLocal) {
        // Show button to click photo
        renderBlock = (
          <View>
            {retryBlock}
            <RenderButton
              text={strings('subJourney.clickFrontNationIdButton')}
              onPress={() => this.capturePhoto(frontType)}
              type={'primary'}
              {...styles.btnProps}
            />
          </View>);
      } else {
        previewBlock = (
          <View style={previewStyles}>
            <RenderPreview
              key={`uploaded${frontType}`}
              uri={this.state.frontIDLocal.uri}
            />
          </View>);
        actionBlock = this.getActionBlock(frontType);
      }
    } else {
      headingBlock = (<Text style={headingStyle}>{strings('subJourney.nationalIdBackHeading')}</Text>);
      if (!this.state.backIDLocal) {
        // Show button to click photo
        renderBlock = (
          <View>
            {retryBlock}
            <RenderButton
              text={strings('subJourney.clickBackNationIdButton')}
              onPress={() => this.capturePhoto(backType)}
              type={'primary'}
              {...styles.btnProps}
            />
          </View>);
      } else {
        previewBlock = (
          <View style={previewStyles}>
            <RenderPreview
              key={`uploaded${backType}`}
              uri={this.state.backIDLocal.uri}
            />
          </View>);
        actionBlock = this.getActionBlock(backType);
      }
    }
    return (
      <View style={{ flex: 1 }}>
        <View style={{ flex: 1 }}>
          <ScrollView keyboardShouldPersistTaps="always">
            {headingBlock}
            {messageBlock}
            {previewBlock}
          </ScrollView>
        </View>
        {renderBlock}
        {actionBlock}
      </View>
    );
  }
}
