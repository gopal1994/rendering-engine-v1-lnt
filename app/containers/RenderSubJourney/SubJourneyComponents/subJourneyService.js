
import { strings } from '../../../../locales/i18n';

export const subJourneyIds = {
  nationalId: 'nationalIdSubJourney',
  selfie: 'selfie',
  facebook: 'facebook',
};

const errorMessages = {
  nationalIdSubJourney: '',
};

export const getSubJourneyErrorMessage = (key) => errorMessages[key];

export const setNationalIdErrorMessage = (codeArray) => {
  let message = '';
  codeArray.forEach((errCode, index) => {
    if (errCode === 1) {
      message += strings('subJourneyStrings.nationalIdNumber');
    }
    if (errCode === 2) {
      message += strings('subJourneyStrings.fullName');
    }
    if (errCode === 3) {
      message += strings('subJourneyStrings.dob');
    }

    if (index !== (codeArray.length - 1)) {
      message += ', ';
    }
    console.debug('what is national id number mismatch error', message);
  });

  message += ` ${strings('subJourneyStrings.detailsString')}`;

  errorMessages.nationalIdSubJourney = message;
  console.debug('FINALLLL mismatch error', message);
};
