// This file maps the respective subJourneyIds to respective Components/Containers
import RenderNationalIdSubJourney from './SubJourneyComponents/RenderNationalIdSubJourney';
import RenderSelfieSubJourney from './SubJourneyComponents/RenderSelfieSubJourney';
import RenderHVFBSubJourney from './SubJourneyComponents/RenderHVFBSubJourney';

import { subJourneyIds } from './SubJourneyComponents/subJourneyService';

export default {
  [subJourneyIds.nationalId]: RenderNationalIdSubJourney,
  [subJourneyIds.selfie]: RenderSelfieSubJourney,
  [subJourneyIds.facebook]: RenderHVFBSubJourney,
};
