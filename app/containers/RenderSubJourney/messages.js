/*
 * RenderSubJourney Messages
 *
 * This contains all the text for the RenderSubJourney component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.RenderSubJourney.header',
    defaultMessage: 'This is RenderSubJourney container !',
  },
});
