import { createSelector } from 'reselect';

/**
 * Direct selector to the renderMpin state domain
 */
const selectRenderMpinDomain = (state) => state.get('renderMpin');

/**
 * Other specific selectors
 */


/**
 * Default selector used by RenderMpin
 */

const makeSelectRenderMpin = () => createSelector(
  selectRenderMpinDomain,
  (substate) => substate.toJS()
);

export default makeSelectRenderMpin;
export {
  selectRenderMpinDomain,
};
