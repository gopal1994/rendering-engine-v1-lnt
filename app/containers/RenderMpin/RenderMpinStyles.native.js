import { colors, fonts, sizes, lineHeight } from '../../configs/styleVars';
import { nativeWrapperStyle } from '../../configs/componentStyles/commonStyles';

export const wrapperStyles = {
  justifyContent: 'flex-start',
  paddingLeft: sizes.appPaddingLeft,
  paddingRight: sizes.appPaddingRight,
  paddingBottom: 20,
};

export const inputProps = {
  keyboardType: 'numeric',
  errorColor: colors.errorColor,
  tintColor: colors.primaryBGColor,
};

export const btnProps = {
  containerStyle: {
    marginTop: 50,
  },
};

export const topViewStyles = {
  style: {
    paddingBottom: 30,
  },
};

export const headingStyles = {
  style: {
    fontSize: fonts.mainHeadingSize,
    ...fonts.getFontFamilyWeight(),
    color: colors.primaryBGColor,
    textAlign: 'center',
    paddingBottom: 10,
  },
};


export const paraStyles = {
  style: {
    ...fonts.getFontFamilyWeight(),
    lineHeight: lineHeight.description,
    textAlign: 'center',
    padding: 15,
  },
};

export const iconStyles = {
  color: colors.primaryBGColor,
};
