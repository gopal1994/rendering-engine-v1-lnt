/**
 *
 * mpin Native
 *
 */


import React from 'react';
import { ScrollView, Text, Image } from 'react-native';
import CodeInput from 'react-native-confirmation-code-input';
import TextInputField from '../../components/TextInputField';
import calendarIcon from '../../images/calendar.png';
import RenderMpinAbstract from './RenderMpinAbstract';
import RenderButton from '../../components/RenderButton';
import RenderError from '../../components/RenderError/';
import RenderPreHeader from '../../components/RenderPreHeader';
import { colors } from '../../configs/styleVars';
import * as styles from './RenderMpinStyles.native';
import { strings } from '../../../locales/i18n';
import { checkSupport } from '../../utilities/native/touchIDService.native';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';

export default class RenderMpinComponent extends RenderMpinAbstract { // eslint-disable-line react/prefer-stateless-function
  // TODO: create separate component for Code input

  componentWillMount() {
    this.checkTouchIDStatus();
  }

  checkTouchIDStatus = async () => {
    const supportStatus = await checkSupport();
    this.setState({ isTouchIDSupported: supportStatus });
  }

  // to be called from Pop up
  shouldEnableTouchID = (value) => {
    // show popup for permission to enable touch ID
    this.state.isTouchIDSupported && this.props.toggleTouchID(value);
    this.setMpinToStorage();
  }

  showTouchIDPopUp = () => {
    // show popup for permission to enable touch ID
    const popupData = this.getPopUpdata();
    this.props.setPopupData(popupData);
  }

  onSubmit = () => {
    const { reset, showPassword } = this.state;
    const submitAction = reset ? this.updateMpin() : this.submitMpin();
  }

  toggleShowPassword = () => {
    this.setState(({ showPassword }) => ({ showPassword: !showPassword }));
  }

  renderIcon = () => {
    const { showPassword } = this.state;

    const name = showPassword ?
      'visibility' :
      'visibility-off';

    return (
      <MaterialIcon
        size={24}
        name={name}
        color={showPassword ? colors.primaryBGColor : colors.labelColor}
        onPress={this.toggleShowPassword}
      />
    );
  }

  render() {
    const { showPassword } = this.state;
    return (
      <ScrollView
        keyboardShouldPersistTaps="always"
        contentContainerStyle={styles.wrapperStyles}
      >
        <TextInputField
          label={strings('login.newPassword')}
          error={this.state.invalidMpinError}
          onChangeText={this.handleMpinChange}
          maxLength={4}
          keyboardType={'numeric'}
          secureTextEntry={!showPassword}
          renderAccessory={this.renderIcon}
        />
        <TextInputField
          label={strings('login.confirmPassword')}
          error={this.state.invalidConfirmMpinError}
          onChangeText={this.handleConfirmMpinChange}
          maxLength={4}
          secureTextEntry
          keyboardType={'numeric'}
          onSubmitEditing={this.onSubmit}
          returnKeyType={'done'}
        />
        <Text {...styles.paraStyles}>{strings('login.feCreditPasswordMessage')}</Text>
        <RenderButton
          text={strings('login.buttonText')}
          onPress={this.onSubmit}
          type={'primary'}
          {...styles.btnProps}
        />
      </ScrollView>
    );
  }
}

