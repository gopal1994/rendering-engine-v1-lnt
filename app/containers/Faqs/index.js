/**
 *
 * Faqs
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { View, ScrollView } from 'react-native';
import { connect } from 'react-redux';
import { compose } from 'redux';
import RenderSubHeader from '../../components/RenderSubHeader';
import RenderLabel from '../../components/RenderLabel';
import { faqListStyle, faqDetailStyle } from './style';
import * as inactivityTracker from '../../utilities/inactivityTracker';

const faqList = [
  {
    title: 'Who can apply for a personal loan through the mobile app?',
    content: 'Any Vietnamese citizen having a valid National ID and between the age of 19 and 60 years can apply for a personal loan.',
  },
  {
    title: 'What is the minimum required income to be qualified for a loan?',
    content: 'The minimum income required to apply for a loan is 2 mio VND. However, the required income for successful application will be dependent on the required loan amount.',
  },
  {
    title: 'How long does it take to get loan approval?',
    content: 'Normally, the entire approval process will be completed in 5 minutes but can vary due to mobile connectivity.',
  },
  {
    title: 'What is the interest rate for my loan? Will it change during the tenure of the loan?',
    content: 'The interest rate will be determined during the application process. The interest rate will remain fixed through the tenure of the loan',
  },
  {
    title: 'Any specific purpose is required for applying for FE Credit loan?',
    content: 'According to the rules specified by the State Bank of Vietnam (SBV), customers are required to specify the purpose of their loan requirement.',
  },
  {
    title: 'What documents do I need to provide to apply for the loan?',
    content: 'All customers are required to provide a valid National ID. Customers may be required to provide income proof in the form of Bank statement/ EVN Bill/ MRC/ Insurance Contract and Insurance bill depending on the category of customer',
  },
  {
    title: 'How can I get the money if my loan is approved?',
    content: 'We can either transfer the money to the Bank Account of your choice or you can collect the money in cash from our partner locations. Our partners include VNPost, Sacombank, BIDV, VPBank, Agribank.',
  },
  {
    title: 'Where are the channels to repay the loan?',
    content: 'You can either pay online through our mobile app or pay cash at our partner locations. Our partners include VNPOST, Sacombank, BIDV, VPBank, Agribank.',
  },
  {
    title: 'I haven’t received Mpin yet, how can I get this code?',
    content: 'The MPIN can be requested through the app using the “Forgot MPIN” option ',
  },
  {
    title: ' If I terminate my loan early, how much will it cost?',
    content: 'Customers are free to foreclose the loan by paying an early termination fee of 5% of the outstanding principal loan amount',
  },
];

export class Faqs extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.resetInactivity = this.resetInactivity.bind(this);
  }

  componentDidMount() {
    this.resetInactivity();
  }

  componentWillUnmount() {
    inactivityTracker.clearTracker();
  }

  toggleMpin = () => {
    this.props.screenProps.toggleMpin(false);
  }

  // logs out user in case of inactivity for x min
  resetInactivity() {
    const self = this;
    inactivityTracker.resetInactivity(this.toggleMpin);
  }

  render() {
    return (
      <View>
        {this.props.screenProps.appHeader}
        <RenderSubHeader>{'FAQs'}</RenderSubHeader>
        <ScrollView>
          {faqList.map((detail, index) => (
            <View style={faqListStyle} key={index}>
              <RenderLabel type={'title'}>{detail.title}</RenderLabel>
              <RenderLabel type={'value'} style={faqDetailStyle}>{detail.content}</RenderLabel>
            </View>
        ))}
        </ScrollView>
      </View>
    );
  }
}

Faqs.propTypes = {
  dispatch: PropTypes.func.isRequired,
};


function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(null, mapDispatchToProps);

export default compose(
  withConnect,
)(Faqs);
