import { colors } from '../../configs/styleVars';

export const faqListStyle = {
  paddingTop: 5,
  paddingBottom: 10,
  paddingLeft: 10,
  borderColor: colors.borderLight,
  borderBottomWidth: 0.5,
};

export const faqDetailStyle = {
  paddingLeft: 10,
};
