import { colors,
    fonts,
    lineHeight,
    sizes } from '../../../configs/styleVars';

export const wrapperStyle = {
  backgroundColor: colors.white,
  flex: 1,
};

export const topWrapperStyle = {
  paddingHorizontal: sizes.appPaddingHorizontal,
  paddingBottom: 0,
  paddingTop: sizes.headerPadding,
};

export const headerWrap = {
  paddingVertical: 10,
};

export const headerStyle = {
  color: colors.white,
  fontSize: fonts.h4,
  lineHeight: lineHeight.h4,
  ...fonts.getFontFamilyWeight(),
};

export const payNowWraper = {
  paddingVertical: 10,
  flexDirection: 'row',
  alignItems: 'center',
};

export const amountStyle = {
  color: colors.white,
  fontSize: fonts.h1,
  lineHeight: lineHeight.h1,
  ...fonts.getFontFamilyWeight('bold'),
};

export const payNowBtnWrap = {
  backgroundColor: colors.white,
  borderRadius: 6,
  paddingVertical: 8,
  paddingHorizontal: 15,
  marginLeft: 15,
};

export const payNowBtn = {
  color: colors.primaryBGColor,
};

export const midViewWrap = {
  flexDirection: 'row',
  justifyContent: 'space-between',
};

export const tilesWrap = {
  flex: 1,
};

export const tilesHeader = {
  fontSize: fonts.body,
  lineHeight: lineHeight.body,
  color: colors.white,
  ...fonts.getFontFamilyWeight(),
};

export const tilesDesc = {
  fontSize: fonts.body,
  lineHeight: lineHeight.body,
  ...fonts.getFontFamilyWeight('bold'),
  color: colors.white,
};

export const progressWrap = {
  paddingVertical: 20,
};

export const bottomWrap = {
  paddingHorizontal: sizes.appPaddingHorizontal,
  flex: 1,
};

export const bottomTilesWrap = {
  flexDirection: 'row',
  flexWrap: 'wrap',
};

export const tileWrapper = {
  backgroundColor: colors.white,
  flexBasis: '50%',
};

export const tileBox = {
  backgroundColor: colors.primaryBGColor,
  marginVertical: 10,
  padding: 10,
  borderRadius: 6,
};

export const tileHeader = {
  fontSize: fonts.h4,
  lineHeight: lineHeight.h4,
  ...fonts.getFontFamilyWeight('bold'),
  color: colors.white,
};

export const tileDesc = {
  fontSize: fonts.description,
  lineHeight: lineHeight.description,
  color: colors.white,
  ...fonts.getFontFamilyWeight(),
};

export const tileImageWrap = {
  marginTop: 25,
  width: 38,
  height: 38,
};
