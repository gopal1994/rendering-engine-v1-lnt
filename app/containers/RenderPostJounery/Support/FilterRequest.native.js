import React from 'react';
// import PropTypes from 'prop-types';
import { View, Text, ScrollView, TouchableOpacity, Image } from 'react-native';
import { ModalOverlay } from '../../../components/RenderStaticComponents';
import RenderButton from '../../../components/RenderButton';
import * as styles from './styles.native';
import DropDown from '../../../components/DropDown/';
import Datepicker from '../../../components/Datepicker/';
import * as common from '../../../utilities/commonUtils';
import { strings } from '../../../../locales/i18n';

export default class CreateRequest extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {
      supportInfo: {},
      showCreateForm: false,
      title: '',
      status: '',
      dateRange: {
        from: '',
        to: '',
      },
      startDateError: '',
      endDateError: '',
    };
  }

  componentWillReceiveProps(newProps) {
    if (newProps && newProps.selectedFilters && newProps.selectedFilters !== this.props.selectedFilters) {
      this.setState({ ...newProps.selectedFilters });
    }
  }

  closeForm = () => {
    this.props.hideFilters(false);
  }

  handleSubjectChange = (option) => {
    this.setState({ title: option });
  }

  handleStatusChange = (option) => {
    this.setState({ status: option });
  }

  handleDateChange = (date, type) => {
    const newDateRange = {
      ...this.state.dateRange,
      [type]: date,
    };
    let errorType = null;
    if (type === 'to') {
      errorType = 'endDateError';
    } else {
      errorType = 'startDateError';
    }
    this.setState({ dateRange: newDateRange, [errorType]: '' });
  }

  checkDateRange = () => {
    const { dateRange } = this.state;
    if (dateRange.from && !dateRange.to) {
      this.setState({ endDateError: strings('support.startDateError') });
      return false;
    } else if (dateRange.to && !dateRange.from) {
      this.setState({ startDateError: strings('support.endDateError') });
      return false;
    }
    this.setState({ endDateError: '', startDateError: '' });
    return true;
  }

  submitForm = () => {
    if (this.checkDateRange()) {
      const { title, status, dateRange } = this.state;
      this.props.applyFilter({ title, status, dateRange: { from: dateRange.from || '', to: dateRange.to || '' } });
    }
  }

  render() {
    const { title, status, dateRange, startDateError, endDateError } = this.state;
    const { from, to } = dateRange;
    const fromDate = from ? new Date(common.unformatDateStr(from)) : null;
    const toDate = to ? new Date(common.unformatDateStr(to)) : null;
    const { showFilters, subjectOptions, statusOptions } = this.props;
    const currentDate = new Date();
    console.log('filter props', this.props);
    const startDateProps = {
      label: strings('support.filterFrom'),
      date: new Date(),
      maximumDate: new Date(),
    };

    const endDateProps = {
      label: strings('support.filterTo'),
      date: new Date(),
    };

    return (
      <ModalOverlay
        onBack={() => this.closeForm()}
        isVisible={showFilters}
        headerType={'white'}
        onModalHide={this.onModalHide}
      >
        <View style={styles.formModalWrap}>
          <View style={styles.modalTop}>
          </View>
          <ScrollView
            style={styles.formScrollableView}
            contentContainerStyle={styles.formWrapper}
          >
            {subjectOptions
            ? <DropDown
              {...subjectOptions}
              value={title.value}
              onChangeHandler={this.handleSubjectChange}
            />
            : null
            }
            {statusOptions
            ? <DropDown
              {...statusOptions}
              value={status.value}
              onChangeHandler={this.handleStatusChange}
            />
            : null
            }
            <View>
              <Datepicker
                {...startDateProps}
                date={fromDate}
                maximumDate={toDate || currentDate}
                error={startDateError}
                onChangeHandler={(date) => this.handleDateChange(date, 'from')}
              />
              <Datepicker
                {...endDateProps}
                minimumDate={fromDate}
                maximumDate={currentDate}
                date={toDate}
                error={endDateError}
                onChangeHandler={(date) => this.handleDateChange(date, 'to')}
              />
            </View>
          </ScrollView>
          <View>
            <View style={styles.ctaButtonWrap}>
              <RenderButton
                type={'primary'}
                text={strings('support.filterCta')}
                onPress={this.submitForm}
              />
            </View>
          </View>
        </View>
      </ModalOverlay>
    );
  }
}
