import React from 'react';
// import PropTypes from 'prop-types';
import { View, Text, ScrollView, TouchableOpacity, Image } from 'react-native';
import { ModalOverlay } from '../../../components/RenderStaticComponents';
import RenderButton from '../../../components/RenderButton';
import * as styles from './styles.native';
import DropDown from '../../../components/DropDown/';
import TextInputField from '../../../components/TextInputField';
import * as validationFns from '../../../utilities/validations/validationFns';
import { strings } from '../../../../locales/i18n';
import RenderPopup from '../../../components/RenderPopup';

export default class CreateRequest extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {
      supportInfo: {},
      showCreateForm: false,
      subject: '',
      body: '',
      subjectErrorObj: {},
      bodyErrorObj: {},
    };
  }

  closeForm = () => {
    this.props.toggleCreateForm(false);
  }

  onModalHide = () => {
    this.setState({ subject: '', body: '' });
  }

  handleDropDownChange = (option) => {
    const subjectErrorObj = validationFns.getValidationObj('isRequired', option.value);
    this.setState({ subject: option.value, subjectErrorObj });
  }

  onInputChangeHandler = (e) => {
    const bodyErrorObj = validationFns.getValidationObj('isRequired', e.nativeEvent.text);
    this.setState({ body: e.nativeEvent.text, bodyErrorObj });
  }

  validateData = () => {
    const { subject, body } = this.state;
    const subjectErrorObj = validationFns.getValidationObj('isRequired', subject);
    const bodyErrorObj = validationFns.getValidationObj('isRequired', body);
    this.setState({ subjectErrorObj, bodyErrorObj }, this.submitForm);
  }

  submitForm = () => {
    const { subject, body, subjectErrorObj, bodyErrorObj } = this.state;
    if (subjectErrorObj.isValid && bodyErrorObj.isValid) {
      this.props.submitForm({ subject, body });
    }
  }

  render() {
    const { subject, subjectErrorObj, bodyErrorObj } = this.state;
    const { showCreateForm, popupData } = this.props;
    const dropDownProps = {
      label: strings('support.requestSupport'),
      options: [{
        value: 'Payment',
        name: strings('support.subjectPayment'),
      }, {
        value: 'Disbursement',
        name: strings('support.subjectDisbursement'),
      }, {
        value: 'Others',
        name: strings('support.subjectOthers'),
      }],
    };

    const descriptionProps = {
      label: strings('support.requestDesc'),
      characterRestriction: 1000,
      maxLength: 1000,
      height: 100,
      multiline: true,
    };
    return (
      <ModalOverlay
        onBack={() => this.closeForm()}
        isVisible={showCreateForm}
        headerType={'white'}
        onModalHide={this.onModalHide}
      >
        <View style={styles.formModalWrap}>
          <View style={styles.modalTop}>
            <View style={styles.modalHeader}>
              <Text style={styles.modalTitle}>{strings('support.requestModalTitle')}</Text>
            </View>
          </View>
          <ScrollView
            style={styles.formScrollableView}
            contentContainerStyle={styles.formWrapper}
          >
            <DropDown
              {...dropDownProps}
              value={subject}
              error={subjectErrorObj.errorMessage}
              onChangeHandler={this.handleDropDownChange}
            />
            <TextInputField
              {...descriptionProps}
              error={bodyErrorObj.errorMessage}
              onChange={this.onInputChangeHandler}
            />
          </ScrollView>
          <View>
            <View style={styles.ctaButtonWrap}>
              <RenderButton
                type={'primary'}
                text={strings('support.requestModalCta')}
                onPress={this.validateData}
              />
            </View>
          </View>
        </View>
        <RenderPopup {...popupData} />
      </ModalOverlay>
    );
  }
}
