/**
 *
 * RenderPostJounery
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import RenderPostJourneyComponent from './RenderPostJourneyComponent';
import { createStructuredSelector } from 'reselect';
import { makeSelectUserDetails, makeSelectUserLanguage, makeSelectCurrentScreen, makeSelectDashboardData } from '../AppDetails/selectors';
import * as appActions from '../AppDetails/actions';

export class RenderPostJounery extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <RenderPostJourneyComponent {...this.props} />
    );
  }
}

RenderPostJounery.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  userDetails: makeSelectUserDetails(),
  userLanguage: makeSelectUserLanguage(),
  currentScreen: makeSelectCurrentScreen(),
  dashboardData: makeSelectDashboardData(),
});

function mapDispatchToProps(dispatch) {
  return {
    getRequest: (reqObject) => dispatch(appActions.getRequest(reqObject)),
    postRequest: (reqObject) => dispatch(appActions.postRequest(reqObject)),
    setPopupData: (data) => dispatch(appActions.setPopupData(data)),
    setUserDetails: (data) => dispatch(appActions.setUserDetails(data)),
    setDashboardData: (data) => dispatch(appActions.setDashboardData(data)),
    navigateToScreen: (screen, params) => dispatch(appActions.navigateToSCreen(screen, params)),
    dispatch,
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export default compose(
  withConnect,
)(RenderPostJounery);
