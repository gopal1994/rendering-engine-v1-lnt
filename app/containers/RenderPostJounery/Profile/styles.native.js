import { fonts,
    sizes,
    lineHeight,
    colors } from '../../../configs/styleVars';

export const wrapperStyle = {
  backgroundColor: colors.white,
  flex: 1,
};

export const topViewWrap = {
  backgroundColor: colors.primaryBGColor,
  paddingHorizontal: sizes.appPaddingHorizontal,
  paddingVertical: 15,
  paddingTop: 50,
  flexDirection: 'row',
  alignItems: 'center',
};

export const proImageStyle = {
  width: 80,
  height: 80,
  borderRadius: 40,
};

export const topDetailWrap = {
  marginLeft: 20,
};

export const topDetailLabel = {
  fontSize: fonts.h3,
  lineHeight: lineHeight.h3,
  color: colors.white,
  ...fonts.getFontFamilyWeight('bold'),
};

export const topDetailDesc = {
  ...fonts.getFontFamilyWeight(),
  fontSize: fonts.description,
  lineHeight: lineHeight.description,
  color: colors.white,
  paddingTop: 5,
};

export const bottomViewWrap = {
  flex: 1,
};

export const contentContainer = {
  paddingHorizontal: sizes.appPaddingHorizontal,
};
