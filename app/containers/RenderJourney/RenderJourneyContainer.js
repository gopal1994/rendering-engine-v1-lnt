/**
 *
 * RenderJourney
 *
 */

import React from 'react';
import RenderStep from 'containers/RenderStep/';

// import RaisedButton from 'material-ui/RaisedButton';

import { layoutConfig } from 'configs/appConfig';
import * as renderService from 'utilities/renderDataService';
import RenderPrebuiltStatic from '../../components/RenderPrebuiltStatic/';
import RenderJourneyAbstract from './RenderJourneyAbstract';
import RenderHeader from '../../components/RenderHeader/';
import RenderButton from '../../components/RenderButton/';
import RenderStepper from '../../components/RenderStepper/';
import RenderSideNav from '../../components/RenderSideNav/';

import JourneyWrapper from './JourneyWrapper';
import StepWrapper from './StepWrapper';

import Row from '../../components/Row';
import Column from '../../components/Column';

export default class RenderJourneyContainer extends RenderJourneyAbstract { // eslint-disable-line react/prefer-stateless-function

  render() {
    let renderStep;
    let headerBlock;
    let stepperBlock = null;
    let sideNavBlock = null;
    let submitButton = null;
    let backButton = null;
    let actionBlock = null;

    if (this.props.renderjourney && this.props.renderjourney.refreshFlag
      && this.props.renderjourney.stepData
      && this.props.renderjourney.screenId
      && this.props.renderjourney.fieldOrder
      && this.props.renderjourney.formData) {
      const stepperData = this.props.renderjourney.stepperData;
      if (stepperData && typeof (stepperData) === 'object' && stepperData.category) {
        const categoryLabels = stepperData.category;
        const currentActiveCategory = stepperData.currentActiveCategory;
        const defaultScreenList = stepperData.defaultScreenId;
        if (categoryLabels && categoryLabels.length) {
          // This is Stepper for the steps
          stepperBlock = (<RenderStepper
            labels={categoryLabels}
            currentActiveStep={currentActiveCategory}
            defaultScreenList={defaultScreenList}
            backTrigger={this.backTrigger}
          />);

          sideNavBlock = (<RenderSideNav
            labels={categoryLabels}
            currentActiveStep={currentActiveCategory}
            defaultScreenList={defaultScreenList}
            backTrigger={this.backTrigger}
          />);
        }
      }
      const applicationId = this.props.renderjourney.stepData.data.data.applicationId;
      if (layoutConfig.includeHeader) {
        headerBlock = (<RenderHeader stepData={this.props.renderjourney.stepperData} applicationId={applicationId} />);
      }

      const isLastStep = renderService.getLastStepFlag(this.props.renderjourney.screenId);
      const isSingleComponent = renderService.getSingleComponentFlag(this.props.renderjourney.screenId);
      const isPrebuiltStaticComponent = renderService.getPrebuiltStaticFlag(this.props.renderjourney.screenId);
        // If its not a single custom component or a prebuilt static (html) component
        // Then use the renderStep which uses basic unit components based on form_types
        // Else use the prebuilt/single components

      if (isSingleComponent) {
        const ScreenComponent = renderService.getComponentByScreenId(this.props.renderjourney.screenId);
        renderStep = (<ScreenComponent
          screenId={this.props.renderjourney.screenId}
          renderData={this.props.renderjourney.renderData}
          fieldOrder={this.props.renderjourney.fieldOrder}
          formData={this.props.renderjourney.formData}
          validateAndUpdateRender={this.validateAndUpdateRender}
          updateFormData={this.updateFormData}
        />);
      } else if (isPrebuiltStaticComponent) {
        renderStep = (<RenderPrebuiltStatic
          screenId={this.props.renderjourney.screenId}
          renderData={this.props.renderjourney.renderData}
          fieldOrder={this.props.renderjourney.fieldOrder}
          formData={this.props.renderjourney.formData}
          validateAndUpdateRender={this.validateAndUpdateRender}
          updateFormData={this.updateFormData}
        />);
      } else {
          // TODO: All the below props should be a direct substate call for render journey
          // Example: in mapStateToProps
          // formData: makeSelectFormData()
          // And to be used below as: formData:{this.props.formData}
        renderStep = (<RenderStep
          screenId={this.props.renderjourney.screenId}
          renderData={this.props.renderjourney.renderData}
          fieldOrder={this.props.renderjourney.fieldOrder}
          formData={this.props.renderjourney.formData}
          validateAndUpdateRender={this.validateAndUpdateRender}
          updateFormData={this.updateFormData}
          setRenderData={this.props.setRenderData}
        />);
      }

      submitButton = (<RenderButton
        type="primary"
        onClick={this.submitStep}
        label="Next"
      />);

      if (this.props.renderjourney.completedTasks && this.props.renderjourney.completedTasks.length) {
        backButton = (<RenderButton
          type="secondary"
          onClick={() => this.backTrigger()}
          label="Back"
        />);
      }

      if (!isLastStep) {
        actionBlock = (<div className="row center-align">
          {backButton}
          {submitButton}
        </div>);
      }
    }

    const isSideNavVisible = sideNavBlock && layoutConfig.isSideNavShown;
    const isStepperVisible = stepperBlock && layoutConfig.isStepperShown;

    const sideNavWrapper = isSideNavVisible ? (<Column colWidth={3}> {sideNavBlock} </Column>) : null;
    const stepperBlockWrapper = isStepperVisible ? (<Row>{stepperBlock}</Row>) : null;

    const journeyWrapperColSize = sideNavWrapper ? 9 : 12;
    return (
      <JourneyWrapper>
        {stepperBlockWrapper}
        <Row>
          {sideNavWrapper}
          <Column colWidth={journeyWrapperColSize} style={{ boxShadow: '0px 0px 20px rgba(0,0,0,0.3)' }}>
            {headerBlock}
            <StepWrapper>
              {renderStep}
            </StepWrapper>
            {actionBlock}
          </Column>
        </Row>
      </JourneyWrapper>
    );
  }
}
