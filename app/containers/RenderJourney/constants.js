/*
 *
 * RenderJourney constants
 *
 */

export const DEFAULT_ACTION = 'app/RenderJourney/DEFAULT_ACTION';
export const INIT_RENDER_SERVICE = 'app/RenderJourney/INIT_RENDER_SERVICE';

export const SET_STEP_DATA = 'app/RenderJourney/SET_STEP_DATA';
export const SET_STEPPER_DATA = 'app/RenderJourney/SET_STEPPER_DATA';

export const SET_RENDER_DATA = 'app/RenderJourney/SET_RENDER_DATA';
export const SET_FIELD_ORDER = 'app/RenderJourney/SET_FIELD_ORDER';

export const SET_SCREEN_NAME = 'app/RenderJourney/SET_SCREEN_NAME';
export const SET_SCREEN_ID = 'app/RenderJourney/SET_SCREEN_ID';
export const SET_APPLICATION_ID = 'app/RenderJourney/SET_APPLICATION_ID';
export const SET_FORM_DATA = 'app/RenderJourney/SET_FORM_DATA';
export const SET_STEP_VALID = 'app/RenderJourney/SET_STEP_VALID';

export const SET_COMPLETED_TASKS = 'app/RenderJourney/SET_COMPLETED_TASKS';
export const CLEAR_JOURNEY_DATA = 'app/RenderJourney/CLEAR_JOURNEY_DATA';

export const SET_REFRESH_FLAG = 'app/RenderJourney/SET_REFRESH_FLAG';
export const SET_SUBJOURNEY_ID = 'app/RenderJourney/SET_SUBJOURNEY_ID';
export const CLEAN_FORM_DATA = 'app/RenderJourney/CLEAN_FORM_DATA';
export const UPDATE_FORM_ELM_AND_RENDER = 'app/RenderJourney/UPDATE_FORM_ELM_AND_RENDER';
export const VALIDATE_FORM_AND_UPDATE_RENDER = 'app/RenderJourney/VALIDATE_FORM_AND_UPDATE_RENDER';
export const VALIDATE_ELEMENT_AND_UPDATE_RENDER = 'app/RenderJourney/VALIDATE_ELEMENT_AND_UPDATE_RENDER';
export const SUBMIT_FORM = 'app/RenderJourney/SUBMIT_FORM';
export const INITIALIZE_AND_UPDATE_RENDER = 'app/RenderJourney/INITIALIZE_AND_UPDATE_RENDER';
export const CHECK_HIDDEN_AND_RENDER = 'app/RenderJourney/CHECK_HIDDEN_AND_RENDER';
export const INITIATE_JOURNEY = 'app/RenderJourney/INITIATE_JOURNEY';
export const GOTO_BACK_TASK = 'app/RenderJourney/GOTO_BACK_TASK';

