import appVars from '../../configs/styleVars';
import styled from 'styled-components';
const colors = appVars.colors,
  sizes = appVars.sizes;

const JourneyWrapper = styled.div`
  /* padding: 0 20px;
  width: ${sizes.containerWidth};*/
  margin: 0 auto;
  margin-top: 10px;
`;

export default JourneyWrapper;
