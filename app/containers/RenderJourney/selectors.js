import { createSelector } from 'reselect';

/**
 * Direct selector to the renderJourney state domain
 */
const selectRenderJourneyDomain = (state) => state.get('renderJourney');

/**
 * Other specific selectors
 */


/**
 * Default selector used by RenderJourney
 */

const makeSelectRenderJourney = () => createSelector(
  selectRenderJourneyDomain,
  (substate) => substate.toJS()
);

const makeSelectRenderData = () => createSelector(
  selectRenderJourneyDomain,
  (substate) => substate.get('renderData')
);

const makeSelectFormData = () => createSelector(
  selectRenderJourneyDomain,
  (substate) => substate.get('formData')
);

const makeSelectFieldOrder = () => createSelector(
  selectRenderJourneyDomain,
  (substate) => substate.get('fieldOrder')
);

const makeSelectStepperData = () => createSelector(
  selectRenderJourneyDomain,
  (substate) => substate.get('stepperData')
);

const makeSelectScreenId = () => createSelector(
  selectRenderJourneyDomain,
  (substate) => substate.get('screenId')
);

const makeSelectIsStepValid = () => createSelector(
  selectRenderJourneyDomain,
  (substate) => substate.get('isStepValid')
);

export default makeSelectRenderJourney;
export {
  selectRenderJourneyDomain,
  makeSelectRenderData,
  makeSelectFormData,
  makeSelectFieldOrder,
  makeSelectStepperData,
  makeSelectScreenId,
  makeSelectIsStepValid,
};
