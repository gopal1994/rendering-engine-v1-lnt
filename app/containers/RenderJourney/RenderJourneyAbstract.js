/**
 *
 * RenderJourney
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import * as inactivityTracker from '../../utilities/inactivityTracker';
import { appConst, userConfig } from '../../configs/appConfig';

export default class RenderJourneyAbstract extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  componentDidMount() {
    if (this.props.journeyType !== 'POST_JOURNEY') {
      if (userConfig.runAsPreviewForm) {
        // When Code is run in preview mode in UI editor 
        // No need to make the API call to get the renderData 
        // - use the provided formRenderingJson which is already in form ofr returned resposne from an API
        this.props.initializeAndUpdateRender(this.props.formRenderingJson);
      } else {
        this.triggerInitiate();
      }
    }
  }

  componentWillUnmount() {
    inactivityTracker.clearTracker();
  }

  toggleMpin = () => {
    if (this.props.screenProps && this.props.screenProps.toggleMpin) {
      this.props.screenProps.toggleMpin(false);
    }
  }

  // logs out user in case of inactivity for x min
  resetInactivity = () => {
    inactivityTracker.resetInactivity(this.toggleMpin);
  }

  scrollToTop = () => {
    window.scrollTo(0, 0);
  }

  // validate the single field ==> validate the step
  // Validation result should be available in renderData[elmId]
  // so the errorMessage and valid/invalid state could be represented
  validateAndUpdateRender = (value, elmId) => {
    this.props.validateElmAndUpdateRender(value, elmId);
  }

  // Update formData for the step
  updateFormData = (formKey, value) => {
    this.props.updateFormElmAndRender(formKey, value);
    this.resetInactivity();
  }

  triggerInitiate = (showLastStepScreen) => {
    const successCb = () => {
      this.resetInactivity();
    };
    this.props.initiateJourney(successCb, { showLastStepScreen });
  }

  // Submit the current step
  // Modify the form/step data with returned response
  submitStep = () => {
    const successCb = () => {
      this.resetInactivity();
      this.scrollToTop();
    };
    this.props.submitForm(successCb);
  }

  backTrigger = (screenKey) => {
    const successCb = () => {
      this.resetInactivity();
      this.scrollToTop();
    };
    this.props.gotoBackTask(screenKey, successCb);
  }

  render() {
    return null;
  }
}

RenderJourneyAbstract.propTypes = {
  journeyType: PropTypes.string,
  validateElmAndUpdateRender: PropTypes.func.isRequired,
  updateFormElmAndRender: PropTypes.func.isRequired,
  initiateJourney: PropTypes.func.isRequired,
  submitForm: PropTypes.func.isRequired,
  gotoBackTask: PropTypes.func.isRequired,
  screenProps: PropTypes.object,
};
