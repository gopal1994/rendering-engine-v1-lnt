import appVars from '../../configs/styleVars';
import styled from 'styled-components';
const colors = appVars.colors;

const StepWrapper = styled.div`
  padding: 20px;
  background-color: ${colors.stepWrapperBGColor};
  border-radius: 5px;
  margin: 0 auto 20px;
`;

export default StepWrapper;
