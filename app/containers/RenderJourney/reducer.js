/*
 *
 * RenderJourney reducer
 *
 */

import { fromJS } from 'immutable';
import * as C from './constants';

const initialState = fromJS({
  stepData: null,
  stepperData: null,
  renderData: null,
  fieldOrder: null,
  screenName: null,
  screenId: null,
  applicationId: null,
  formData: null,
  isStepValid: null,
  completedTasks: [],
  refreshFlag: false,
  subJourneyId: null,
});

function renderJourneyReducer(state = initialState, action) {
  switch (action.type) {
    case C.DEFAULT_ACTION:
      return state;
    case C.SET_STEP_DATA:
      return state
        .set('stepData', action.data);
    case C.SET_STEPPER_DATA:
      return state
        .set('stepperData', action.data);
    case C.SET_RENDER_DATA:
      return state
        .set('renderData', action.data);
    case C.SET_FIELD_ORDER:
      return state
        .set('fieldOrder', action.data);
    case C.SET_SCREEN_NAME:
      return state
        .set('screenName', action.data);
    case C.SET_SCREEN_ID:
      return state
        .set('screenId', action.data);
    case C.SET_COMPLETED_TASKS:
      return state
        .set('completedTasks', action.data);
    case C.SET_APPLICATION_ID:
      return state
        .set('applicationId', action.data);
    case C.SET_FORM_DATA:
      return state
        .set('formData', action.data);
    case C.SET_STEP_VALID:
      return state
        .set('isStepValid', action.data);
    case C.CLEAR_JOURNEY_DATA:
      return state
        .set('stepData', null)
        .set('formData', null)
        .set('fieldOrder', null)
        .set('subJourneyId', null);
    case C.SET_REFRESH_FLAG:
      return state
        .set('refreshFlag', action.data);
    case C.SET_SUBJOURNEY_ID:
      return state
        .set('subJourneyId', action.data);
    default:
      return state;
  }
}

export default renderJourneyReducer;
