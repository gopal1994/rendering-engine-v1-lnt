import { Dimensions } from 'react-native';
import styleVars from '../../configs/styleVars';
const { colors, fonts } = styleVars;

export const btnWrapper = {
  style: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    backgroundColor: colors.white,
    marginTop: 5,
  },
};

export const mainWrapperStyle = {
  style: {
    flex: 1,
    padding: 0,
    margin: 0,
    backgroundColor: colors.journeyWrapperBGColor,
    justifyContent: 'space-between',
  },
};

export const stepperWrapper = {
  backgroundColor: colors.journeyWrapperBGColor,
};

export const stepWrapperStyle = {
  contentContainerStyle: {
    backgroundColor: colors.stepWrapperBGColor,
    borderRadius: 5,
  },
};

export const subStepWrapperStyle = {
  style: {
    backgroundColor: colors.stepWrapperBGColor,
    borderRadius: 5,
    padding: 10,
    flex: 1,
  },
};

export const formWrapper = {
  style: {
    paddingHorizontal: 20,
  },
};

export const getButtonStyle = (type, enableState) => {
  const buttonStyle = {
    buttonStyle: {
      borderColor: 'transparent',
      borderWidth: 0,
      backgroundColor: (type === 'back') ? 'transparent' : colors.primaryBGColor,
      padding: 0,
      margin: 0,
      elevation: 0,
    },
    textStyle: {
      color: (type === 'back') ? colors.primaryBGColor : colors.white,
      fontSize: fonts.body,
      ...fonts.getFontFamilyWeight(),
    },
    containerStyle: {
      margin: 10,
      flex: 1,
      alignItems: (type === 'back') ? 'flex-start' : 'flex-end',
    },
  };

  return buttonStyle;
};

export const getBtnIconStyle = (enableState = true) => {
  const iconStyles = {
    size: 15,
    color: (enableState) ? colors.primaryFontColor : colors.primaryDisableColor,
  };
  return iconStyles;
};

export const btnBlockInScrollStyles = {
  backgroundColor: colors.journeyWrapperBGColor,
  paddingTop: 5,
};
