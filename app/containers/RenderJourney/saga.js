// import { take, call, put, select } from 'redux-saga/effects';

import { put, select, takeEvery, take } from 'redux-saga/effects';
import * as C from './constants';
import * as A from '../AppDetails/constants';
import * as renderActions from './actions';
import * as renderService from '../../utilities/renderDataService';

import * as appActions from '../AppDetails/actions';
import * as appDetailsSelector from '../AppDetails/selectors';
import * as renderJourneySelector from './selectors';
import { getTranslation } from '../../utilities/localization';
import { appConst } from '../../configs/appConfig';
import * as validationFns from '../../utilities/validations/validationFns';
import { getCleanedFormData, getValidatedRenderData, getBackTaskScreenId } from './RenderJourneyHelper';

// Individual exports for testing
export default function* defaultSaga() {
  yield takeEvery(C.INIT_RENDER_SERVICE, renderServiceFn);
  yield takeEvery(C.CLEAN_FORM_DATA, cleanFormData);
  yield takeEvery(C.UPDATE_FORM_ELM_AND_RENDER, updateFormData);
  yield takeEvery(C.SUBMIT_FORM, submitForm);
  yield takeEvery(C.VALIDATE_FORM_AND_UPDATE_RENDER, validateAndUpdateRender);
  yield takeEvery(C.VALIDATE_ELEMENT_AND_UPDATE_RENDER, validateElmAndUpdateRender);
  yield takeEvery(C.INITIALIZE_AND_UPDATE_RENDER, initializeAndUpdateRender);
  yield takeEvery(C.CHECK_HIDDEN_AND_RENDER, checkHiddenAndRender);
  yield takeEvery(C.INITIATE_JOURNEY, initiateJourney);
  yield takeEvery(C.GOTO_BACK_TASK, gotoBackTask);
}

export function* gotoBackTask(action) {
  const { screenKey, successCb } = action;
  const userDetails = yield select(appDetailsSelector.makeSelectUserDetails());
  const stepperData = yield select(renderJourneySelector.makeSelectStepperData());
  const backTaskList = stepperData && stepperData.backTaskList;
  const backScreenKey = getBackTaskScreenId(screenKey, backTaskList);
  const reqData = {
    params: {
      currentTaskId: userDetails.taskId,
      processInstanceId: userDetails.processInstanceId,
      backTaskDefinitionKey: backScreenKey,
    },
    userDetails,
    successAction: renderActions.initializeAndUpdateRender,
    successCb,
  };
  if (backScreenKey) {
    yield put(appActions.postRequest({ key: 'backScreenTrigger', data: reqData }));
  } else {
    window.logger.warn('SAGA', `Invalid back task called ${screenKey}`, { backTaskList });
  }
}

export function* validateAndUpdateRender() {
  const formData = yield select(renderJourneySelector.makeSelectFormData());
  const renderData = yield select(renderJourneySelector.makeSelectRenderData());
  const fieldOrder = yield select(renderJourneySelector.makeSelectFieldOrder());
  const validatedRenderData = getValidatedRenderData(formData, renderData, fieldOrder);
  const isStepInvalid = Object.keys(formData).some((elmId) => validatedRenderData[elmId] && !validatedRenderData[elmId].isValid);
  yield put(renderActions.setIsStepValid(!isStepInvalid));
  yield put(renderActions.setRenderData(validatedRenderData));
}

export function* validateElmAndUpdateRender(action) {
  const { value, elmId } = action;
  const renderData = yield select(renderJourneySelector.makeSelectRenderData());
  const formData = yield select(renderJourneySelector.makeSelectFormData());
  const validObj = validationFns.validateElm(renderData[elmId], value, formData);
  const currentRenderData = { ...renderData };
  // Set errorMessage and invalid flag
  currentRenderData[elmId].isValid = validObj.isValid;
  currentRenderData[elmId].errorMessage = validObj.errorMessage;
  yield put(renderActions.setRenderData(currentRenderData));
}

export function* initiateJourney(action) {
  const { options, successCb } = action;
  const userDetails = yield select(appDetailsSelector.makeSelectUserDetails());
  const reqData = {
    params: { journeyName: appConst.journeyName },
    userDetails,
    successCb,
    successAction: (response) => {
      const actionObj = renderActions.initializeAndUpdateRender(response);
      actionObj.response = { ...response, showLastStepScreen: options.showLastStepScreen };
      return actionObj;
    },
  };
  yield put(appActions.getRequest({ key: 'initiate', data: reqData }));
}

export function* checkHiddenAndRender() {
  const fData = yield select(renderJourneySelector.makeSelectFormData());
  const rData = yield select(renderJourneySelector.makeSelectRenderData());
  const fOrder = yield select(renderJourneySelector.makeSelectFieldOrder());
  const { renderData, formData } = renderService.updateHiddenFields({ ...rData }, { ...fData }, [...fOrder]);
  yield put(renderActions.setRenderData(renderData));
  yield put(renderActions.setFormData(formData));
}

export function* submitForm(action) {
  const { successCb } = action;
  yield put(renderActions.cleanFormData());
  yield put(renderActions.validateAndUpdateRender());
  yield take(C.SET_STEP_VALID); // Wait for validation/VALIDATE_FORM_AND_UPDATE_RENDER to complete
  const formData = yield select(renderJourneySelector.makeSelectFormData());
  const screenId = yield select(renderJourneySelector.makeSelectScreenId());
  const isStepValid = yield select(renderJourneySelector.makeSelectIsStepValid());
  const userDetails = yield select(appDetailsSelector.makeSelectUserDetails());
  const reqData = {
    params: {
      taskId: userDetails.taskId,
      processInstanceId: userDetails.processInstanceId,
      formProperties: { ...formData },
    },
    screenId,
    userDetails,
          // Setup the render Data in the app
      // Call the render saga service
    successAction: renderActions.initializeAndUpdateRender,
    successCb,
  };
  if (isStepValid) {
    yield put(appActions.postRequest({ key: 'submitForm', data: reqData }));
  }
}

export function* initializeAndUpdateRender(action) {
  const { response } = action;
  yield put(renderActions.initRenderService(response));
  yield take(A.SET_USER_DETAILS); // waiting for init render service call all sagas.
  yield put(renderActions.checkHiddenAndRender());
}

export function* cleanFormData() {
  const formData = yield select(renderJourneySelector.makeSelectFormData());
  const renderData = yield select(renderJourneySelector.makeSelectRenderData());
  const cleanedFormData = getCleanedFormData(formData, renderData);
  yield put(renderActions.setFormData(cleanedFormData));
}

export function* updateFormData(action) {
  const { formKey, value } = action;
  const renderData = yield select(renderJourneySelector.makeSelectRenderData());
  const formData = yield select(renderJourneySelector.makeSelectFormData());
  const fOrder = yield select(renderJourneySelector.makeSelectFieldOrder());
  // if (formData[formKey] === value) {
  //   return;
  // }
  let updatedFormData;
  let updatedData;
  const meta = renderData[formKey] && renderData[formKey].metaData;
  updatedFormData = { ...formData, [formKey]: value };
  // check for hide conditions for any form elements if exists
  updatedData = renderService.updateHiddenFields({ ...renderData}, { ...updatedFormData}, [...fOrder]);
  yield put(renderActions.setRenderData(updatedData.renderData ));
  yield put(renderActions.setFormData(updatedData.formData));
}

export function* renderServiceFn(action) {
  // This saga is called when we get the screen details response from initiate or submit-form

  yield put(renderActions.setRefreshFlag(false));

  // set modified renderData
  const stepData = action.data;
  yield put(renderActions.setStepData(stepData));

  // set stepper data - completed/Active steps
  let isJourneyCompleted = null;
  if (stepData.data) {
    const stepperData = !!stepData.data.stepperData && getTranslation(stepData.data.stepperData);
    window.logger.info('DATA', 'Stepper Data', stepperData);
    yield put(renderActions.setStepperData(stepperData));
    // set render data with metaData for the current screen
    // Contains all the fields' json with order to be rendered in
    const renderData = renderService.buildRenderData(action.data);
    window.logger.info('DATA', 'Render Journey Data', renderData);
    yield put(renderActions.setRenderData(renderData));

    // set the ordering of the fields to be rendered in a step
    const fieldOrder = renderService.getFieldOrder(renderData);
    // window.logger.info('DATA', 'Field Order', fieldOrder);
    yield put(renderActions.setFieldOrder(fieldOrder));

    // set current screen name
    yield put(renderActions.setCurrentScreenName(stepData.screenName));

    // set current screen Id
    yield put(renderActions.setCurrentScreenId(stepData.screenInfo));

    // set Completed Tasks List Id
    // completedTasks would be an array of completed tasks by the user
    const completedTasks = stepperData && stepperData.backTaskList;
    yield put(renderActions.setCompletedTasks(completedTasks));

    // set application Id
    yield put(renderActions.setApplicationId(stepData.data.data && stepData.data.data.applicationId));

    // set form data to submit
    if (renderData && fieldOrder) {
      const formData = renderService.buildFormData(renderData, fieldOrder);
      yield put(renderActions.setFormData(formData));
    }

    // set user details with taskId and processInstance Id
    // build the userDetails object and trigger the app-details action to set the user details
    isJourneyCompleted = stepData.showLastStepScreen ? null : renderService.getLastStepFlag(stepData.screenInfo);
    const userDetailsObj = yield select(appDetailsSelector.makeSelectUserDetails());
    yield put(appActions.setUserDetails({
      ...userDetailsObj,
      taskId: stepData.data.taskId,
      processInstanceId: stepData.data.processInstanceId,
      isJourneyCompleted,
    }));
  } else {
    // fake journey complete condition for redirection to listView.
    // completing journey here, resetting the journey name too
    const userDetailsObj = yield select(appDetailsSelector.makeSelectUserDetails());
    yield put(appActions.setUserDetails({
      ...userDetailsObj,
      taskId: null,
      processInstanceId: null,
      journeyName: null,
      isJourneyCompleted,
    }));
  }
  yield put(renderActions.setRefreshFlag(true));
}
