/**
 *
 * AppDetails Native
 *
 */

import React from 'react';

import AppDetailsAbstract from './AppDetailsAbstract.js';
import * as userUtils from '../../utilities/userUtils';
import Login from '../Login/';
import RenderJourney from '../RenderJourney/';
import AppHeader from '../../components/AppHeader/';
import Loader from '../../components/Loader';

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import materialUiTheme from '../../configs/materialUiTheme';
import getMuiTheme from 'material-ui/styles/getMuiTheme';

import { globalColors, colors, brand } from '../../configs/styleVars';
import { appConst, userConfig } from '../../configs/appConfig';
import RenderPopup from '../../components/RenderPopup';
import AppWrapper from './AppWrapperStyle';
import * as config from '../../configs/configHelper';



export default class AppDetailsContainer extends AppDetailsAbstract { // eslint-disable-line react/prefer-stateless-function

  init = () => {
    let userDetails;
    const self = this;
    userUtils.getSessionUserDetails().then((userObj) => {
      self.setState({ initiateScreen: true });
      userDetails = userObj;
      // If the journey is being run through Plugin
      // Then use the userDetails passed to it
      if (userConfig.runAsPlugin) {
        userDetails.processInstanceId = self.props.processInstanceId ? self.props.processInstanceId : undefined;
        userDetails.emailId = self.props.userId ? self.props.userId : undefined;
        userDetails.bpm_token = self.props.bpm_token ? self.props.bpm_token : undefined;
        appConst.journeyName = self.props.journeyName || appConst.journeyName;
        config.BASE_API_URL = self.props.baseUrl || config.BASE_API_URL;
      }
      self.props.setUserDetails(userDetails);
    });
  }

  render() {
    let renderBlock;
    const isLoggedin = this.props.userDetails && this.props.userDetails.bpm_token;
    const headerProps = {
      isLoggedin,
      hideLangBlock: true,
      setUserDetails: this.props.setUserDetails,
      clearJourneyData: this.props.clearJourneyData,
      updateUserLanguage: this.props.setUserLanguage,
      dispatch: this.props.dispatch,
      logout: this.props.logout,
    };

    if (!isLoggedin) {
      renderBlock = (<Login />);
    } else {
      // formRenderingJson is used only when code is running in form-preview+plugin mode (right now only used in UI editor)
      renderBlock = (<RenderJourney formRenderingJson={{...this.props.formRenderingJson}}/>);
    }
    return (
      <MuiThemeProvider muiTheme={getMuiTheme(materialUiTheme)}>
        <AppWrapper id="ku-lendin-journey-container">
          <div className="">
            <Loader loaderStatus={this.props.loadingState} />
            <RenderPopup
              {...this.props.popupData}
            />
            {
              !userConfig.runAsPlugin ?
               (<AppHeader {...headerProps} />) : ''
            }
            {renderBlock}
          </div>
        </AppWrapper>
      </MuiThemeProvider>
    );
  }
}
