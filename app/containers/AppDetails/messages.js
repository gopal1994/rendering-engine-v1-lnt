/*
 * AppDetails Messages
 *
 * This contains all the text for the AppDetails component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.AppDetails.header',
    defaultMessage: 'This is AppDetails container !',
  },
});
