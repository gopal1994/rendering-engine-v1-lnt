/**
 *
 * AppDetails
 *
 */

import React from 'react';

import * as userUtils from '../../utilities/userUtils';
import * as sessionUtils from '../../utilities/sessionUtils';
import { currentLocale } from '../../utilities/localization';
import { languages, defaultLocale } from '../../configs/appConstants';
import * as inactivityTracker from '../../utilities/inactivityTracker';

export default class AppDetailsAbstract extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

  // TODO: merge touch ID and mpinConfirm screens
  constructor(props) {
    super(props);
    this.state = {
      initiateScreen: false,
      mpinConfirmed: false,
      setTouchID: false,
      useTouchID: true,
      getStarted: false,
      showScreen: false,
    };
    this.toggleMpin = this.toggleMpin.bind(this);
  }


  componentWillMount() {
    // If there exists information in session for user details then set it in the store
    // let userDetails = userUtils.getSessionUserDetails();
    // this.props.setUserDetails(userDetails);
    let userDetails;
    const self = this;
    this.init();
    sessionUtils.getSessionKey('userLanguage').then((userLanguage) => {
      let locale = currentLocale();
      const isLocaleSupported = languages.findIndex((langObj) => langObj.value === locale);
      if (isLocaleSupported !== -1) {
        locale = userLanguage;
      } else {
        locale = defaultLocale;
      }
      this.props.setUserLanguage(locale);
    });
  }

  toggleMpin = (value) => {
    if (value) {
      inactivityTracker.setDefaultCallback(() => this.toggleMpin(false));
    } else {
      inactivityTracker.clearTracker();
    }
    this.setState({ mpinConfirmed: value });
    this.props.clearJourneyData();
    this.props.clearCurrentScreen();
  }

  toggleAuthMethod = (value) => {
    this.setState({ useTouchID: value });
  }

  startJourney = () => {
    console.debug('inside the startJourney now');
    this.props.setUserDetails({
      ...this.props.userDetails,
      introCompleted: 'true',
    });
  }

  render() {
    return null;
  }
}
