/*
 *
 * AppDetails constants
 *
 */

export const DEFAULT_ACTION = 'app/AppDetails/DEFAULT_ACTION';

export const GET_REQUEST = 'app/AppDetails/GET_REQUEST';
export const POST_REQUEST = 'app/AppDetails/POST_REQUEST';
export const DELETE_REQUEST = 'app/AppDetails/DELETE_REQUEST';
export const SET_USER_DETAILS = 'app/AppDetails/SET_USER_DETAILS';
export const SET_USER_STORE = 'app/AppDetails/SET_USER_STORE';
export const LOGIN = 'app/AppDetails/LOGIN';
export const UPDATE_LOADING_STATE = 'app/AppDetails/UPDATE_LOADING_STATE';

export const SET_POPUP_DATA = 'app/AppDetails/SET_POPUP_DATA';
export const SET_SHOW_POPUP_STATE = 'app/AppDetails/SET_SHOW_POPUP_STATE';
export const CLEAR_JOURNEY_DATA = 'app/AppDetails/CLEAR_JOURNEY_DATA';
export const SET_USER_LANGUAGE = 'app/AppDetails/SET_USER_LANGUAGE';
export const UPDATE_USER_LANGUAGE = 'app/AppDetails/UPDATE_USER_LANGUAGE';
export const SET_DASHBOARD_DATA = 'app/AppDetails/SET_DASHBOARD_DATA';
export const SET_APP_ERROR = 'app/AppDetails/SET_APP_ERROR';
export const NAVIGATE_TO_SCREEN = 'app/AppDetails/NAVIGATE_TO_SCREEN';
export const CLEAR_CURRENT_SCREEN = 'app/AppDetails/CLEAR_CURRENT_SCREEN';
export const SET_APP_CONFIG = 'app/AppDetails/SET_APP_CONFIG';
export const DUMP_LOG = 'app/AppDetails/DUMP_LOG';
export const LOGOUT = 'app/AppDetails/LOGOUT';
