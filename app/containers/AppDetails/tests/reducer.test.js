
import { fromJS } from 'immutable';
import appDetailsReducer from '../reducer';

describe('appDetailsReducer', () => {
  it('returns the initial state', () => {
    expect(appDetailsReducer(undefined, {})).toEqual(fromJS({}));
  });
});
