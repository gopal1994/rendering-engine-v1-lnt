
import styled from 'styled-components';

// Remember -> we are importing the third party CSS files as text-only (see webpack.base.babel config)
// Because we want to keep those CSS files as scoped as possible to our journey container
import sanitize from 'sanitize.css/sanitize.css';
import material from 'materialize-css/dist/css/materialize.min.css';

const AppWrapper = styled.div`
	${sanitize}
	${material}
`;

export default AppWrapper;
