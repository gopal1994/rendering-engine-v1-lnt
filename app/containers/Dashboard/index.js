/**
 *
 * Dashboard
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Platform, Text, View, Image } from 'react-native';

import { connect } from 'react-redux';
import { compose } from 'redux';

import RenderSubHeader from '../../components/RenderSubHeader';
import RenderLabel from '../../components/RenderLabel';
import RenderDetailList from '../../components/RenderDetailList';
import * as inactivityTracker from '../../utilities/inactivityTracker';

import {
  detailsWrapperStyle,
  loanInfoStyle,
} from './style';

export class Dashboard extends React.Component { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);
    this.resetInactivity = this.resetInactivity.bind(this);
    this.state = {
      loanData: {},
    };
  }

  componentDidMount() {
    const reqObj = {
      key: 'loanData',
      data: {
        successCb: (response) => {
          this.setState({ loanData: response.data });
        },
        userDetails: this.props.screenProps.userDetails,
      },
    };
    this.props.screenProps.getRequest(reqObj);
    this.resetInactivity();
  }

  componentWillUnmount() {
    inactivityTracker.clearTracker();
  }

  toggleMpin = () => {
    this.props.screenProps.toggleMpin(false);
  }

  // logs out user in case of inactivity for x min
  resetInactivity() {
    const self = this;
    inactivityTracker.resetInactivity(this.toggleMpin);
  }

  getLoanDetails = () => {
    const { loanData } = this.state;
    const loanDetailsList = [
      {
        id: 'contactNumber',
        label: 'Contact Number',
        value: loanData.contractNumber,
      },
      {
        id: 'loanAmount',
        label: 'Loan Amount',
        value: loanData.loanAmount,
      },
      {
        id: 'tenure',
        label: 'Tenure',
        value: `${loanData.tenure} Months`,
      },
      {
        id: 'dueDate',
        label: 'Due Date',
        value: loanData.dueDate,
      },
      {
        id: 'monthlyInstallment',
        label: 'Monthly Installment',
        value: loanData.monthlyInstallment,
      },
      {
        id: 'overDue',
        label: 'Over Due Amount',
        value: loanData.overDueAmount,
      },
      {
        id: 'outstanding',
        label: 'Total Outstanding Amount',
        value: loanData.outstandingAmount,
      },
    ];
    return loanDetailsList;
  }
  render() {
    const loanDetailsList = this.getLoanDetails();
    return (
      <View>
        {this.props.screenProps.appHeader}
        <RenderSubHeader>{'Collect your loan amount from VN Post'}</RenderSubHeader>
        <View style={loanInfoStyle}>
          <RenderLabel type={'value'}>{'Your loan security code is'}</RenderLabel>
          <RenderLabel type={'title'}>{'5555'}</RenderLabel>
          <RenderLabel type={'value'}>{'Your code will expire in 15 days'}</RenderLabel>
        </View>
        <View style={loanInfoStyle}>
          <RenderLabel type={'title'}>{'Please collect the loan before code expires'}</RenderLabel>
        </View>
        <View style={detailsWrapperStyle}>
          <RenderSubHeader>{'Loan Details'}</RenderSubHeader>
          <RenderDetailList detailList={loanDetailsList} />
        </View>
      </View>
    );
  }
}

Dashboard.propTypes = {
  dispatch: PropTypes.func.isRequired,
};


function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(null, mapDispatchToProps);

export default compose(
  withConnect,
)(Dashboard);
