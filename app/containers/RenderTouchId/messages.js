/*
 * RenderTouchId Messages
 *
 * This contains all the text for the RenderTouchId component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.RenderTouchId.header',
    defaultMessage: 'This is RenderTouchId container !',
  },
});
