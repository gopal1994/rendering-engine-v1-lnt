import { colors, fonts, lineHeight } from '../../configs/styleVars';

export const wrapperStyles = {
  flex: 1,
  padding: 20,
};

export const topWrapperStyles = {
  flex: 1,
};

export const headingStyles = {
  color: colors.black,
  fontSize: fonts.h1,
  lineHeight: lineHeight.h1,
  textAlign: 'center',
  padding: 10,
  ...fonts.getFontFamilyWeight(),
};

export const subHeadStyles = {
  color: colors.black,
  fontSize: fonts.description,
  lineHeight: lineHeight.description,
  textAlign: 'center',
  padding: 10,
  ...fonts.getFontFamilyWeight(),
};

export const imageWrapperStyles = {
  justifyContent: 'center',
  alignItems: 'center',
};

export const imageStyles = {
  margin: 30,
};

export const descriptionStyles = {
  color: colors.labelColor,
  fontSize: fonts.description,
  lineHeight: lineHeight.description,
  textAlign: 'center',
  paddingHorizontal: 20,
};
