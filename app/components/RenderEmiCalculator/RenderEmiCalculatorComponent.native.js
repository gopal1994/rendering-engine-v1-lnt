/**
*
* RenderEmiCalculator
*
*/

import React from 'react';
import { View } from 'react-native';
import Text from '../RenderText';
import RenderEmiCalculatorAbstract from './RenderEmiCalculatorAbstract';
import RenderSlider from '../RenderSlider';
import RenderCheckbox from '../RenderCheckbox';
import RenderDropdown from '../RenderDropdown';
import RenderInputText from '../RenderInputText';
import * as styles from './RenderEmiCalculatorStyles';
import { layoutConfig, appConst } from '../../configs/appConfig';
import { strings } from '../../../locales/i18n';
import RenderPopupForm from '../RenderPopupForm/';
import * as common from '../../utilities/commonUtils';
import * as C from './constants';

import { Row, Column as Col, Grid } from 'react-native-responsive-grid';
import RenderRadio from '../RenderRadio';
import RenderInputHidden from '../RenderInputHidden';
import RenderAutoComplete from '../RenderAutoComplete';


export default class RenderEmiCalculatorComponent extends RenderEmiCalculatorAbstract { // eslint-disable-line react/prefer-stateless-function
  // TODO : remove rangeLabel from loanAmount. move it compProps
  render() {
    const formattedEMI = common.convertCurrencyFormat(Math.ceil(this.props.formData.emi / 100) * 100);
    const fieldOrder = [...this.props.fieldOrder];
    const renderData = { ...this.props.renderData };
    const roiValue = this.props.formData[C.roiKey] || appConst.roi;
    const emiComponent = (
      <View
        {...styles.emiBlock}
      >
        <Text {...styles.emiText}> {strings('renderEmiCal.totalMonthlyInstallment')}: </Text>
        <Text {...styles.emiValue}> {formattedEMI} </Text>
        <Text {...styles.emiText}> @ {roiValue}% per annum </Text>
        <Text {...styles.emiText}> {strings('renderEmiCal.disclaimer')} </Text>
      </View>
    );
    let loanAmountBlock = null,
      tenureBlock = null,
      loanRoiBlock = null,
      loanPurposeBlock = null,
      loanPurposeBlockOthersBlock = null,
      replaymentScheduleBlock = null,
      replaymentScheduleOthersBlock = null,
      securityOfferedBlock = null,
      securityOfferedOthersBlock = null,
      valueOfAssetBlock = null,
      newToCreditCustomerBlock = null,
      commentsOnSecurityBlock = null,
      schemeDescriptionBlock = null,
      productDescriptionBlock = null,
      schemeIdBlock = null,
      productIdBlock = null;


    if (renderData[C.loanAmountKey]) {
      loanAmountBlock = (<RenderSlider
        compProps={this.getComponentProps('slider', C.loanAmountKey)}
        elmId={C.loanAmountKey}
        onChangeHandler={(...params) => { this.onChangeHandler(...params);}}
        renderData={renderData[C.loanAmountKey]}
      />);
    }
    if (renderData[C.roiKey]) {
      loanRoiBlock = (<RenderInputText
        compProps={this.getComponentProps('number', C.roiKey)}
        elmId={C.roiKey}
        rangeLabel={strings('renderEmiCal.percentage')}
        onChangeHandler={this.onChangeHandler}
        renderData={renderData[C.roiKey]}
      />);
    }
    if (renderData[C.loanTenureKey]) {
      tenureBlock = (<RenderSlider
        compProps={this.getComponentProps('slider', C.loanTenureKey)}
        elmId={C.loanTenureKey}
        rangeLabel={strings('renderEmiCal.monthsLabel')}
        onChangeHandler={this.onChangeHandler}
        renderData={renderData[C.loanTenureKey]}
      />);
    }
    if (renderData[C.loanPurposeKey] && !renderData[C.loanPurposeKey].isHidden) {
      const colSize = renderData[C.loanPurposeKey].metaData.colWidth * (layoutConfig.defaultColumnSizeForMobile);
      loanPurposeBlock = (
        <Col size={colSize} style={{ ...styles.colWrapper }}>
          <RenderDropdown
            compProps={this.getComponentProps('dropdown', C.loanPurposeKey)}
            elmId={C.loanPurposeKey}
            onChangeHandler={this.onChangeHandler}
            renderData={renderData[C.loanPurposeKey]}
            formData={this.props.formData}
            updateFormData={this.props.updateFormData}
            toggleScroll={this.props.toggleScroll}
          />
        </Col>);
    }
    if (renderData[C.purposeOthersKey] && !renderData[C.purposeOthersKey].isHidden) {
      const colSize = renderData[C.purposeOthersKey].metaData.colWidth * (layoutConfig.defaultColumnSizeForMobile);
      loanPurposeBlockOthersBlock = (
        <Col size={colSize} style={{ ...styles.colWrapper }}>
          <RenderInputText
            compProps={this.getComponentProps('text', C.purposeOthersKey)}
            elmId={C.purposeOthersKey}
            onChangeHandler={this.onChangeHandler}
            renderData={renderData[C.purposeOthersKey]}
            formData={this.props.formData}
            updateFormData={this.props.updateFormData}
            toggleScroll={this.props.toggleScroll}
          />
        </Col>);
    }
    if (renderData[C.repaymentScheduleKey] && !renderData[C.repaymentScheduleKey].isHidden) {
      const colSize = renderData[C.repaymentScheduleKey].metaData.colWidth * (layoutConfig.defaultColumnSizeForMobile);
      replaymentScheduleBlock = (
        <Col size={colSize} style={{ ...styles.colWrapper }}>
          <RenderDropdown
            compProps={this.getComponentProps('dropdown', C.repaymentScheduleKey)}
            elmId={C.repaymentScheduleKey}
            onChangeHandler={this.onChangeHandler}
            renderData={renderData[C.repaymentScheduleKey]}
            formData={this.props.formData}
            updateFormData={this.props.updateFormData}
            toggleScroll={this.props.toggleScroll}
          />
        </Col>);
    }
    if (renderData[C.repaymentScheduleOthersKey] && !renderData[C.repaymentScheduleOthersKey].isHidden) {
      const colSize = renderData[C.repaymentScheduleOthersKey].metaData.colWidth * (layoutConfig.defaultColumnSizeForMobile);
      replaymentScheduleOthersBlock = (
        <Col size={colSize} style={{ ...styles.colWrapper }}>
          <RenderInputText
            compProps={this.getComponentProps('text', C.repaymentScheduleOthersKey)}
            elmId={C.repaymentScheduleOthersKey}
            onChangeHandler={this.onChangeHandler}
            renderData={renderData[C.repaymentScheduleOthersKey]}
            formData={this.props.formData}
            updateFormData={this.props.updateFormData}
            toggleScroll={this.props.toggleScroll}
          />
        </Col>);
    }
    if (renderData[C.securityOfferedKey] && !renderData[C.securityOfferedKey].isHidden) {
      const colSize = renderData[C.securityOfferedKey].metaData.colWidth * (layoutConfig.defaultColumnSizeForMobile);
      const securityOfferedRenderData = renderData[C.securityOfferedKey];
      if (this.state.customValidation.invalidateSecurityOfferedKey) {
        securityOfferedRenderData.metaData.validator = '["customSecurityInvalidate"]';
        securityOfferedRenderData.metaData.invalidationMessage = this.state.customValidation.invalidationMessage;
      } else {
        securityOfferedRenderData.metaData.validator = '[]';
        securityOfferedRenderData.metaData.invalidationMessage = '';
      }
      securityOfferedBlock = (
        <Col size={colSize} style={{ ...styles.colWrapper }}>
          <RenderDropdown
            compProps={this.getComponentProps('dropdown', C.securityOfferedKey)}
            elmId={C.securityOfferedKey}
            onChangeHandler={(...params) => { this.onChangeHandler(...params); this.customOnChangeHandler(...params); }}
            renderData={securityOfferedRenderData}
            formData={this.props.formData}
            updateFormData={this.props.updateFormData}
            toggleScroll={this.props.toggleScroll}
          />
        </Col>);
    }
    if (renderData[C.securityOfferedOthersKey] && !renderData[C.securityOfferedOthersKey].isHidden) {
      const colSize = renderData[C.securityOfferedOthersKey].metaData.colWidth * (layoutConfig.defaultColumnSizeForMobile);
      securityOfferedOthersBlock = (
        <Col size={colSize} style={{ ...styles.colWrapper }}>
          <RenderInputText
            compProps={this.getComponentProps('text', C.securityOfferedOthersKey)}
            elmId={C.securityOfferedOthersKey}
            onChangeHandler={this.onChangeHandler}
            renderData={renderData[C.securityOfferedOthersKey]}
            formData={this.props.formData}
            updateFormData={this.props.updateFormData}
            toggleScroll={this.props.toggleScroll}
          />
        </Col>);
    }

    if (renderData[C.valueOfAssetKey] && !renderData[C.valueOfAssetKey].isHidden) {
      const colSize = renderData[C.valueOfAssetKey].metaData.colWidth * (layoutConfig.defaultColumnSizeForMobile);
      valueOfAssetBlock = (
        <Col size={colSize} style={{ ...styles.colWrapper }}>
          <RenderInputText
            compProps={this.getComponentProps('number', C.valueOfAssetKey)}
            elmId={C.valueOfAssetKey}
            onChangeHandler={this.onChangeHandler}
            renderData={renderData[C.valueOfAssetKey]}
            formData={this.props.formData}
            updateFormData={this.props.updateFormData}
            toggleScroll={this.props.toggleScroll}
          />
        </Col>);
    }

    if (renderData[C.commentsOnSecurityKey] && !renderData[C.commentsOnSecurityKey].isHidden) {
      const colSize = renderData[C.commentsOnSecurityKey].metaData.colWidth * (layoutConfig.defaultColumnSizeForMobile);
      commentsOnSecurityBlock = (
        <Col size={colSize} style={{ ...styles.colWrapper }}>
          <RenderInputText
            compProps={this.getComponentProps('text', C.commentsOnSecurityKey)}
            elmId={C.commentsOnSecurityKey}
            onChangeHandler={this.onChangeHandler}
            renderData={renderData[C.commentsOnSecurityKey]}
            formData={this.props.formData}
            updateFormData={this.props.updateFormData}
            toggleScroll={this.props.toggleScroll}
          />
        </Col>);
    }

    if (renderData[C.newToCreditCustomerKey] && !renderData[C.newToCreditCustomerKey].isHidden) {
      const colSize = renderData[C.newToCreditCustomerKey].metaData.colWidth * (layoutConfig.defaultColumnSizeForMobile);
      newToCreditCustomerBlock = (
        <Col size={colSize} style={{ ...styles.colWrapper }}>
          <RenderRadio
            compProps={this.getComponentProps('radio', C.newToCreditCustomerKey)}
            elmId={C.newToCreditCustomerKey}
            onChangeHandler={(...params) => { this.onChangeHandler(...params); this.customOnChangeHandler(...params); }}
            renderData={renderData[C.newToCreditCustomerKey]}
            formData={this.props.formData}
            updateFormData={this.props.updateFormData}
            toggleScroll={this.props.toggleScroll}
          />
        </Col>);
    }
    if (renderData[C.schemeDescription] && !renderData[C.schemeDescription].isHidden) {
      const colSize = renderData[C.schemeDescription].metaData.colWidth * (layoutConfig.defaultColumnSizeForMobile);
      schemeDescriptionBlock = (
        <Col size={colSize} style={{ ...styles.colWrapper }}>
          <RenderAutoComplete
            compProps={this.getComponentProps('rest-dropdown', C.schemeDescription)}
            elmId={C.schemeDescription}
            onChangeHandler={(...params) => { this.onChangeHandler(...params); this.customOnChangeHandler(...params); }}
            renderData={renderData[C.schemeDescription]}
            formData={this.props.formData}
            updateFormData={this.props.updateFormData}
            toggleScroll={this.props.toggleScroll}
          />
        </Col>
          );
    }
    if (renderData[C.productDescription] && !renderData[C.productDescription].isHidden) {
      const colSize = renderData[C.productDescription].metaData.colWidth * (layoutConfig.defaultColumnSizeForMobile);
      productDescriptionBlock = (
        <Col size={colSize} style={{ ...styles.colWrapper }}>
          <RenderInputText
            compProps={this.getComponentProps('text', C.productDescription)}
            elmId={C.productDescription}
            onChangeHandler={this.onChangeHandler}
            renderData={renderData[C.productDescription]}
            formData={this.props.formData}
            updateFormData={this.props.updateFormData}
            toggleScroll={this.props.toggleScroll}
          />
        </Col>
          );
    }
    if (renderData[C.schemeId] && !renderData[C.schemeId].isHidden) {
      const colSize = renderData[C.schemeId].metaData.colWidth * (layoutConfig.defaultColumnSizeForMobile);
      schemeIdBlock = (
        <Col size={colSize} style={{ ...styles.colWrapper }}>
          <RenderInputHidden
            compProps={this.getComponentProps('hidden', C.schemeId)}
            elmId={C.schemeId}
            onChangeHandler={this.onChangeHandler}
            renderData={renderData[C.schemeId]}
            formData={this.props.formData}
            updateFormData={this.props.updateFormData}
            toggleScroll={this.props.toggleScroll}
          />
        </Col>
          );
    }
    if (renderData[C.productId] && !renderData[C.productId].isHidden) {
      const colSize = renderData[C.productId].metaData.colWidth * (layoutConfig.defaultColumnSizeForMobile);
      productIdBlock = (
        <Col size={colSize} style={{ ...styles.colWrapper }}>
          <RenderInputHidden
            compProps={this.getComponentProps('hidden', C.productId)}
            elmId={C.productId}
            onChangeHandler={this.onChangeHandler}
            renderData={renderData[C.productId]}
            formData={this.props.formData}
            updateFormData={this.props.updateFormData}
            toggleScroll={this.props.toggleScroll}
          />
        </Col>
          );
    }
    if (renderData[C.loanAmountKey] && renderData[C.loanTenureKey] && renderData[C.roiKey]) {
      return (<View
        {...styles.wrapperStyles}
      >
        {loanAmountBlock}
        {tenureBlock}
        {/* {loanRoiBlock} */}
        {/* <RenderCheckbox
                      compProps={this.getComponentProps('checkbox', 'includeLoanInsurance')}
                      elmId={'includeLoanInsurance'}
                      onChangeHandler={this.onChangeHandler}
                      renderData={renderData.includeLoanInsurance}/>*/}
        {emiComponent}
        {/* <RenderPopupForm
                      elmId={'popupInsurance'}
                      renderData={renderData.popupInsurance}
                      formData={this.props.formData}/>*/}
        {/* <Text
                      {...styles.textStyles}>
                      {strings('renderEmiCal.renderFooterMesssage1')}
                    </Text>
                    <Text
                      {...styles.textStyles}>
                   {strings('renderEmiCal.renderFooterMesssage2')}
                    </Text>*/}
        <Row>
          {newToCreditCustomerBlock}
        </Row>
        <Row>
          {schemeDescriptionBlock}
          {productDescriptionBlock}
          {schemeIdBlock}
          {productIdBlock}
        </Row>
        <Row>
          {loanPurposeBlock}
          {loanPurposeBlockOthersBlock}
        </Row>
        <Row>
          {replaymentScheduleBlock}
          {replaymentScheduleOthersBlock}
        </Row>
        <Row>
          {securityOfferedBlock}
          {securityOfferedOthersBlock}
        </Row>
        <Row>
          {valueOfAssetBlock}
        </Row>
        <Row>
          {commentsOnSecurityBlock}
        </Row>
      </View>);
    }
    return null;
  }
}

/* Received props are
All the default styling
Behavioural attributes

onChange Fn
onBlur Fn
*/
