import styled from 'styled-components';
import { fonts, colors } from '../../configs/styleVars';
export const EmiContainer = styled.div`
    font-size: ${`${fonts.h3}px`};
    margin-top: 50px;
    margin-bottom: 20px;
    text-align: center;
    color: ${colors.primaryBGColor};
`;

export const ValueLabel = styled.span`
    font-weight: bold;
`;

export const DeclarationBlock = styled.div`
    margin-top: 40px;
    font-size: ${fonts.labelFontSize};
    color: grey;
`;
