/**
 *
 * Asynchronously loads the component for RenderEmiCalculator
 *
 */

import Loadable from 'react-loadable';

export default Loadable({
  loader: () => import('./index'),
  loading: () => null,
});
