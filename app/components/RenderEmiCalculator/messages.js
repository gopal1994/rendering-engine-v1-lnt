/*
 * RenderEmiCalculator Messages
 *
 * This contains all the text for the RenderEmiCalculator component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.RenderEmiCalculator.header',
    defaultMessage: 'This is the RenderEmiCalculator component !',
  },
});
