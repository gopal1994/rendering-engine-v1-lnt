/**
*
* RenderInputText
*
*/

import React from 'react';
// import styled from 'styled-components';
// import TextField from 'material-ui/TextField';
import RenderInputTextAbstract from './RenderInputTextAbstract';
import * as styles from './RenderInputTextStyles';
import * as common from '../../utilities/commonUtils';
import { appConst } from '../../configs/appConfig';
import TextInputField from '../TextInputField';

export default class RenderInputTextComponent extends RenderInputTextAbstract { // eslint-disable-line react/prefer-stateless-function


  render() {
    const errorMessage = this.props.renderData.errorMessage;

    const meta = this.props.renderData.metaData,
      {autoCapitalize, ...compProps} = this.props.compProps,
      numValue = compProps.value;
    // const propsRe = { ...compProps, value: !!numValue || numValue === 0 ? common.convertCurrency(numValue, this.state.isFocused) : numValue };
    // console.log('propsRe: ', propsRe);
    // const newCompProps = meta.isCurrency ? propsRe : compProps;
    const componentProps = { ...compProps, ...styles.inputStyles }; // needed for TextField
    const value = meta.isCurrency && compProps.value ? common.convertCurrency(compProps.value, this.state.isFocused) : compProps.value;
    return (
      <TextInputField
        value={value}
        componentProps={componentProps}
        errorMessage={errorMessage}
        className="field-container"
        onChangeHandler={(e) => this.onChangeHandler(e.target.value)}
        toggleFocus={(bool) => this.toggleFocus(bool)}
        onBlurHandler={(e) => this.onBlurHandler(e.target.value)}
      />

    );
  }
}

/* Received props are
All the default styling
Behavioural attributes

onChange Fn
onBlur Fn
*/
