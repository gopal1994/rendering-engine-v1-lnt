import { inputComponentStyle } from '../../configs/componentStyles/commonStyles';

export const inputStyles = {
  ...inputComponentStyle,
};
