/**
*
* RenderRadio
*
*/

import React from 'react';
// import styled from 'styled-components';


export default class RenderRadioAbstract extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {

    };
    this.onChangeHandler = this.onChangeHandler.bind(this);
    this.onBlurHandler = this.onBlurHandler.bind(this);
  }

  componentWillMount() {

  }

  onChangeHandler(e, value) {
    this.props.onChangeHandler(value, this.props.elmId);
  }

  onBlurHandler(e, value) {
    this.props.onBlurHandler(value, this.props.elmId);
  }

  render() {
    return null;
  }
}
