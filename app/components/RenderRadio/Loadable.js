/**
 *
 * Asynchronously loads the component for RenderRadio
 *
 */

import Loadable from 'react-loadable';

export default Loadable({
  loader: () => import('./index'),
  loading: () => null,
});
