/**
*
* Loader
*
*/

import React from 'react';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';
import LoaderComponent from './LoaderComponent';

class Loader extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <LoaderComponent {...this.props} />
    );
  }
}

Loader.propTypes = {

};

export default Loader;
