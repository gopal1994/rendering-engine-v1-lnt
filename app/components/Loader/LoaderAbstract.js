/**
 *
 * Loader Abstract
 *
 */

import React from 'react';

export default class LoaderAbstract extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);
    this.state = {};
  }

  componentWillReceiveProps(newProps) {
    if (newProps && newProps !== this.props) {
      newProps.loaderStatus.isLongReq ? this.showPopup() : this.hidePopup();
    }
  }

  render() {
    return null;
  }
}
