/**
 *
 * Asynchronously loads the component for RenderFinalScreen
 *
 */

import Loadable from 'react-loadable';

export default Loadable({
  loader: () => import('./index'),
  loading: () => null,
});
