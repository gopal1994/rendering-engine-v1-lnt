/**
*
* RenderFinalScreenComponent
*
*/

import React from 'react';
import RenderFinalScreenAbstract from './RenderFinalScreenAbstract';
// import styled from 'styled-components';


class RenderFinalScreenComponent extends RenderFinalScreenAbstract { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <div>This is RenderFinalScreen Component</div>
    );
  }
}

RenderFinalScreenComponent.propTypes = {

};

export default RenderFinalScreenComponent;
