/**
 *
 * TextField
 *
 */

import React from 'react';

import { Text } from 'react-native';
import * as styles from './styles';


const RenderText = (props) => <Text {...props} />;

export default RenderText;
