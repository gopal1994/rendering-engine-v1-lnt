/**
*
* Column
*
*/

import React from 'react';
// import styled from 'styled-components';
import PropTypes from 'prop-types';
import { Column as Col } from 'react-native-responsive-grid';
import { Text } from 'react-native';
import { layoutConfig } from '../../configs/appConfig';


class Column extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    let { size, colWidth } = this.props;
    size = size || (colWidth * layoutConfig.defaultColumnSizeForMobile);
    return (
      <Col size={size} smSize={this.props.smSize || 100} {...this.props} >
        {this.props.children}
      </Col>
    );
  }
}

Column.propTypes = {
  size : PropTypes.number,
  colWidth : PropTypes.number,
};

export default Column;
