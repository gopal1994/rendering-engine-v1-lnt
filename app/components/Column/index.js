/**
*
* Column
*
*/

import React from 'react';
// import styled from 'styled-components';


class Column extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    const style = this.props.style || {};
    return (
      <div
        className={`col s12 m${this.props.colWidth} offset-m${this.props.offset}`}
        style={{ ...style }}
      >
        {this.props.children}
      </div>
    );
  }
}

Column.propTypes = {

};

export default Column;
