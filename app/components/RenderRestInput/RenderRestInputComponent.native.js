/**
*
* RenderRestInput
*
*/

import React from 'react';
import { View } from 'react-native';

import RenderRestInputAbstract from './RenderRestInputAbstract';
import TextInputField from '../TextInputField/';
import * as styles from './RenderRestInputStyles';


export default class RenderRestInputComponent extends RenderRestInputAbstract { // eslint-disable-line react/prefer-stateless-function

  // TODO:
  // This rest component just support the Input text for now
  // Make is supported for all other form input types
  // Use form_type for choosing the right type
  render() {
    return (
      <View>
        <TextInputField
          {...this.props.compProps}
          onChange={this.onChangeHandler}
          onEndEditing={this.onBlurHandler}
        />
      </View>
    );
  }
}
