/**
*
* RenderRestInput
*
*/

import React from 'react';
// import styled from 'styled-components';
import { makeSelectUserDetails } from '../../containers/AppDetails/selectors';
import * as metaApiHelper from '../../utilities/metaApiHelper';

import * as validationFns from '../../utilities/validations/validationFns';

export default class RenderRestInputAbstract extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);
    this.state = {

    };
    this.onChangeHandler = this.onChangeHandler.bind(this);
    this.onBlurHandler = this.onBlurHandler.bind(this);
    this.validateElement = this.validateElement.bind(this);
    this.callRestElem = this.callRestElem.bind(this);
  }

  componentWillMount() {

  }

  validateElement(e) {
    const value = e.target.value || e.nativeEvent.text;
    return validationFns.validateElm(this.props.renderData, value);
  }

  onChangeHandler(e) {
    this.props.onChangeHandler(e.target.value || e.nativeEvent.text, this.props.elmId);
  }

  _restSuccessCallback = (response, api) => {
    const self = this;
    let deltaUpdatedFormObject = metaApiHelper._restSuccessCallback(response, api);
    Object.keys(deltaUpdatedFormObject).forEach((formKey) => {
      window.setTimeout(() => {
        self.props.updateFormData(formKey, deltaUpdatedFormObject[formKey]);
      });
    });
  }

  _buildRequestObject = (api) => {
    const self = this;
    const reqData = {
      url: api && api.url,
      params: metaApiHelper._buildPayloadData(api, this.props.formData, this.props.userDetails),
      userDetails: this.props.userDetails,
      successCb: (response) => {
        if (api.mapping && typeof(api.mapping) === 'object') {
          if (Object.keys(api.mapping).length) {
            self._restSuccessCallback(response, api);
          }
        }
      },
      errorCb: () => {
      
      },
    };
    return reqData;
  }

  callRestElem(api) {
    if (typeof api === 'object') {
      const meta = this.props.renderData && this.props.renderData.metaData,
        method = api && api.method;
      const reqObj = this._buildRequestObject(api);
      if (method === 'POST') {
        this.props.postRequest({ key: null, data: reqObj });
      } else {
        this.props.getRequest({ key: null, data: reqObj });
      }
    }
  }

  onBlurHandler(e) {
    this.props.onBlurHandler(e.target.value || e.nativeEvent.text, this.props.elmId);
    // Call the rest api for the rest element and perform necessary updates
    if (this.validateElement(e).isValid) {
      const meta = this.props.renderData && this.props.renderData.metaData;
      this.callRestElem(meta.api);
    }
  }

  // TODO:
  // This rest component just support the Input text for now
  // Make is supported for all other form input types
  // Use form_type for choosing the right type
  render() {
    return null;
  }
}
