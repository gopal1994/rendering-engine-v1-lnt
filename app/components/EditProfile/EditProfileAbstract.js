/**
*
* EditProfileAbstract
*
*/

import React from 'react';
// import styled from 'styled-components';

export default class EditProfileAbstract extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

  validateFormData = () => {
    this.props.cleanFormData();
    this.props.validateStep();
  }

  buildRequestParams = (selfie) => {
    const fd = new FormData();
    const { formData } = this.props;
    fd.append('image', selfie);
    Object.keys(formData).forEach((key) => {
      fd.append('key', formData[key]);
    });
    return fd;
  }

  submitData = (selfie) => {
    const requestParams = this.buildRequestParams(selfie);
  }

  render() {
    return null;
  }
}
EditProfileAbstract.propTypes = {

};
