import { colors, fonts, lineHeight } from '../../configs/styleVars';

export const modal = {
  style: {
    padding: 0,
    margin: 0,
  },
};

export const wrapper = {
  style: {
    flex: 1,
    backgroundColor: colors.white,
  },
};

export const fieldWrapper = {
  style: {
    paddingHorizontal: 20,
  },
};

export const backWrapper = {
  style: {
    padding: 20,
  },
};

export const btnWrapper = {
  style: {
    borderTopWidth: 1,
    borderTopColor: colors.underlineColor,
    height: 44,
    justifyContent: 'flex-end',
    flexDirection: 'row',
    flex: 0,
    alignItems: 'center',
    paddingHorizontal: 20,
  },
};

export const btnText = {
  style: {
    ...fonts.getFontFamilyWeight(),
    color: colors.primaryBGColor,
    fontSize: fonts.body,
    lineHeight: lineHeight.body,
  },
};
