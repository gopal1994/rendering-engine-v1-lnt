/**
*
* AutoCompleteModal
*
*/

import React from 'react';
import { View, ScrollView, TouchableOpacity, Platform, KeyboardAvoidingView } from 'react-native';
import Text from '../RenderText';
import AutoCompleteOptions from '../AutoCompleteOptions/';
import TextInputField from '../TextInputField/';
import * as styles from './styles';
import Modal from '../Modal';
import RenderBackButton from '../RenderBackButton';
import Loader from '../Loader';
import { strings } from '../../../locales/i18n';

class AutoCompleteModal extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

  render() {
    const { showList, handleBackPress, compProps, inputVal, onChange, options, updateOption } = this.props;
    let optionsList = (<ScrollView></ScrollView>);
    if (showList) {
      optionsList = (
        <AutoCompleteOptions
          optionsList={options}
          updateOption={updateOption}
        />
      );
    }
    const scrollBehave = {};
    if (Platform.OS === 'ios') {
      scrollBehave.behavior = 'padding';
    }
    return (
      <Modal
        isVisible={showList}
        handleBackPress={handleBackPress}
        {...styles.modal}
      >
        <KeyboardAvoidingView
          style={{ flex: 1 }}
          {...scrollBehave}
        >
          <View
            {...styles.wrapper}
          >
            <View {...styles.backWrapper}>
              <RenderBackButton
                type={'black'}
                onBack={handleBackPress}
              />
            </View>
            <View {...styles.fieldWrapper}>
              <TextInputField
                {...compProps}
                value={inputVal}
                autoFocus
                error={''}
                onChange={onChange}
              />
            </View>
            {optionsList}
            <View {...styles.btnWrapper}>
              <TouchableOpacity onPress={handleBackPress}>
                <Text {...styles.btnText}>{strings('renderJourney.submitBtnText')}</Text>
              </TouchableOpacity>
            </View>
          </View>
        </KeyboardAvoidingView>

        <Loader loaderStatus={this.props.loaderState || { isVisible: false }} />
      </Modal>
    );
  }
}

AutoCompleteModal.propTypes = {

};

export default AutoCompleteModal;
