/*
 * RenderAccordion Messages
 *
 * This contains all the text for the RenderAccordion component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.RenderAccordion.header',
    defaultMessage: 'This is the RenderAccordion component !',
  },
});
