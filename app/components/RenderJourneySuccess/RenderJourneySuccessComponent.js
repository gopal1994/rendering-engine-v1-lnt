/**
*
* RenderJourneySuccessComponent
*
*/

import React from 'react';
import RenderJourneySuccessAbstract from './RenderJourneySuccessAbstract';
// import styled from 'styled-components';


class RenderJourneySuccessComponent extends RenderJourneySuccessAbstract { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <div>This is RenderJourneySuccess Component</div>
    );
  }
}

RenderJourneySuccessComponent.propTypes = {

};

export default RenderJourneySuccessComponent;
