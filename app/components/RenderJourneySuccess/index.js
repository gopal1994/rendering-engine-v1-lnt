/**
*
* RenderJourneySuccess
*
*/

import React from 'react';
import RenderJourneySuccessComponent from './RenderJourneySuccessComponent';
// import styled from 'styled-components';


class RenderJourneySuccess extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <RenderJourneySuccessComponent {...this.props} />
    );
  }
}

RenderJourneySuccess.propTypes = {

};

export default RenderJourneySuccess;
