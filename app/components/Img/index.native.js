/**
 *
 * Img.js
 *
 * Renders an image, enforcing the usage of the alt="" tag
 */

import React from 'react';
import PropTypes from 'prop-types';

import { Image } from 'react-native';

export default class Img extends React.PureComponent {
  render() {
    const { src, className, alt,...otherProps } = this.props;
    return (
      <Image source={src} {...otherProps} />
    );
  }
}

// We require the use of src and alt, only enforced by react in dev mode
Img.propTypes = {
  alt: PropTypes.string,
  className: PropTypes.string,
};
