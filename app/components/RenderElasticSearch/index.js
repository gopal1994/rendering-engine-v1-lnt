/**
*
* RenderElasticSearch
*
*/

import React from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import * as appActions from '../../containers/AppDetails/actions';
import { makeSelectUserDetails } from '../../containers/AppDetails/selectors';
import RenderElasticSearchComponent from './RenderElasticSearchComponent';

class RenderElasticSearch extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <RenderElasticSearchComponent {...this.props} />
    );
  }
}

RenderElasticSearch.propTypes = {

};

const mapStateToProps = createStructuredSelector({
  userDetails: makeSelectUserDetails(),
});

function mapDispatchToProps(dispatch) {
  return {
    getRequest: (reqObject) => dispatch(appActions.getRequest(reqObject)),
    postRequest: (reqObject) => dispatch(appActions.postRequest(reqObject)),
    dispatch,
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export default compose(
  withConnect,
)(RenderElasticSearch);

