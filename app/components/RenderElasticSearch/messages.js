/*
 * RenderElasticSearch Messages
 *
 * This contains all the text for the RenderElasticSearch component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.RenderElasticSearch.header',
    defaultMessage: 'This is the RenderElasticSearch component !',
  },
});
