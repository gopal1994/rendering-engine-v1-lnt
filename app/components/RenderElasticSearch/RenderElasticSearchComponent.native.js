/**
*
* RenderElasticSearch
*
*/

import React from 'react';
import { View, TouchableWithoutFeedback, ScrollView } from 'react-native';
import Text from '../RenderText';
import RenderElasticSearchAbstract from './RenderElasticSearchAbstract';
import TextInputField from '../TextInputField/';
import * as styles from '../RenderAutoComplete/RenderAutoCompleteStyles';
import AutoCompleteModal from '../AutoCompleteModal';

export default class RenderElasticSearchComponent extends RenderElasticSearchAbstract { // eslint-disable-line react/prefer-stateless-function

  onChange = (e) => {
    this.setState({ inputVal: e.nativeEvent.text }, this.onChangeHandler);
  }

  render() {
    return (
      <View>
        <AutoCompleteModal
          compProps={this.props.compProps}
          onChange={this.onChange}
          inputVal={this.state.inputVal.toString()}
          options={this.state.optionsList}
          updateOption={this.updateOption}
          showList={this.state.showList}
          handleBackPress={() => this.setListFlag(false)}
          loaderState={this.state.loaderState}
        />
        <TextInputField
          type={'viewOnly'}
          {...this.props.compProps}
          value={this.state.inputVal}
          onPress={() => this.setListFlag(true)}
        />
      </View>
    );
  }
}
