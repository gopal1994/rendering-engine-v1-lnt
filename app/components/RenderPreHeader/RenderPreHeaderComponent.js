/**
*
* RenderPreHeaderComponent
*
*/

import React from 'react';
import RenderPreHeaderAbstract from './RenderPreHeaderAbstract';
// import styled from 'styled-components';


class RenderPreHeaderComponent extends RenderPreHeaderAbstract { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <div>This is RenderPreHeader Component</div>
    );
  }
}

RenderPreHeaderComponent.propTypes = {

};

export default RenderPreHeaderComponent;
