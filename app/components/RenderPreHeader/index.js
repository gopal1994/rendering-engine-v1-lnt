/**
*
* RenderPreHeader
*
*/

import React from 'react';
import RenderPreHeaderComponent from './RenderPreHeaderComponent';
// import styled from 'styled-components';


class RenderPreHeader extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <RenderPreHeaderComponent {...this.props} />
    );
  }
}

RenderPreHeader.propTypes = {

};

export default RenderPreHeader;
