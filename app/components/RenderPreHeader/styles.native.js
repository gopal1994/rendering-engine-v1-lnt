import { Dimensions } from 'react-native';
import { colors, sizes, fonts } from '../../configs/styleVars';

export const containerStyle = {
  backgroundColor: colors.primaryBGColor,
  paddingLeft: sizes.appPaddingLeft,
  paddingRight: sizes.appPaddingRight,
  paddingTop: 10,
  paddingBottom: 10,
  width: Dimensions.get('window').width,
};

export const textWrapStyle = { flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', paddingTop: 20 };

export const textStyle = { fontSize: 20, ...fonts.getFontFamilyWeight('bold'), color: colors.white };

export const callIconStyleProps = {
  style: { height: 30, width: 30 },
  resizeMode: 'contain',
};
