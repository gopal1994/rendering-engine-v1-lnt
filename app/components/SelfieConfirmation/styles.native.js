import { colors, sizes, fonts, lineHeight } from '../../configs/styleVars';

export const wrapper = {
  paddingHorizontal: sizes.appPaddingHorizontal,
  flex: 1,
  paddingBottom: 40,
};

export const previewStyles = {
  height: 200,
  marginTop: 25,
};

export const headingStyle = {
  fontSize: 20,
  lineHeight: lineHeight.h3,
  color: colors.black,
  marginTop: 15,
  marginBottom: 15,
  ...fonts.getFontFamilyWeight('bold'),
  textAlign: 'center',
};

export const bodyText = {
  fontSize: 16,
  lineHeight: lineHeight.description,
  color: colors.black,
  marginTop: 15,
  marginBottom: 15,
  textAlign: 'center',
  ...fonts.getFontFamilyWeight(),
};

export const infoStyles = {
  ...fonts.getFontFamilyWeight(),
  fontSize: fonts.description,
  lineHeight: lineHeight.description,
  paddingVertical: 10,
};
