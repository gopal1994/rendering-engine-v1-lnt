/**
*
* SelfieConfirmationComponent
*
*/

import React from 'react';
import SelfieConfirmationAbstract from './SelfieConfirmationAbstract';
// import styled from 'styled-components';


class SelfieConfirmationComponent extends SelfieConfirmationAbstract { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <div>This is SelfieConfirmation Component</div>
    );
  }
}

SelfieConfirmationComponent.propTypes = {

};

export default SelfieConfirmationComponent;
