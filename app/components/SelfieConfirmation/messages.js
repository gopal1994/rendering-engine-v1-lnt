/*
 * SelfieConfirmation Messages
 *
 * This contains all the text for the SelfieConfirmation component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.SelfieConfirmation.header',
    defaultMessage: 'This is the SelfieConfirmation component !',
  },
});
