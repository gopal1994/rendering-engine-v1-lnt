/**
*
* RenderHeading
*
*/

import React from 'react';
import RenderHeadingComponent from './RenderHeadingComponent';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

class RenderHeading extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <RenderHeadingComponent {...this.props} />
    );
  }
}

RenderHeading.propTypes = {

};

export default RenderHeading;
