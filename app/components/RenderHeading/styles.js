import { colors, fonts, lineHeight } from '../../configs/styleVars';

export const getStyles = (type = 'body') => ({
  fontSize: `${fonts[type] || fonts.body}px`,
  lineHeight: `${lineHeight[type] || lineHeight.body}px`,
  color: colors.black,
  marginTop: '20px',
  marginBottom: '5px',
  ...fonts.getFontFamilyWeight('bold'),
});
