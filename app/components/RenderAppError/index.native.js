/**
*
* RenderAppError
*
*/

import React from 'react';
import { View, TouchableHighlight, Dimensions } from 'react-native';
// import styled from 'styled-components';
import Text from '../RenderText';
import { FormattedMessage } from 'react-intl';
import messages from './messages';
import * as userUtils from '../../utilities/userUtils';

import RenderButton from '../RenderButton/';
import * as styles from '../RenderApplyButton/RenderApplyButtonStyles';
import { strings } from '../../../locales/i18n';
import styleVars from '../../configs/styleVars';
const colors = styleVars.colors;

class RenderAppError extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

  logout = () => {
    this.props.toggleMpin(false);
    this.props.setAppError({ status: false });
  }

  render() {
    return (
      <View style={{ ...errorWrapStyle }}>
        <Text style={{ ...textStyle }}>{ this.props.showAppError.customMessage || strings('renderJourney.errorMsg')}</Text>
        <RenderButton
          type={'primary'}
          text={strings('renderJourney.errorBtnText')}
          onPress={this.logout}
          {...styles.btnProps}
        />
      </View>
    );
  }
}

const errorWrapStyle = {
  position: 'absolute',
  zIndex: 1000,
  elevation: 10,
  bottom: 0,
  width: Dimensions.get('window').width,
  height: 130,
  borderRadius: 10,
  left: 0,
  padding: 10,
  backgroundColor: colors.white,
};

const textStyle = {
  textAlign: 'center',
  color: colors.primaryBGColor,
  marginBottom: 5,
};
const linkSyle = {
  textAlign: 'center',
  color: '#FFF',
  textDecorationLine: 'underline',
};

RenderAppError.propTypes = {

};

export default RenderAppError;
