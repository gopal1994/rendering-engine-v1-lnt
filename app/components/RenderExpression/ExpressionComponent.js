import React, { Component } from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

// import _ from 'underscore';
// import expr from 'expr-eval';
import RenderExpressionAbstract from './RenderExpressionAbstract';
import componentProps from '../../containers/RenderStep/componentProps';

import * as renderService from '../../utilities/renderDataService';


class ExpressionComponent extends RenderExpressionAbstract {

    renderCardBody = () => {
        let variablesBody = [];
        let { renderData, updatedFields, formData, elmId } = { ...this.props};
        let formType = renderData.metaData.form_type;

        const UnitComponent = renderService.getComponentByFormType(formType);

        let compProps = componentProps[formType] && componentProps[formType](renderData, elmId, renderData.value);
        //NEED to add additional logic; disabled logic to move to compProps
        compProps.value = this.state.value;

        if(UnitComponent) {
            const componentSection = (<UnitComponent
                compProps={compProps}
                editMode={this.props.editMode}
                className={this.props.className}
                key={elmId}
                elmId={elmId}
                renderData={renderData}
                updatedFields={updatedFields}
                formData={formData}
                onChangeHandler={(value, element = elmId) => this.onChangeHandler(value, element)}
                onBlurHandler={(value) => this.onBlurHandler(value, elmId)}>
              </UnitComponent>
            );
            variablesBody.push(componentSection);

        }
        return variablesBody;

    }

    render() {
        //console.log('props in expression', this.props, this.state.value);
        let cardBody = this.renderCardBody();
        return cardBody;
    }
}

ExpressionComponent.defaultProps = {
};


ExpressionComponent.propTypes = {

};

export default ExpressionComponent;