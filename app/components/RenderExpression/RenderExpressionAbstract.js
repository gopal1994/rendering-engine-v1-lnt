import React from 'react';
import _ from 'underscore';
import expr from 'expr-eval';

export default class RenderExpressionAbstract extends React.PureComponent {
    constructor(props) {
        super(props);
        let { formData, elmId} = { ...props }
        this.state = {
            value: formData[elmId].value
        };

    }

    componentWillReceiveProps = (nextProps) => {
        //console.log('nextProps and props', nextProps, this.props, parsedValue);
        let { renderData, formData, elmId} = { ...nextProps };
        let parsedValue = this.parseExpression(renderData, formData);

        if(isFinite(parsedValue) && (parsedValue !== this.state.value)) {
            this.props.onChangeHandler && this.props.onChangeHandler(parsedValue, elmId);
            this.setState({
                value: parsedValue
            });
        }
    }

    onChangeHandler = (elmId, value) => {
    }

    onBlurHandler = (value, elmId) => {

    }


    parseExpression = (variable, formData) => {
        let Parser = expr.Parser;
        let { expression, dependentFields } = { ...variable.metaData };
        let obj = {};
        dependentFields.forEach(field => {
            obj[field] = formData[field] || 0;
        });
        //console.log('expression', dependentFields, obj, expression);
        return Parser.evaluate(expression, obj).toString();
    }

    render() {
        return null;
    }
}

RenderExpressionAbstract.propTypes = {
};