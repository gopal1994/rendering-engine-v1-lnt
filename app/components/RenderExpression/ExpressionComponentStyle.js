import styleVars from '../../configs/styleVars';
import styled from 'styled-components';
const fonts = styleVars.fonts,
    colors = styleVars.colors;

export const DisplayUploadedFilesContainer = styled.span`
    margin: 3px;
    overflow: hidden;
`;

export const InputFieldCountainer = styled.div`
    display: flex;
    flex-wrap: wrap;
`;

export const ButtonFieldCountainer = styled.div`
`;

export const UploadFilesOuterContainer = styled.div`
    display: block;
`;

export const UploadFieldMainContainer = styled.div`
    min-height: 72px;
    margin-top: 14px;
    position: relative;
`;

export const UploadFieldHeading = styled.div`
    position: ${props => props.fileLength || props.editMode ? 'static' : 'absolute'};
    line-height: 22px;
    top: 38px;
    z-index: 1;
    color: rgba(0, 0, 0, 0.3);
    font-size: ${props => props.fileLength || props.editMode ? '12px' : '16px'};
`;