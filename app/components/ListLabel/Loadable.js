/**
 *
 * Asynchronously loads the component for ListLabel
 *
 */

import Loadable from 'react-loadable';

export default Loadable({
  loader: () => import('./index'),
  loading: () => null,
});
