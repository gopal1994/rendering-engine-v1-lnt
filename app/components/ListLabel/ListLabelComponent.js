/**
*
* ListLabelComponent
*
*/

import React from 'react';
import ListLabelAbstract from './ListLabelAbstract';
// import styled from 'styled-components';
import {
  containerStyles,
  labelWrapperStyles,
  collapsedLabelWrapperStyles,
  iconWrapperStyles,
  labelStyles,
  imageStyles,
} from './styles';
import iconMap from '../../utilities/iconMap';


class ListLabelComponent extends ListLabelAbstract { // eslint-disable-line react/prefer-stateless-function
  render() {
    const { leftIcon, leftIconType, rightIcon, rightIconType, label, onClick, isExpanded } = this.props;
    let leftIconSrc = null;
    let rightIconSrc = null;
    let leftIconStyles = {};
    let rightIconStyles = {};
    if (leftIcon) {
      leftIconSrc = leftIconType === 'url' ? leftIcon : iconMap[leftIcon];
      leftIconStyles = leftIconType === 'url' && imageStyles;
    }
    if (rightIcon) {
      rightIconSrc = rightIconType === 'url' ? rightIcon : iconMap[rightIcon];
      rightIconStyles = rightIconType === 'url' && imageStyles;
    }
    const renderLeftIcon = leftIcon ? (<span style={iconWrapperStyles}>
      <img src={leftIconSrc} style={{ ...leftIconStyles }} alt={'left icon '} />
    </span>) : null;

    const renderRightIcon = rightIconSrc ? (<span style={iconWrapperStyles}>
      <img src={rightIconSrc} style={{ ...rightIconStyles }} alt={'Right Icon'} />
    </span>) : null;

    const wrapperStyles = !isExpanded ? collapsedLabelWrapperStyles : labelWrapperStyles;
    const renderLabel = (
      <div style={containerStyles}>
        {renderLeftIcon}
        <span style={wrapperStyles}>
          <span style={{...labelStyles, ...this.props.labelStyles}}>{label}</span>
          { renderRightIcon }
        </span>
      </div>
    );

    return (
      <div
        onClick={onClick}
        role="button"
      >
        {renderLabel}
      </div>
    );
  }
}

ListLabelComponent.propTypes = {

};

export default ListLabelComponent;
