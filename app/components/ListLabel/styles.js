import { colors, fonts, lineHeight } from '../../configs/styleVars';

export const containerStyles = {
  flexDirection: 'row',
  display: 'flex',
  alignItems: 'center',
  padding: '5px 15px',
  cursor: 'pointer',
};

export const labelWrapperStyles = {
  display: 'flex',
  flex: 1,
  flexDirection: 'row',
  justifyContent: 'space-between',
  alignItems: 'center',
  paddingVertical: 15,
};

export const collapsedLabelWrapperStyles = {
  ...labelWrapperStyles,
  borderBottomWidth: 1,
  borderBottomColor: colors.underlineColor,
};

export const iconWrapperStyles = {
  marginRight: 15,
};

export const labelStyles = {
  ...fonts.getFontFamilyWeight(),
  flex: 1,
  color: colors.black,
  fontSize: fonts.body,
  lineHeight: `${lineHeight.body}px`,
};

export const imageStyles = {
  width: 24,
  height: 24,
};
