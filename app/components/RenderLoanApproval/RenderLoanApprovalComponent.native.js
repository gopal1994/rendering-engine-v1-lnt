/**
*
* RenderLoanApprovalComponent
*
*/

import React from 'react';
import { View, Text } from 'react-native';
import RenderLoanApprovalAbstract from './RenderLoanApprovalAbstract';
// import styled from 'styled-components';
import RenderOfferCard from '../RenderOfferCard';
import RenderButton from '../RenderButton';
import { strings } from '../../../locales/i18n';
import { customerSupportNumber } from '../../configs/appConstants';
import Communications from 'react-native-communications';

class RenderLoanApprovalComponent extends RenderLoanApprovalAbstract { // eslint-disable-line react/prefer-stateless-function

  makeCall = () => {
    if (customerSupportNumber) {
      window.setTimeout(() => {
        Communications.phonecall(customerSupportNumber.toString(), true);
      }, 100);
    }
  }

  render() {
    const { fieldOrder, formData, renderData } = this.props;
    if (renderData && renderData.finalLoanAmountToDisburse) {
      return (
        <View>
          <RenderOfferCard
            fieldOrder={fieldOrder}
            renderData={renderData}
            formData={formData}
            headingField={'finalLoanAmountToDisburse'}
            showIcon
          >
            <RenderButton
              type={'secondary'}
              text={strings('statusScreen.callUsButton')}
              onPress={this.makeCall}
            />
          </RenderOfferCard>
        </View>
      );
    }
    return null;
  }
}

RenderLoanApprovalComponent.propTypes = {

};

export default RenderLoanApprovalComponent;
