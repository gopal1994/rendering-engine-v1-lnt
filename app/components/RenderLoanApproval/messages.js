/*
 * RenderLoanApproval Messages
 *
 * This contains all the text for the RenderLoanApproval component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.RenderLoanApproval.header',
    defaultMessage: 'This is the RenderLoanApproval component !',
  },
});
