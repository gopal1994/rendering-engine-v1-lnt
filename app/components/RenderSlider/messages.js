/*
 * RenderSlider Messages
 *
 * This contains all the text for the RenderSlider component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.RenderSlider.header',
    defaultMessage: 'This is the RenderSlider component !',
  },
});
