import styled from 'styled-components/native';
import { colors, fonts } from '../../configs/styleVars';
import bgImage from '../../images/current.png';

export const container = {
  style: {
    marginBottom: 30,
  },
};

export const mainWrapper = {
  style: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
};

export const sliderWrapper = {
  style: {
    flex: 1,
    marginHorizontal: 10,
  },
};

export const slider = {
  maximumTrackTintColor: colors.basicLightFontColor,
  minimumTrackTintColor: colors.primaryBGColor,
  thumbTintColor: colors.transparentColor,
  thumbImage: bgImage,
  thumbStyle: {
    width: 20,
    height: 20,
  },
};

export const minMaxWrapper = {
  style: {
    ...mainWrapper.style,
    marginTop: -5,
  },
};

export const icon = {
  style: {
    width: 24,
    height: 24,
    marginTop: 8,
  },
  resizeMode: 'contain',
};

export const label = {
  style: {
    fontSize: fonts.body,
    ...fonts.getFontFamilyWeight(),
  },
};

export const amount = {
  style: {
    color: colors.amountHeading,
    fontSize: fonts.h4,
    ...fonts.getFontFamilyWeight('bold'),
  },
};

export const minMax = {
  style: {
    fontSize: fonts.label,
    ...fonts.getFontFamilyWeight(),
  },
};

