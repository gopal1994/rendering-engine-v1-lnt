/**
*
* RenderSlider
*
*/

import React from 'react';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

export default class RenderSliderAbstract extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {

    };
    this.onChangeHandler = this.onChangeHandler.bind(this);
    this.increaseSlider = this.increaseSlider.bind(this);
    this.decreaseSlider = this.decreaseSlider.bind(this);
  }

  componentWillMount() {

  }

  onChangeHandler(e, value) {
    const meta = this.props.renderData && this.props.renderData.metaData;
    const val = this.props.compProps.isStepIrregluar ? this.props.compProps.valArr[value] : value;
    this.props.onChangeHandler(val, this.props.elmId);
  }

  increaseSlider() {
    const { value, maximumValue, step } = this.props.compProps;
    let newValue;
    if (this.props.compProps.isStepIrregluar) {
      newValue = this.props.compProps.valArr[this.props.compProps.sliderStepIndex + 1];
      (newValue <= this.props.compProps.valArr[maximumValue]) && this.props.onChangeHandler(newValue, this.props.elmId);
    } else {
      newValue = value + step;
      (newValue <= maximumValue) && this.props.onChangeHandler(newValue, this.props.elmId);
    }
  }

  decreaseSlider() {
    const { value, minimumValue, step } = this.props.compProps;
    let newValue;
    if (this.props.compProps.isStepIrregluar) {
      newValue = this.props.compProps.valArr[this.props.compProps.sliderStepIndex - 1];
      (newValue >= this.props.compProps.valArr[minimumValue]) && this.props.onChangeHandler(newValue, this.props.elmId);
    } else {
      newValue = value - step;
      (newValue >= minimumValue) && this.props.onChangeHandler(newValue, this.props.elmId);
    }
  }

  render() {
    return null;
  }
}

