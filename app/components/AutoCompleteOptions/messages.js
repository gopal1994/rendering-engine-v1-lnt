/*
 * AutoCompleteOptions Messages
 *
 * This contains all the text for the AutoCompleteOptions component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.AutoCompleteOptions.header',
    defaultMessage: 'This is the AutoCompleteOptions component !',
  },
});
