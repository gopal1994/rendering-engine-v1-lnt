/**
*
* RenderError
*
*/

import React from 'react';
import { View, Text } from 'react-native';
// import styled from 'styled-components';

import RenderErrorAbstract from './RenderErrorAbstract';
import * as styles from './RenderErrorStyles';

export default class RenderErrorComponent extends RenderErrorAbstract { // eslint-disable-line react/prefer-stateless-function

  render() {
    let { errorMessage } = {...this.props};
    if(!Array.isArray(errorMessage)){
      errorMessage = [errorMessage];
    }
    return (<View>
      {
        errorMessage.map((error, index) => {
          return (<View key={index}><Text {...styles.textStyles}>{error}</Text></View>);
        })
      }
      </View>
    )
  }
}
