import { colors, fonts, lineHeight } from '../../configs/styleVars';

export const navContainer = {
  boxShadow: '0px 0px 20px rgba(0,0,0,0.3)',
  flex: 1,
};

export const navItemWrapper = {
  padding: 15,
  backgroundColor: colors.white,
  borderBottomWidth: 1,
  borderBottomColor: colors.borderLight,
  opacity: 0.4,
};

export const navItemWrapperActive = {
  ...navItemWrapper,
  backgroundColor: colors.primaryBGColor,
  opacity: 1,
};

export const navItemWrapperCompleted = {
  ...navItemWrapper,
  opacity: 1,
};

export const navItemLabel = {
  fontSize: fonts.body,
  color: colors.basicFontColor,
};

export const navItemLabelActive = {
  ...navItemLabel,
  color: colors.white,
};

export const navItemLabelCompleted = {
  ...navItemLabel,
  cursor: 'pointer',
};
