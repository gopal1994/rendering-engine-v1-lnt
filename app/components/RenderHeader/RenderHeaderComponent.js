/**
*
* RenderHeader
*
*/

import React from 'react';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

import RenderHeaderAbstract from './RenderHeaderAbstract';
import HeaderWrapper from './HeaderWrapper';
import * as common from '../../utilities/commonUtils';
import * as styles from './HeaderStyle';


export default class RenderHeaderComponent extends RenderHeaderAbstract { // eslint-disable-line react/prefer-stateless-function

  render() {
    const date = new Date();
    const formattedData = common.formatDefaultDate(date);
    if (this.props.stepData && this.props.stepData.taskName) {
      const screenHeading = (<div><div>{this.props.stepData.taskName }</div><span {...styles.descItalicStyle}>{this.props.stepData.description}</span></div>);
      const loanBlock = (
        <div {...styles.loanDescStyle}>
          <span {...styles.applicationStyle}>Loan Application No.: # {this.props.applicationId} </span>
          <span {...styles.descItalicStyle}>Date: {formattedData} </span>
        </div>
    );
      return (<HeaderWrapper>
        {screenHeading}
        {loanBlock}
      </HeaderWrapper>);
    }

    return null;
  }
}
