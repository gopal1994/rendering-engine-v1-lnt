/**
*
* RenderHeader
*
*/

import React from 'react';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

import RenderHeaderComponent from './RenderHeaderComponent';

class RenderHeader extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <RenderHeaderComponent {...this.props} />
    );
  }
}

RenderHeader.propTypes = {

};

export default RenderHeader;
