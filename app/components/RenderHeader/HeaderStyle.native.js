import { colors, fonts, lineHeight } from '../../configs/styleVars';

export const wrapperStyles = {
  style: {
    width: '100%',
    paddingVertical: 15,
    paddingLeft: 15,
    paddingRight: 15,
    backgroundColor: colors.appHeaderBG,
    marginBottom: 5,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
};

export const headingStyle = {
  style: {
    fontSize: fonts.h2,
    ...fonts.getFontFamilyWeight('normal'),
    lineHeight: lineHeight.h2,
    color: colors.black,
    marginBottom: 8,
  },
};

export const descriptionStyle = {
  style: {
    fontSize: fonts.description,
    ...fonts.getFontFamilyWeight(),
    lineHeight: lineHeight.description,
    color: colors.labelColor,
  },
};

export const applicationStyle = {
  style: {
    color: colors.secondaryFontColor,
    fontSize: 16,
  },
};

export const descItalicStyle = {
  style: {
    ...fonts.getFontFamilyWeight(),
    fontStyle: 'italic',
    fontSize: 12,
  },
};
