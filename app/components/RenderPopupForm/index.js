/**
 *
 * RenderPopupForm (index entry file for component)
 *
 */

import React from 'react';

import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { connect } from 'react-redux';

import * as appActions from '../../containers/AppDetails/actions';
import RenderPopupFormComponent from './RenderPopupFormComponent';

class RenderPopupForm extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
        <RenderPopupFormComponent {...this.props} />
    );
  }
}

RenderPopupForm.propTypes = {

};

const mapStateToProps = createStructuredSelector({

});

function mapDispatchToProps(dispatch) {
    return {
        setPopupData: (reqObject) => dispatch(appActions.setPopupData(reqObject)),
        dispatch,
    };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export default compose(
    withConnect,
)(RenderPopupForm);

