/**
*
* RenderChip
*
*/

import React from 'react';
import { View } from 'react-native';
import Text from '../RenderText';
import { Badge } from 'react-native-elements';
import Icon from 'react-native-vector-icons/MaterialIcons';

import RenderChipAbstract from './RenderChipAbstract';
import * as styles from './RenderChipStyles';

export default class RenderChip extends RenderChipAbstract { // eslint-disable-line react/prefer-stateless-function

  render() {
    const { style, isDefault } = this.props;
    return (
      <View style={style}>
        <Badge {...styles.containerStyle}>
          <Text
            {...styles.chipTextStyle}
          >
            {this.props.label}
          </Text>
          {!isDefault
            ? (<Icon
              name="close"
              {...styles.iconStyle}
              onPress={this.props.onRemoveHandler}
            />)
            : null
          }
        </Badge>
      </View>
    );
  }
}
