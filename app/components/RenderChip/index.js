/**
*
* RenderChip
*
*/

import React from 'react';

import RenderChipComponent from './RenderChipComponent';

class RenderChip extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <RenderChipComponent {...this.props} />
    );
  }
}

RenderChip.propTypes = {

};

export default RenderChip;
