/**
*
* RenderChip
*
*/

import React from 'react';
// import styled from 'styled-components';

import RenderChipAbstract from './RenderChipAbstract';

import Chip from 'material-ui/Chip';


export default class RenderChip extends RenderChipAbstract { // eslint-disable-line react/prefer-stateless-function

  render() {
    return (
      <Chip
        key={this.props.key}
        onRequestDelete={this.props.onRemoveHandler}
      >
        {this.props.label}
      </Chip>
    );
  }
}
