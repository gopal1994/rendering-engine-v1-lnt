/**
*
* Row
*
*/

import React from 'react';
// import styled from 'styled-components';


class Row extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <div className="row" {...this.props}>
        {this.props.children}
      </div>
    );
  }
}

Row.propTypes = {

};

export default Row;
