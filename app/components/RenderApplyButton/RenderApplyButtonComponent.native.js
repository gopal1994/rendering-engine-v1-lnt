/**
 *
 * Apply Loan Native
 *
 */


import React from 'react';
import { View } from 'react-native';
import Text from '../RenderText';

import RenderApplyButtonAbstract from './RenderApplyButtonAbstract';
import RenderButton from '../RenderButton';
import * as styles from './RenderApplyButtonStyles';

export default class RenderApplyButtonComponent extends RenderApplyButtonAbstract { // eslint-disable-line react/prefer-stateless-function

  render() {
    return (
      <View {...styles.wrapperStyles}>
        <View
          {...styles.topViewStyles}
        >
          <Text {...styles.headingStyles}>Congratulations! </Text>
          <Text {...styles.paraStyles}>Your MPIN has been generated. </Text>
        </View>
        <RenderButton
          type={'primary'}
          text="Apply for Loan "
          onPress={this.applyLoan}
          {...styles.btnProps}
        />
      </View>
    );
  }
}

