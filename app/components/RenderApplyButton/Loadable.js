/**
 *
 * Asynchronously loads the component for RenderApplyButton
 *
 */

import Loadable from 'react-loadable';

export default Loadable({
  loader: () => import('./index'),
  loading: () => null,
});
