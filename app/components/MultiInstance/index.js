/**
 *
 * RenderCloningComponentAbstract later to be converted to a helper for editable table too
 *
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import dottie from 'dottie';
import * as validationFns from '../../utilities/validations/validationFns';
import * as styles from "../RenderEditableTable/tableStyles";
class MultiInstance extends Component {

    state = {
        instancesData: {},
        instancesErrorData: {},
        emptyInstance: {},
        actions: {},
    };

    componentDidMount() {
        this.buildInstanceRenderData();
    }

    componentDidUpdate = (prevProps) => {
        if (!this.props.renderData.isValid && prevProps.renderData.toggleInternalValidation !== this.props.renderData.toggleInternalValidation) {
          this.validateWholeInstance();
        }
    };

    addRow = () => {
        const { instancesData, emptyInstance } = this.state;
        const dataKeys = Object.keys(instancesData).map((el) => Number(el));
        const lastKey = dataKeys.length ? Math.max(...dataKeys) : -1;
        const nextKey = Number(lastKey) + 1;
        this.setState({ instancesData: {
                ...instancesData,
                [nextKey]: emptyInstance,
            } }, () => {
            this.updateFormData();
            // this.validateWholeInstance();
        });
    };

    deleteRow = (key) => {
        const { instancesData } = this.state;
        // console.log('deleting ', JSON.parse(JSON.stringify(instancesData[key])));
        delete instancesData[key];
        this.setState({ instancesData }, () => {
            this.updateFormData();
            // this.validateWholeInstance();
        });
    };

    convertValueToData = () => {
        const { formData, elmId } = this.props;
        const { emptyInstance } = this.state;
        let value = formData[elmId];
        value = !value ? { 0: emptyInstance } : value;
        const data = typeof value === "object" ? value : JSON.parse(value);
        // this is to ensure the initial visible error state is visible, even if onchange for each element have'nt been called.
        this.setState({ instancesData: data }, () => {
            // this.validateWholeInstance();
            this.updateFormData();
        });
    };

    onChangeHandler = (value, fieldId, index) => {
        const { instancesData } = this.state;
        const newInstanceErrorData = this.getUnitValidated(value, fieldId, index);
        const newInstance = {
            ...instancesData,
            [index]: {
                ...instancesData[index],
                [fieldId]: value,
            },
        };
        this.setState({ instancesData: newInstance, instancesErrorData: newInstanceErrorData }, () => {
            this.updateFormData()
            // this.updateInstanceErrorState();
        });
    };

    getUnitValidated = (value, fieldId, index, instancesErrorData = this.state.instancesErrorData) => {
        const { journeyRenderData } = { ...this.props };
        const validationObj = validationFns.validateElm(journeyRenderData[fieldId], value, null);
        if (!validationObj) return;
        return ({
            ...instancesErrorData,
            [index]: {
                ...instancesErrorData[index],
                [fieldId]: validationObj,
            },
        });
    };

    buildInstanceRenderData = () => {
        const { renderData, journeyRenderData } = this.props;
        let { metaData: { fields, columns, maxCount, allowAdd, allowDelete}} = renderData;
        let dummyRenderData = {};
        let emptyValues = {};
        let fieldMap = {};
        let actions = {
            ...(allowDelete ? {
                delete: {
                    headerLabel: '',
                    name: 'Delete',
                    onTrigger: this.deleteRow,
                    shouldRender: this.isDeleteAllowed
                }
            } : {}),
        };
        if(!fields) fields = columns;
        fields.forEach((field) => {
            const { fieldId } = field;
            if (journeyRenderData[fieldId].metaData.dummyField) {
                dummyRenderData = {
                    ...dummyRenderData,
                    [fieldId]: journeyRenderData[fieldId],
                };
                field['name'] = journeyRenderData[fieldId].name;
            }
        });
        Object.keys(dummyRenderData).forEach((key) => {
            emptyValues = {
                ...emptyValues,
                [key]: null,
            };
        });

        fields.forEach((field)=>{
            fieldMap[field.fieldId] = field.name;
        });

        this.setState({ emptyInstance: emptyValues, maxCount, fieldMap, allowAdd, allowDelete, actions }, () => {
            this.convertValueToData();
        });
    };

    isDeleteAllowed = () => {
        const { instancesData } = this.state;
        return this.state.allowDelete && (Object.values(instancesData).length > 1);
    }

    isAddAllowed = () => {
        const { instancesData, maxCount } = this.state;
        return  this.state.allowAdd ? ( maxCount ? Object.values(instancesData).length < maxCount : true) : false;
    };

    validateWholeInstance = () => {
        let newInstanceErrorData = {};
        const data = this.state.instancesData;
        Object.keys(data).forEach((instanceIndex) => {
            const instance = data[instanceIndex];
            const instanceKeys = Object.keys(instance);
            instanceKeys.forEach((fieldKey) => {
                newInstanceErrorData = this.getUnitValidated(data[instanceIndex][fieldKey], fieldKey, instanceIndex, newInstanceErrorData);
            });
        });
        this.setState({ instancesErrorData: newInstanceErrorData });
    };

    updateInstanceErrorState = () => {
        const { elmId, journeyRenderData } = { ...this.props };
        let allValid = true;
        let allErrors = [];
        const { instancesErrorData } = this.state;
        Object.values(instancesErrorData).forEach((instanceErrorRow, instanceIndex) => {
            Object.keys(instanceErrorRow).forEach((instanceKey) => {
                let { isValid, errorMessage } = instanceErrorRow[instanceKey];
                allValid = allValid && isValid;
                !isValid && allErrors.push('Row - '+(instanceIndex+1)+': '+errorMessage);
            });
        });

        this.updateFormData();
    };

    updateFormData = () => {
        const { instancesData } = this.state;
        const { onChangeHandler, elmId } = this.props;
        onChangeHandler(JSON.stringify(instancesData), elmId);
    };

    onBlurHandler = (value, elmId) => {
        // this.props.updateFormData(elmId, value);
    };

    render() {
        return null;
    }
}

MultiInstance.propTypes = {
    onChangeHandler: PropTypes.func,
    setRenderData: PropTypes.func,
};

export default MultiInstance;
