import { colors, fonts, lineHeight } from '../../../configs/styleVars';

export const wrapperStyles = {
  backgroundColor: colors.white,
  borderRadius: 6,
  padding: 10,
  marginVertical: 10,
  paddingBottom: 20,
};

export const statusWrap = {
  flexDirection: 'row',
  justifyContent: 'space-between',
  alignItems: 'center',
  paddingVertical: 10,
};

export const labelStyle = {
  fontSize: fonts.label,
  ...fonts.getFontFamilyWeight(),
  lineHeight: lineHeight.label,
  color: colors.hintTextColor,
};

export const headerStyle = {
  fontSize: fonts.body,
  ...fonts.getFontFamilyWeight('body'),
  lineHeight: lineHeight.body,
  color: colors.black,
  paddingVertical: 10,
};

export const descriptionStyle = {
  fontSize: fonts.body,
  ...fonts.getFontFamilyWeight(),
  lineHeight: lineHeight.body,
  color: colors.black,
};

export const indicatorLabel = {
  fontSize: fonts.label,
  ...fonts.getFontFamilyWeight(),
  lineHeight: lineHeight.label,
  paddingLeft: 5,
};

export const indicatorWrap = {
  flexDirection: 'row',
  alignItems: 'center',
};
