import React from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import { tabNavList } from '../../../configs/appConstants';
import loans from '../../../images/loans.png';
import support from '../../../images/support.png';
import profile from '../../../images/profile.png';
import dashboard from '../../../images/dashboard.png';
import notification from '../../../images/notification-ribbon.png';
import dashboardActive from '../../../images/dashboard-active.png';
import loansActive from '../../../images/loans-active.png';
import supportActive from '../../../images/support-active.png';
import profileActive from '../../../images/profile-active.png';
import notificationActive from '../../../images/notification-active.png';
import { wrapperStyles, drawerMenuStyle, selectedTabText, tabText } from './styles.native';
import { getNavLocales } from '../../../containers/RenderPostJounery/RenderPostJourneyComponent.native';

const iconScreenMap = {
  Dashboard: {
    icon: dashboard,
    iconActive: dashboardActive,
  },
  Loans: {
    icon: loans,
    iconActive: loansActive,
  },
  Notification: {
    icon: notification,
    iconActive: notificationActive,
  },
  Support: {
    icon: support,
    iconActive: supportActive,
  },
  Profile: {
    icon: profile,
    iconActive: profileActive,
  },
};

const BottomNavBar = (props) => {
  const tabNavs = tabNavList.map((title) => {
    const isActiveScreen = title === props.currentScreen;
    const iconSource = isActiveScreen ? iconScreenMap[title].iconActive : iconScreenMap[title].icon;
    const tabTextStyle = isActiveScreen ? selectedTabText : tabText;
    return (
      <TouchableOpacity
        onPress={() => props.navigateToScreen(title)}
        key={title}
      >
        <View style={drawerMenuStyle}>
          <Image source={iconSource} resizeMode={'contain'} />
          <Text style={tabTextStyle}>{getNavLocales(title)}</Text>
        </View>
      </TouchableOpacity>
    );
  }
    );
  return (
    <View style={wrapperStyles}>
      {tabNavs}
    </View>
  );
};

export default BottomNavBar;
