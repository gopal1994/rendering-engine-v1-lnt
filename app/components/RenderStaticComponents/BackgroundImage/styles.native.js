import { Dimensions } from 'react-native';
import { colors } from '../../../configs/styleVars';

export const wrapperStyle = {
  flex: 1,
  backgroundColor: colors.secondaryBGColor,
};

export const imageContainerStyle = {
  position: 'absolute',
  top: 0,
  left: 0,
  width: '100%',
  height: '100%',
};

export const imageStyle = {
  flex: 1,
  width: Dimensions.get('window').width,
  height: Dimensions.get('window').height,
};

export const contentContainerStyle = {
  flex: 1,
  backgroundColor: 'transparent',
  justifyContent: 'center',
};
