import { fonts } from '../../../configs/styleVars';

export const rowWrap = {
  flexDirection: 'row',
  justifyContent: 'space-between',
  marginVertical: 5,
};

export const textStyles = {
  ...fonts.getFontFamilyWeight(),
  flex: 1,
};

export const headingStyles = {
  ...fonts.getFontFamilyWeight('bold'),
  flex: 1,
};
