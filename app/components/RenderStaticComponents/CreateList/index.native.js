import React from 'react';
import PropTypes from 'prop-types';
import { View, Text } from 'react-native';
import { rowWrap, headingStyles, textStyles } from './styles.native';
import * as common from '../../../utilities/commonUtils';

class CreateList extends React.PureComponent {

  getItem = (label, isHeading, isCurrency) => {
    const textStyle = isHeading ? headingStyles : textStyles;
    const formattedLabel = isCurrency ? common.convertCurrencyFormat(label) : label;
    return (<Text style={textStyle}>{formattedLabel}</Text>);
  }

  render() {
    const { header, data, includingHeader } = this.props;
    return (
      <View>
        {data.map((row, index) => (
          <View key={index} style={rowWrap}>
            {header.map((item) => (
              this.getItem(row[item.value], item.isHeading, item.isCurrency)
            ))}
          </View>
        ))}
      </View>
    );
  }
}

CreateList.propTypes = {
  showFilters: PropTypes.func,
};

export default CreateList;
