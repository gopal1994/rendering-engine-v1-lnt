import { colors, fonts, lineHeight, sizes } from '../../../configs/styleVars';


export const wrapperStyle = { paddingHorizontal: sizes.appPaddingHorizontal, paddingVertical: 10, justifyContent: 'space-between', flex: 1 };

export const titleStyle = {
  ...fonts.getFontFamilyWeight('bold'),
  fontSize: fonts.h3,
  lineHeight: lineHeight.h3,
};

export const descStyle = {
  ...fonts.getFontFamilyWeight(),
  fontSize: fonts.label,
  lineHeight: lineHeight.label,
  colors: colors.labelColor,
};

