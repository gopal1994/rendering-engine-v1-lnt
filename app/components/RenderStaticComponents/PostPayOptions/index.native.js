import React from 'react';
import PropTypes from 'prop-types';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import RenderButton from '../../RenderButton';
import * as styles from './styles.native';
import { colors, fonts, lineHeight, sizes } from '../../../configs/styleVars';
import { ListItem } from '../PaymentOptionsModal/index';
import { strings } from '../../../../locales/i18n';


export default class PostPayModal extends React.Component { // eslint-disable-line react/prefer-stateless-functionconst PostPayModal = ({ status,  }) => (
  constructor(props) {
    super(props);
    this.state = {
      showPayOptionsModal: false,
      selectedMethod: '',
    };
  }

  togglePayOptionsModal = (flag) => {
    this.setState({ showPayOptionsModal: flag });
  }

  handleSelectedMethod = (selectedMethod) => {
    this.setState({ selectedMethod });
  }
  render() {
    let buttonBlock = null;
    if (this.props.showPayButton) {
      buttonBlock = (<View>
        <RenderButton text={this.props.buttonText} type={'primary'} onPress={() => this.props.onPayPres()} />
      </View>);
    }
    return (<View style={styles.wrapperStyle}>
      <View>
        <Text style={styles.titleStyle}>{this.props.title}</Text>
        <Text style={styles.descStyle}>{this.props.description}</Text>
        {this.props.payOptions.map((option) => (
          <ListItem
            label={option.label}
            leftIcon={option.leftIcon}
            key={option.id}
            value={option.id}
            onPress={this.handleSelectedMethod}
            style={{
              borderColor: this.state.selectedMethod === option.id
                ? colors.primaryBGColor : colors.underlineColor,
              borderBottomColor: this.state.selectedMethod === option.id
                ? colors.primaryBGColor : colors.underlineColor,
              marginVertical: 7,
              borderWidth: 1,
              borderRadius: 6 }}
          />
      ))}
      </View>
      {buttonBlock}
    </View>);
  }
}
