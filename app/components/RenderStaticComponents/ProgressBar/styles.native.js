import { colors } from '../../../configs/styleVars';

const TRACK_WIDTH = 8;
const TRACK_COLOR = colors.secondaryBGColor;
const FILLED_TRACK_COLOR = colors.primaryBGColor;

export const getWrapperStyle = (trackWidth = TRACK_WIDTH) => ({
  position: 'relative',
  height: trackWidth,
});

export const getTrackStyle = (trackWidth = TRACK_WIDTH) => ({
  position: 'absolute',
  height: trackWidth,
  borderRadius: trackWidth / 2,
  width: '100%',
});

export const getUnfilledTrackStyle = (trackWidth, trackColor) => {
  const trackStyles = getTrackStyle(trackWidth);
  return {
    ...trackStyles,
    backgroundColor: trackColor || TRACK_COLOR,
    zIndex: 1,
  };
};

export const getFilledTrackStyle = (trackWidth, filledTrackColor, filled) => {
  const trackStyles = getTrackStyle(trackWidth);
  return {
    ...trackStyles,
    backgroundColor: filledTrackColor || FILLED_TRACK_COLOR,
    zIndex: 2,
    width: `${filled}%`,
  };
};
