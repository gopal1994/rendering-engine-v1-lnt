import React from 'react';
import PropTypes from 'prop-types';
import { View, TouchableOpacity, Image } from 'react-native';
import hamMenu from '../../../images/hamburger.png';
import notificationIcon from '../../../images/notification.png';
import { wrapperStyles } from './styles.native';


const PostHeader = (props) => (
  <View style={wrapperStyles}>
    <TouchableOpacity
      onPress={props.onMenuPress}
    >
      <Image
        source={hamMenu}
      />
    </TouchableOpacity>
    <TouchableOpacity
      onPress={() => {}}
    >
      <Image
        source={notificationIcon}
      />
    </TouchableOpacity>

  </View>);

PostHeader.propTypes = {
  onMenuPress: PropTypes.func,
};

export default PostHeader;
