import React from 'react';
import PropTypes from 'prop-types';
import { View, TouchableOpacity, Image, Text } from 'react-native';
import editIcon from '../../../images/edit.png';
import * as styles from './styles.native';


const ProfileDetail = ({ title, details, isEditable, onEdit }) => (
  <View style={styles.wrapperStyle}>
    <View style={styles.titleWrap}>
      <Text style={styles.titleStyle}>{title}</Text>
      {
        isEditable
            ? <TouchableOpacity onPress={onEdit}>
              <Image source={editIcon} />
            </TouchableOpacity>
            : null
    }
    </View>
    <View>
      {
            details.map((detail, index) => (
              <View key={`${title}_${index}`} style={styles.detailWrap}>
                <View style={styles.detailLabelWrap}>
                  <Image source={detail.icon} />
                  <Text style={styles.detailLabelStyle}>{detail.label}</Text>
                </View>
                <View style={styles.detailValueWrap}>
                  <Text style={styles.detailValueStyle}>{detail.value}</Text>
                </View>
              </View>
            ))
        }
    </View>
  </View>);

ProfileDetail.propTypes = {
  onMenuPress: PropTypes.func,
};

export default ProfileDetail;
