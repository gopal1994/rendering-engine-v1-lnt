import React from 'react';
import PropTypes from 'prop-types';
import { View, Text, TouchableOpacity, Image, NativeModules } from 'react-native';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import * as styles from './styles.native';
import { strings } from '../../../../locales/i18n';
import { makeSelectUserLanguage, makeSelectDashboardData } from '../../../containers/AppDetails/selectors';
import { ModalOverlay, PostPayOptions } from '../../RenderStaticComponents';
import iconMap from '../../../utilities/iconMap';
import RenderPopup from '../../RenderPopup';
import * as appActions from '../../../containers/AppDetails/actions';
import RenderButton from '../../RenderButton/';

const PAY_METHOD = {
  PAY_CASH: 'PAY_CASH',
  PAY_BANK: 'PAY_BANK',
};

const PAYMENT_STATUS = {
  SUCCESS: 0,
  FAIL: 1,
  UNKNOWN: 2,
  CANCEL: 3,
};

export const ListItem = ({ label, leftIcon, rightIcon, onPress, value, style, listWrapStyle, textStyle }) => (
  <TouchableOpacity onPress={() => onPress(value)} style={style}>
    <View style={[styles.listWrap, listWrapStyle]} >
      <View style={styles.labelWrap}>
        <Image source={iconMap[leftIcon]} />
        <View style={styles.textWrapStyle}>
          <Text style={[styles.textStyle, textStyle]}>{label}</Text>
        </View>
      </View>
      <Image source={iconMap[rightIcon]} />
    </View>
  </TouchableOpacity>
);
class PaymentOptionsModal extends React.Component { // eslint-disable-line react/prefer-stateless-functionconst PostPayModal = ({ status,  }) => (
  constructor(props) {
    super(props);
    this.state = {
      showPayOptionsModal: false,
      selectedPayMethod: '',
      payOptionsList: [],
      popupData: '',
    };
  }
  getPayOptionsList = () => {
    const payOptions = [
      {
        label: strings('postpayOptions.byCash'),
        leftIcon: 'paymentCash',
        rightIcon: 'rightArrow',
        id: PAY_METHOD.PAY_CASH,
        style: styles.listTouchWrap,
      },
      {
        label: strings('postpayOptions.byBank'),
        leftIcon: 'paymentBanking',
        rightIcon: 'rightArrow',
        id: PAY_METHOD.PAY_BANK,
        style: styles.listTouchWrap,
      },
    ];
    return payOptions.map((option) => (
      <ListItem label={option.label} leftIcon={option.leftIcon} key={option.id} value={option.id} onPress={this.handlePayOptions} />
    ));
  }

  handlePayOptions = (payMethod) => {
    this.setState({ selectedPayMethod: payMethod });
    let payOptions;
    if (payMethod === PAY_METHOD.PAY_BANK) {
      payOptions = [
        {
          label: 'Payoo',
          id: 'payoo',
          leftIcon: 'payoo',
        },
      ];
    } else {
      payOptions = [
        {
          label: 'Vietnam Post',
          id: 'vietnamPost',
          leftIcon: 'vnp',
        },
        {
          label: 'BIDV',
          id: 'bidv',
          leftIcon: 'bidv',
        },
        {
          label: 'Agribank',
          id: 'agribank',
          leftIcon: 'agb',
        },
        {
          label: 'VPBank',
          id: 'vpBank',
          leftIcon: 'vpb',
        },
      ];
    }
    this.setState({ payOptionsList: payOptions });
    this.handlePayMethodModal(true);
  }

  handlePayMethodModal = (flag) => {
    this.setState({ showPayOptionsModal: flag });
  }

  getSuccessPopUpData = () => {
    const self = this;
    return {
      headerText: strings('payOnlineModal.successHeading'),
      bodyText: strings('payOnlineModal.successBody'),
      actionButtons: [{
        label: strings('payOnlineModal.successBtn'),
        callback: () => {
          self.props.navigateToScreen('Loans', { paymentDone: true });
        },
        action: 'callback',
        type: 'primary',
      }],
      type: 'success',
      showPopup: true,
    };
  }

  getErrorMessage = (type) => {
    if (type === PAYMENT_STATUS.FAIL) {
      return 'Payment Failed';
    } else if (type === PAYMENT_STATUS.CANCEL) {
      return 'Payment Cancelled';
    }
    return 'Something went wrong';
  }

  getErrorPopUpData = (type) => {
    const self = this;
    return {
      headerText: strings('support.createSuccessTitle'),
      bodyText: strings('support.createSuccessInfo'),
      actionButtons: [{
        label: strings('support.createSuccessCta'),
        callback: () => {
          self.props.navigateToScreen('Loans', { paymentDone: true });
        },
        action: 'callback',
        type: 'primary',
      }],
      type: 'error',
      showPopup: true,
    };
  }

  triggerPayment = () => {
    const { userLanguage, dashboardData } = this.props;
    const { emailId, contactNumber } = dashboardData;
    // if (NativeModules.PayooModule && NativeModules.PayooModule.openPayoo) {
    //   console.log('this.props.data.loanContractNumber', this.props);
    //   NativeModules.PayooModule.openPayoo(userLanguage, emailId, contactNumber, this.props.data.loanContractNumber, (res) => {
    //     console.debug('this is the payoo response', res);
    //     let popupData = '';
    //     if (res === PAYMENT_STATUS) {
    //       popupData = this.getSuccessPopUpData();
    //     } else {
    //       popupData = this.getErrorPopUpData(res);
    //     }
    //     this.setState({ popupData });
    //   });
    // }
  }


  render() {
    const { popupData } = this.state;
    return (<View style={styles.wrapperStyle}>
      <View>
        <Text style={styles.headerStyle} >{strings('postpayOptions.title')}</Text>
        {this.getPayOptionsList()}
      </View>
      <ModalOverlay
        onBack={() => this.handlePayMethodModal(false)}
        isVisible={this.state.showPayOptionsModal}
        headerType={'white'}
      >
        <PostPayOptions
          payOptions={this.state.payOptionsList}
          buttonText={strings('postpayOptions.buttonCta')}
          showPayButton={this.state.selectedPayMethod === PAY_METHOD.PAY_BANK}
          onPayPres={this.triggerPayment}
          title={
            this.state.selectedPayMethod === PAY_METHOD.PAY_CASH
            ? strings('postpayOptions.byCash')
            : strings('postpayOptions.byBank')
          }
          description={
            this.state.selectedPayMethod === PAY_METHOD.PAY_CASH
            ? strings('postpayOptions.cashDescription')
            : ''
          }
        />
        <RenderPopup
          popupData={popupData}
        />
      </ModalOverlay>
    </View>);
  }
}

const mapStateToProps = createStructuredSelector({
  userLanguage: makeSelectUserLanguage(),
  dashboardData: makeSelectDashboardData(),
});

function mapDispatchToProps(dispatch) {
  return {
    navigateToScreen: (screen, params) => dispatch(appActions.navigateToSCreen(screen, params)),
    dispatch,
  };
}

PaymentOptionsModal.propTypes = {
  status: PropTypes.string,
};

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export default compose(
  withConnect,
)(PaymentOptionsModal);


// export default PostPayModal;
