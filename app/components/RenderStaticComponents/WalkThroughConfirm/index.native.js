import React from 'react';
import PropTypes from 'prop-types';
import { View, Text, ScrollView, WebView } from 'react-native';
import RenderButton from '../../RenderButton';
import * as styles from './styles.native';

const WalkThroughConfirm = ({ header, docUrl, onButtonClick, buttonText }) => (
  <View style={styles.wrapperStyle}>
    <View style={styles.topViewStyle}>
      <Text style={styles.headerStyle}>{header}</Text>
      <WebView
        startInLoadingState
        scalesPageToFit
        javaScriptEnabled
        domStorageEnabled
        source={docUrl}
        style={styles.webViewStyle}
      />
    </View>
    {/* <View style={styles.buttonWrapStyle}>
      <RenderButton
        type={'primary'}
        text={buttonText}
        onPress={onButtonClick}
      />
    </View> */}
  </View>);

WalkThroughConfirm.propTypes = {
  onButtonClick: PropTypes.func,
};

export default WalkThroughConfirm;
