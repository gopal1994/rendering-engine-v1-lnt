import * as permissionService from '../../utilities/native/permissionService';

export const getPermission = async () => {
  try {
    const granted = await permissionService.getPermission(['storageWrite']);
    return granted;
  } catch (err) {
    console.warn(err);
  }
};
