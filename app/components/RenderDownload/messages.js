/*
 * RenderDownload Messages
 *
 * This contains all the text for the RenderDownload component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.RenderDownload.header',
    defaultMessage: 'This is the RenderDownload component !',
  },
});
