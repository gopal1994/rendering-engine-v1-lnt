/**
*
* RenderDownload
*
*/

import React from 'react';
// import styled from 'styled-components';
import { connect } from 'react-redux';
import { compose } from 'redux';

import * as appActions from '../../containers/AppDetails/actions';
import RenderDownloadComponent from './RenderDownloadComponent';

class RenderDownload extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <RenderDownloadComponent {...this.props} />
    );
  }
}

RenderDownload.propTypes = {

};


function mapDispatchToProps(dispatch) {
  return {
    toggleLoader: (data) => dispatch(appActions.updateLoadingState(data)),
    dispatch,
  };
}

const withConnect = connect(null, mapDispatchToProps);

// export default RenderRestInput;
export default compose(
  withConnect,
)(RenderDownload);
