/**
*
* Slider
*
*/

import React from 'react';
import {
  View,
  Image,
  TouchableOpacity,
  Dimensions,
  Text,
} from 'react-native';
// import styled from 'styled-components';
import { styles } from './RenderWalkThroughStyles.native';
import LogoSrc from '../../images/question-mark.png';
import { strings } from '../../../locales/i18n';
import { List, ListItem } from 'react-native-elements';
const { width, height } = Dimensions.get('window');
const slidImage = {
  width,
  height,
};
const Slider = (props) => (
  <View style={styles.slideView}>
    <Image
      style={styles.slideImage}
      source={props.imgSrc}
      resizeMode="cover"
    />
    <View style={styles.sliderInnerView}>
      {/* <View style={{ flexDirection: 'row' }}> */}
      <Text style={[styles.sliderTextHead, { flexDirection: 'row', flexWrap: 'wrap', alignItems: 'center' }]}>
        {props.textHead}
      </Text>
      {/* </View> */}
      {getDescription(props)}
    </View>
    <View style={styles.skewedBlock}></View>
  </View>
  );
const getDescription = (props) => {
  if (Array.isArray(props.textDescription)) {
    return (
      <View>
        {props.textDescription.map((description) => (
          <View type={'description'} style={styles.descriptionWrapper} key={description}>
            <Text style={styles.listIconStyles}>•</Text>
            <Text style={styles.sliderTextDes}>{description}</Text>
          </View>
        ))}
      </View>
    );
  }
  return (
    <View style={{ flexDirection: 'row', flex: 1, alignContent: 'flex-start' }}>
      <Text type={'description'} style={styles.sliderTextDes}>{props.textDescription}</Text>
      <TouchableOpacity style={{ width: 20, height: 20, marginLeft: 10 }} onPress={props.showProductInfo}>
        <Image
          source={LogoSrc}
          style={{ width: 20, height: 20 }}
        />
      </TouchableOpacity>
    </View>
  );
};
export default Slider;
