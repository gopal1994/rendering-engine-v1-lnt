import bg1 from './img/1.jpg';
import bg2 from './img/2.jpg';
import bg3 from './img/3.jpg';
import bg4 from './img/4.jpg';
import bg5 from './img/5.jpg';
import bg6 from './img/6.jpg';
import { strings } from '../../../locales/i18n';

export const slidesData = [
  {
    imgSrc: bg1,
    textHead: 'swiperScreen.personalLoan',
    textDescription: 'swiperScreen.personalLoanDis',
  }, {
    imgSrc: bg2,
    textHead: 'swiperScreen.regitration',
    textDescription: 'swiperScreen.regitrationDis',
  }, {
    imgSrc: bg3,
    textHead: 'swiperScreen.identetyVer',
    textDescription: 'swiperScreen.identetyVerDis',
  }, {
    imgSrc: bg4,
    textHead: 'swiperScreen.incomeVer',
    textDescription: 'swiperScreen.incomeVerDis',
  }, {
    imgSrc: bg5,
    textHead: 'swiperScreen.reviewVer',
    textDescription: 'swiperScreen.reviwverDis',
  }, {
    imgSrc: bg6,
    textHead: 'swiperScreen.constract',
    textDescription: 'swiperScreen.constractDis',
  },
];

export const getSlideData = () => slidesData.map((slideData) => (
  {
    ...slideData,
    textHead: strings(slideData.textHead),
    textDescription: strings(slideData.textDescription),
  }
));
