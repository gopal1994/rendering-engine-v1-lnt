export const geoSubsectionLeft = {
  width: 'auto',
  marginRight: '20px',
};
export const geoSubsectionRight = {
  width: '80%',
};

export const geoMainWrapper = {
  flexDirection: 'row',
  display: 'flex',
};

export const disabledWrapper = {
  opacity: 0.4,
  cursor: 'not-allowed',
};
