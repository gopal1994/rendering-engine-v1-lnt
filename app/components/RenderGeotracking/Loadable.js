/**
 *
 * Asynchronously loads the component for RenderGeotracking
 *
 */

import Loadable from 'react-loadable';

export default Loadable({
  loader: () => import('./index'),
  loading: () => null,
});
