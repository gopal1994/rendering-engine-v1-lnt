/**
*
* RenderGeotrackingComponent
*
*/

import React from 'react';
import RenderGeotrackingAbstract from './RenderGeotrackingAbstract';
import * as styles from './RenderGeotrackingStyles';
import styledComponents from '../../configs/styledComponents';
import RenderError from '../RenderError';

// import styled from 'styled-components';


class RenderGeotrackingComponent extends RenderGeotrackingAbstract { // eslint-disable-line react/prefer-stateless-function

  render() {
    const { compProps } = this.props;
    const renderData = this.props.renderData;
    const errorMessage = renderData.errorMessage;
    let error = null;
    if (errorMessage) {
      error = (<RenderError errorMessage={errorMessage} />);
    }

    return (
      <div style={styles.geoMainWrapper}>
        <div style={styles.geoSubsectionLeft}>
          <div onClick={this.locate} style={compProps.value ? styles.disabledWrapper : {}}>
            {styledComponents.getLinkStyledText(compProps.label || 'Geo Tag')}
          </div>
          <div>
            {error}
          </div>
        </div>
        <div style={styles.geoSubsectionRight}>
          {/* <div>Latitude: {this.state.latitude && this.state.latitude.toFixed(4)}</div>*/}
          {/* <div>Longitude: {this.state.longitude && this.state.longitude.toFixed(4)}</div>*/}
          {this.state.address ? <div>Area : {this.state.address}</div> : null}
          {this.state.error ? <div>Error: {this.state.error}</div> : null}
        </div>
      </div>
    );
  }
}

RenderGeotrackingComponent.propTypes = {

};

export default RenderGeotrackingComponent;
