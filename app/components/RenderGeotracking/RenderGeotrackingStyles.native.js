import { colors } from '../../configs/styleVars';
// const colors = styleVars.colors;

export const locateBtnStyle = {
  style: {
    buttonStyle: {
      borderRadius: 0,
      backgroundColor: 'transparent',
      paddingTop: 0,
      paddingBottom: 0,
      paddingLeft: 0,
      paddingRight: 0,
    },
    textStyle: {
      color: colors.primaryBGColor,
      textAlign: 'left',
    },
  },
};

export const geoSubsectionLeft = {
  width: '20%',
};
export const geoSubsectionRight = {
  width: '80%',
};

export const geoMainWrapper = {
  flex: 1,
  flexDirection: 'row',
};

export const disabledWrapper = {
  opacity: 0.4,
  cursor: 'not-allowed',
};
