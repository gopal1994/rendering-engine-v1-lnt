import { colors, fonts, lineHeight } from '../../configs/styleVars';

export const wrapperStyles = {
  style: {
    padding: 15,
    flexDirection: 'row',
    backgroundColor: colors.appHeaderBG,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
};

export const headingStyle = {
  style: {
    fontSize: fonts.headerSize,
    ...fonts.getFontFamilyWeight(),
    color: colors.appHeaderFontColor,
  },
};


export const buttonStyle = {
  style: {
    buttonStyle: {
      height: 56,
      borderRadius: 0,
      width: '100%',
    },
  },
};

export const dialogContentView = {
  style: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 15,
  },
};

export const iconWrapper = {
  style: {
    overflow: 'visible',
    alignItems: 'center',
  },
};
export const iconStyles = {
  style: {
    top: '50%',
    zIndex: 100,
    height: 70,
    width: 70,
  },
  resizeMode: 'stretch',
};

export const heading = {
  style: {
    textAlign: 'center',
    fontSize: fonts.h2,
    ...fonts.getFontFamilyWeight('bold'),
    lineHeight: lineHeight.h2,
    color: colors.primaryBGColor,
  },
};

export const errorHeading = {
  style: {
    ...heading.style,
    color: colors.errorColor,
  },
};

export const bodyText = {
  style: {
    paddingTop: 15,
    paddingBottom: 15,
    textAlign: 'center',
    fontSize: fonts.h4,
    ...fonts.getFontFamilyWeight(),
    lineHeight: lineHeight.h4,
  },
};

export const bodyWrapper = {
  style: {
    backgroundColor: colors.white,
    paddingTop: 40,
    borderRadius: 6,
    overflow: 'hidden',
  },
};

export const textWrapper = {
  style: {
    paddingHorizontal: 30,
  },
};

export const buttonWrapper = {
  style: {
    marginTop: 10,
    display: 'flex',
    justifyContent: 'flex-end',
  },
};
