/**
*
* RenderSlider
*
*/

import React from 'react';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';
import { Step, Stepper, StepLabel, StepButton } from 'material-ui/Stepper';
import RenderStepperAbstract from './RenderStepperAbstract';
import * as styles from './StepperStyles';
import { colors } from '../../configs/styleVars';

export default class RenderStepperComponent extends RenderStepperAbstract { // eslint-disable-line react/prefer-stateless-function

  onPress = (index, idx) => {
    console.debug('what is index', index, idx);
    if (index < this.state.currentActiveIndex) {
      const { defaultScreenList, backTrigger } = this.props;
      const backScreenKey = Array.isArray(defaultScreenList) && defaultScreenList[index];
      if (backScreenKey) {
        backTrigger(backScreenKey);
      }
    }
  }

  render() {
    const { currentActiveIndex } = this.state,
      { labels } = this.props,
      maxWidth = `${100 / labels.length}%`;
    const stepperList = labels.map((label, index) => (
      <Step
        style={{ maxWidth }}
        key={label}
      >
        <StepButton onClick={() => this.onPress(index)}>{label}</StepButton>
      </Step>
    ));
    return (
      <Stepper activeStep={currentActiveIndex}>
        { stepperList }
      </Stepper>
    );
  }
}

