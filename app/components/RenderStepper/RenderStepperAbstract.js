/**
*
* RenderSlider
*
*/

import React from 'react';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

export default class RenderStepperAbstract extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {

    };
  }
  componentWillMount = () => {
    if (this.props.labels) {
      this.setState({
        currentActiveIndex: this.props.labels.indexOf(this.props.currentActiveStep),
        stepCount: this.props.labels.length,
      });
    }
  }

  componentWillReceiveProps = (nextProps) => {
    if (nextProps.labels) {
      this.setState({
        currentActiveIndex: nextProps.labels.indexOf(nextProps.currentActiveStep),
        stepCount: nextProps.labels && nextProps.labels.length,
      });
    }
  }

  onChangeHandler = (e, value) => {
    this.props.onChangeHandler(value, this.props.elmId);
  }

  render() {
    return null;
  }
}

