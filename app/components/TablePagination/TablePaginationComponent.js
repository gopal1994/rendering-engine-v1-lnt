/**
 *
 * TablePaginationComponent
 *
 */

import React from 'react';
import Text from '../../components/RenderText';
import ClickableDiv from '../../components/AppHeader/ClickableDiv';
import rightArrowDisabled from '../../images/right-disabled.png';
import rightArrowEnabled from '../../images/right-enabled.png';
import leftArrowDisabled from '../../images/left-disabled.png';
import leftArrowEnabled from '../../images/left-enabled.png';


import TablePaginationAbstract from './TablePaginationAbstract';
// import styled from 'styled-components';
import * as styles from './tablePaginationStyles';

class TablePaginationComponent extends TablePaginationAbstract { // eslint-disable-line react/prefer-stateless-function
  render() {
    const {
            onPressNext,
            onPressPrev,
            paginationOptions,
        } = this.props;
    const { size, page, dataLength } = paginationOptions;
    const from = dataLength ? (page * size) + 1 : dataLength;
    const to = dataLength < ((page + 1) * size) ? dataLength : ((page + 1) * size);
    const paginationLabel = this.customPaginationLabel(from, to, dataLength);
    const isFirstPage = page === 0;
    const isLastPage = to >= dataLength;
    return (
    dataLength ?
        (<div style={styles.paginationWrapper}>
          <div>
            <Text> { paginationLabel } </Text>
          </div>
          <div style={styles.paginationButtonsWrapper} >
            <ClickableDiv onClick={() => { if (!isFirstPage) onPressPrev(); }} style={styles.paginationIconWrapper}>
              <img
                style={styles.paginationIcon}
                src={isFirstPage ? leftArrowDisabled : leftArrowEnabled}
              />
            </ClickableDiv>
            <ClickableDiv onClick={() => { if (!isLastPage) onPressNext(); }} style={styles.paginationIconWrapper}>
              <img
                style={styles.paginationIcon}
                src={isLastPage ? rightArrowDisabled : rightArrowEnabled}
              />
            </ClickableDiv>
          </div>
        </div>)
        : null
    );
  }
}

TablePaginationComponent.propTypes = {

};

export default TablePaginationComponent;
