/**
*
* TablePaginationComponent
*
*/

import React from 'react';
import { View, TouchableOpacity, Image } from 'react-native';
import Text from '../../components/RenderText';
import rightArrowDisabled from '../../images/right-disabled.png';
import rightArrowEnabled from '../../images/right-enabled.png';
import leftArrowDisabled from '../../images/left-disabled.png';
import leftArrowEnabled from '../../images/left-enabled.png';


import TablePaginationAbstract from './TablePaginationAbstract';
// import styled from 'styled-components';
import * as styles from './tablePaginationStyles.native';

class TablePaginationComponent extends TablePaginationAbstract { // eslint-disable-line react/prefer-stateless-function
  render() {
    const {
      onPressNext,
      onPressPrev,
      paginationOptions,
    } = this.props;
    const { size, page, dataLength } = paginationOptions;
    const from = dataLength ? (page * size) + 1 : dataLength;
    const to = dataLength < ((page + 1) * size) ? dataLength : ((page + 1) * size);
    const paginationLabel = this.customPaginationLabel(from, to, dataLength);
    const isFirstPage = page === 0;
    const isLastPage = to >= dataLength;
    return (
      dataLength ?
        (<React.Fragment>
          <View>
            <Text> { paginationLabel } </Text>
          </View>
          <View style={styles.paginationButtonsWrapper} >
            <TouchableOpacity onPress={() => { if (!isFirstPage) onPressPrev(); }} style={styles.paginationIconWrapper}>
              <Image
                style={styles.paginationIcon}
                source={isFirstPage ? leftArrowDisabled : leftArrowEnabled}
              />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => { if (!isLastPage) onPressNext(); }} style={styles.paginationIconWrapper}>
              <Image
                style={styles.paginationIcon}
                source={isLastPage ? rightArrowDisabled : rightArrowEnabled}
              />
            </TouchableOpacity>
          </View>
        </React.Fragment>)
      : null
    );
  }
}

TablePaginationComponent.propTypes = {

};

export default TablePaginationComponent;
