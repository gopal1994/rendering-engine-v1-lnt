/**
*
* RenderMultiDropdownComponent Native
*
*/

import React from 'react';
import RenderMultiDropdownAbstract from './RenderMultiDropdownAbstract';
import MultiDropDown from '../MultiDropDown/';

export default class RenderMultiDropdownComponent extends RenderMultiDropdownAbstract { // eslint-disable-line react/prefer-stateless-function
  onChange = (value) => {
    const { elmId, onChangeHandler } = this.props;
    onChangeHandler(value, elmId);
  }

  render() {
    const { enumValues: enums } = this.props.renderData;
    const { compProps } = this.props;
    // check if the default empty string is received
    if (compProps.value === '') {
      // change the default empty string to empty array string
      compProps.value = '[]';
    }
    const options = enums.map(({ name, id }) => ({ name, value: id }));
    const displayNames = enums.reduce((acc, { name, id }) => {
      const valueIndex = JSON.parse(compProps.value).indexOf(id);
      if (valueIndex !== -1) {
        return [
          ...acc,
          name,
        ];
      }
      return acc;
    }, []);
    return (
      <MultiDropDown
        {...compProps}
        value={compProps.value}
        displayNames={displayNames}
        options={options}
        onChangeHandler={this.onChange}
      />
    );
  }
}

/* Received props are
All the default styling
Behavioural attributes

onChange Fn
*/
