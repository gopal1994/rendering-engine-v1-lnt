/**
*
* RenderMultiDropdownComponent
*
*/

import React from 'react';
import RenderMultiDropdownAbstract from './RenderMultiDropdownAbstract';
import MultiDropDown from '../MultiDropDown/';

export default class RenderMultiDropdownComponent extends RenderMultiDropdownAbstract { // eslint-disable-line react/prefer-stateless-function

  updateChange = (option) => {
    this.onChangeHandler(!option.length ? '' : JSON.stringify(option));
  }

  render() {
    const { enumValues: enums, errorMessage: error } = this.props.renderData;
    const options = enums.map(({ name, id }) => ({ name, value: id }));
    const { compProps } = this.props;

    if (compProps.value === '') {
      compProps.value = [];
    } else {
      compProps.value = JSON.parse(compProps.value);
    }

    return (
      <MultiDropDown
        options={options}
        error={error}
        className="field-container"
        compProps={{ ...compProps }}
        onChangeHandler={this.updateChange}
      />
    );
  }
}

/* Received props are
All the default styling
Behavioural attributes

onChange Fn
*/
