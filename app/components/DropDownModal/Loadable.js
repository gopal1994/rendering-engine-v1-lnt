/**
 *
 * Asynchronously loads the component for DropDownModal
 *
 */

import Loadable from 'react-loadable';

export default Loadable({
  loader: () => import('./index'),
  loading: () => null,
});
