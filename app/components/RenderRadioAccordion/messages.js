/*
 * RenderRadioAccordion Messages
 *
 * This contains all the text for the RenderRadioAccordion component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.RenderRadioAccordion.header',
    defaultMessage: 'This is the RenderRadioAccordion component !',
  },
});
