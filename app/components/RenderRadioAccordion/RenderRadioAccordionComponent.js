/**
*
* RenderRadioAccordionComponent
*
*/

import React from 'react';
import Accordion from '../Accordion';
import RenderRadioAccordionAbstract from './RenderRadioAccordionAbstract';
// import styled from 'styled-components';


class RenderRadioAccordionComponent extends RenderRadioAccordionAbstract { // eslint-disable-line react/prefer-stateless-function
  render() {
    const { formData, compProps, elmId } = this.props;
    const { label, valueHolder, imageUrl } = compProps;
    const shouldOpen = (formData[valueHolder] === elmId);
    return (
      <Accordion
        leftIcon={imageUrl}
        leftIconType={'url'}
        label={label}
        onPress={this.onChangeHandler}
        expandIcon={'radioDeselect'}
        collapseIcon={'radioSelect'}
        shouldOpen={shouldOpen}
        type={'radio'}
      >
        {this.props.children}
      </Accordion>
    );
  }
}

RenderRadioAccordionComponent.propTypes = {

};

export default RenderRadioAccordionComponent;
