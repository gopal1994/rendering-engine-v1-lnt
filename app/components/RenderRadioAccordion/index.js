/**
*
* RenderRadioAccordion
*
*/

import React from 'react';
import RenderRadioAccordionComponent from './RenderRadioAccordionComponent';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

class RenderRadioAccordion extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <RenderRadioAccordionComponent {...this.props} />
    );
  }
}

RenderRadioAccordion.propTypes = {

};

export default RenderRadioAccordion;
