/**
*
* CheckboxField
*
*/

import React from 'react';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';
import * as styles from './CheckboxStyles';
import Checkbox from 'material-ui/Checkbox';

import RenderError from '../RenderError/';

export default class CheckboxField extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

  onChangeHandler = (e) => {
    this.props.onChangeHandler && this.props.onChangeHandler(e);
  }

  onBlurHandler = (e) => {
    this.props.onBlurHandler && this.props.onBlurHandler(e);
  }

  render() {
        // props is expected to have a componentProps object:

    const { errorMessage, componentProps, className } = this.props;
    let error = null,
      radioButtons = [];

    if (errorMessage) {
      error = (<RenderError errorMessage={errorMessage} />);
    }


    return (
      <div className={className}>
        <Checkbox
          {...styles.checkBoxStyles}
          {...componentProps}
          onCheck={this.onChangeHandler}
          onBlur={this.onBlurHandler}
        />
        {error}
      </div>
    );
  }
}

CheckboxField.propTypes = {

};

CheckboxField.defaultProps = {
  checkBoxStyles: styles.checkBoxStyles,
};

