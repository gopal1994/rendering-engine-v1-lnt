/*
 * MultiDropDownModal Messages
 *
 * This contains all the text for the MultiDropDownModal component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.MultiDropDownModal.header',
    defaultMessage: 'This is the MultiDropDownModal component !',
  },
});
