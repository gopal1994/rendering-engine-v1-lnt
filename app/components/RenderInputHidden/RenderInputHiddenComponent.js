/**
*
* RenderInputHidden
*
*/

import React from 'react';
// import styled from 'styled-components';
import RenderInputHiddenAbstract from './RenderInputHiddenAbstract';

export default class RenderInputHiddenComponent extends RenderInputHiddenAbstract { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <input type="hidden" {...this.props.compProps} />
    );
  }
}

