/**
*
* RenderCongrats
*
*/

import React from 'react';
import RenderCongratsComponent from './RenderCongratsComponent';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

class RenderCongrats extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <RenderCongratsComponent {...this.props} />
    );
  }
}

RenderCongrats.propTypes = {

};

export default RenderCongrats;
