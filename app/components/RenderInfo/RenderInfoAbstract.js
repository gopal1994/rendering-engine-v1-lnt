/**
*
* RenderInfoAbstract
*
*/

import React from 'react';
// import styled from 'styled-components';

export default class RenderInfoAbstract extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {
      showPopup: false,
    };
  }
  getPopUpdata = () => {
    const { headerText, bodyText, btnText } = this.props;
    return {
      headerText,
      bodyText,
      actionButtons: [{
        label: btnText || 'Ok, Got it!',
        action: 'callback',
        callback: () => {
          console.log('helllo');
          this.setState({ showPopup: false });
        },
        type: 'primary',
      }],
      type: 'info',
    };
  }

  showInfo = () => {
    this.setState({ showPopup: true });
  }

  render() {
    return null;
  }
}

RenderInfoAbstract.propTypes = {

};
