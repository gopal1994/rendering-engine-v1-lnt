/**
*
* RenderInfoComponent
*
*/

import React from 'react';
import { TouchableOpacity, Image, View } from 'react-native';
import RenderInfoAbstract from './RenderInfoAbstract';
import RenderPopup from '../RenderPopup';
// import styled from 'styled-components';
import {
  wrapperStyles,
  iconStyles,
} from './style';
import infoIcon from '../../images/info.png';


class RenderInfoComponent extends RenderInfoAbstract { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <View>
        <TouchableOpacity style={[wrapperStyles, this.props.infoStyles]} onPress={this.showInfo}>
          <Image
            source={infoIcon}
            style={iconStyles}
          />
        </TouchableOpacity>
        <RenderPopup
          {...this.getPopUpdata()}
          showPopup={this.state.showPopup}
        />
      </View>
    );
  }
}

RenderInfoComponent.propTypes = {

};

export default RenderInfoComponent;
