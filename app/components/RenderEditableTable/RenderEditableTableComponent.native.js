/**
 *
 * RenderEditableTableComponent
 *
 */

import React from 'react';
import { View, ScrollView, TouchableOpacity } from 'react-native';
import { Table, TableWrapper, Cell } from 'react-native-table-component';
import Text from '../RenderText';
import componentProps from '../../containers/RenderStep/componentProps';
import * as styles from './tableStyles';
import RenderButton from '../RenderButton';
import RenderError from '../RenderError';
import RenderHeading from '../RenderHeading';
import MultiInstance from '../../components/MultiInstance'

import styledComponents from "../../configs/styledComponents";
import * as componentMap from "../../utilities/componentMap";
import dottie from "dottie";

class RenderEditableTableComponent extends MultiInstance { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
  }

  renderTableHeader = () => {
    const { journeyRenderData } = this.props;
    const { fieldMap, emptyInstance } = this.state;
    const instanceKeys = Object.keys(emptyInstance);
    let fieldHeaders = [];
    if(instanceKeys) {
      fieldHeaders = instanceKeys.map((instanceKey, index) => {
        let headerColumnWrapperStyle = this.getHeaderCellStyle(journeyRenderData[instanceKey]);
        const cellData = (<Text>{fieldMap[instanceKey]}</Text>);
        return (<Cell style={headerColumnWrapperStyle} key={`cell-header${index}`} data={cellData} />); // `
      });
    }
    return [...fieldHeaders,...(this.getActionHeaders())];
  };

  getActionHeaders = () => {
    const { actions } = this.state;
    const actionHeaders = [];
    Object.keys(actions).forEach((actionKey,index) => {
      const cellStyle =  this.getHeaderCellStyle({metaData: {}});
      const cellData = (<Text style={styles.headerColumnWrapperStyle}>{actions[actionKey].headerLabel}</Text>);
      actions[actionKey].shouldRender() && actionHeaders.push(<Cell key={`cell-header-action-${index}`} data={cellData} style={[styles.text, cellStyle]} />); // `
    });
    return actionHeaders;
  };

  getActionCells = (key) => {
    const { actions } = this.state;
    let actionCells = [];
    Object.keys(actions).forEach((actionKey, index) => {
      const cellStyle =  this.getHeaderCellStyle({metaData: {}});
      actions[actionKey].shouldRender() &&
      actionCells.push(
          <Cell
              key={`cell-header-action-${index}`}
              data={<View>
                <Text onPress={
                  () => { actions[actionKey].onTrigger(key) }}>{styledComponents.getLinkStyledText(actions[actionKey].name)}</Text></View
              >}
              style={[styles.text, cellStyle]}
          /> // `
      );
    });
    return actionCells;
  };

  getFieldCellStyle = (renderData, isErrored) => {
    const { metaData: {fixedLayout, colWidth} } = renderData;
    let widthObject = !fixedLayout ?  { width: styles.colWidth * colWidth || styles.colWidth} : {};
    return {
      ...styles.headerColumnWrapperStyle,
      ...widthObject,
      ...(isErrored ? styles.cellColumnErrorStyle: {})
    };
  };

  getHeaderCellStyle = (renderData) => {
    const { metaData: {fixedLayout, colWidth} } = renderData;
    let widthObject = !fixedLayout ?  { width: styles.colWidth * colWidth || styles.colWidth} : {};
    return {
      ...styles.headerColumnWrapperStyle,
      ...widthObject,
    };
  };

  renderTableRows = () => {
    const { journeyRenderData } = this.props;
    const { instancesData } = { ...this.state };
    const instanceKeys = Object.keys(instancesData);
    if ( instanceKeys ) {
      return instanceKeys.map(
          (key, index) => (<TableWrapper key={`tableRow-${index}`} style={styles.row}>
            {this.renderUnitInstance(instancesData[key], journeyRenderData, key)}
          </TableWrapper>) //`
      );
    }
  }


  renderUnitInstance = (instance, data, instanceIndex) => {
    const instanceKeys = Object.keys(instance);
    let renderCells = [];
    instanceKeys.forEach((elm, index) => {
      const keyProps = data[elm];
      const meta = typeof keyProps.metaData === 'string' ? JSON.parse(keyProps.metaData) : keyProps.metaData;
      const instanceValue = (instance[elm] || instance[elm] === '') ? instance[elm] : meta.default;
      const formType = meta.form_type;
      const isUploadValue = (formType === 'upload' && !!instanceValue && !!instanceValue.length);
      keyProps.value = isUploadValue && instanceValue? JSON.parse(instanceValue) : instanceValue;
      const elmId = keyProps.id;
      const UnitComponent = componentMap.componentMapByFormType[formType];
      if (UnitComponent) {
        let compProps = componentProps[formType] &&
            componentProps[formType](
                keyProps,
                keyProps.id,
                keyProps.value,
            );
        if (formType === 'dropdown' || formType === 'text' ||
            formType === 'datepicker' || formType === 'number') {
          compProps.underlineStyle = styles.underlineStyle;
          compProps.label = '';
          compProps.hintText = '';
          compProps.inline = true;
        }
        const { instancesErrorData, instancesData } = this.state;
        const unitErrorObject = dottie.get(instancesErrorData, `${instanceIndex}.${elmId}`);
        const isErrored = !dottie.get(unitErrorObject,'isValid',true);

        const renderDataProp = { ...keyProps, ...unitErrorObject };
        const componentSection = (
            <View style={styles.cellWrapper}>
              <UnitComponent
                  compProps={{ ...compProps}}
                  key={elmId}
                  elmId={elmId}
                  renderData={keyProps}
                  formData={{ ...instancesData[instanceIndex] }}
                  onChangeHandler={(value, unitKey = elmId) => {
                    this.onChangeHandler(value, unitKey, instanceIndex);
                  }}
                  // onBlurHandler={value => this.onBlurHandler(value, elmId)}
              />
            </View>
        );
        const cellColumnWrapperStyle = this.getFieldCellStyle(renderDataProp, isErrored);
        renderCells.push(
            <Cell key={`cell${instanceIndex}-${index}`} data={componentSection} style={[styles.text, cellColumnWrapperStyle]} /> // `
        );
      }
    });
    return [...renderCells,...(this.getActionCells(instanceIndex))];
  };

  setLastTouchedFieldData = (event,lastTouchedInstanceIndex, lastTouchedFieldId) => {
    this.setState({
      lastTouchedInstanceIndex,
      lastTouchedFieldId,
      openPopover: true,
      anchorEl: event.currentTarget,
    });
  };

  closePopover = () => {
    this.setState({
      openPopover: false,
    });
  };

  render() {
    const tableHeader = this.renderTableHeader();
    const tableRows = this.renderTableRows();
    const { renderData } = this.props;
    const { errorMessage } = renderData;
    const Wrapper = (renderData.metaData.fixedLayout) ? View : ScrollView;
    let error = null;
    if (errorMessage) {
      error = (<RenderError errorMessage={errorMessage} />);
    }
    return (
        <View style={{ marginBottom: 15 }}>
          <RenderHeading
              compProps={{
                label: renderData.name,
                type: 'h3',
              }}
          />
          <Wrapper horizontal style={styles.container}>
            <Table borderStyle={styles.tableBorderStyle}>
              <TableWrapper key={'tableHeader'} style={styles.headerRow}>
                {tableHeader}
              </TableWrapper>
              {tableRows}
            </Table>
          </Wrapper>
          {error}
          {this.isAddAllowed()
              ? <RenderButton
                  type={'primary'}
                  text="+ Add Row"
                  onPress={this.addRow}
              />
              : null
          }
        </View>
    );
  }
}

RenderEditableTableComponent.propTypes = {

};

export default RenderEditableTableComponent;
