/**
*
* RenderEditableTableAbstract
*
*/

import React from 'react';
import * as validationFns from '../../utilities/validations/validationFns';
import * as styles from './tableStyles';

// import styled from 'styled-components';

export default class RenderEditableTableAbstract extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

    constructor(props) {
        super(props);
        this.state = {
            value: '',
            rowData: [],
            rowDataError: [],
            headerData: [],
            tableConfig: [],
        };
    }

    componentDidMount() {
        this.buildTableRenderConfig();
        this.convertValueToData();
    }

    buildTableRenderConfig = () => {
        const meta = this.props.renderData.metaData;
        const columns = meta.deleteRow ? [...meta.fields, { fieldId: 'delete', viewOnly: false }] : meta.fields;
        this.setState({ tableConfig: columns }, () => {
            this.buildTableHeaders();
        });
    }

    getErrorMessage = () => (this.props.renderData.errorMessage || '')

    canAdd = () => {
        const tableData = [...this.state.rowData];
        const meta = this.props.renderData && this.props.renderData.metaData;
        const maxRowCount = meta.maxRowCount;
        return meta.addRow && ((!maxRowCount && maxRowCount !== 0) ? true : tableData.length < maxRowCount);
    }


    // Convert table rows into value for the table field
    convertValueToData = () => {
        const self = this;
        // value is recieved in a json string
        let value = this.props.formData[this.props.elmId];
        if (!value || value === '') {
            value = "[]";
        }
        let tableData;
        if (typeof (value) === 'string') {
            tableData = JSON.parse(value);
        } else {
            tableData = value;
        }

        this.setState({ rowData: tableData }, () => {
            this.initialiseErrorData();
            //this.resetDeleteAction();
            if (!self.state.rowData.length) {
                self.addRow();
            }
        });
    }

    calculateColWidth = (defaultStyle, renderData, columnData) => {
        return (!renderData.metaData.fixedLayout) ? {
            ...defaultStyle,
            width: styles.colWidth * columnData.colWidth || styles.colWidth,
        } : {
                ...defaultStyle,
            };
    }

    initialiseErrorData = () => {
        let tableDataError = [...this.state.rowDataError];
        this.state.rowData.forEach(row => {
            tableDataError.push(this.getEmptyRowData());
        });
        this.setState({ rowDataError: tableDataError });
    }

    validateTable = (value, fieldId, index) => {
        let elmData = this.props.journeyRenderData[fieldId];
        const validObj = validationFns.validateElm(elmData, value, this.props.formData);

        let currentRenderData = { ...this.props.journeyRenderData };
        let { elmId } = this.props;


        let tableDataError = [...this.state.rowDataError];
        tableDataError[index][fieldId] = validObj.errorMessage && validObj.errorMessage + ` @row ${index + 1}`; // `
        this.setState({ rowDataError: tableDataError }, () => {
            //loop over errorData and push the errormessage to renderData[elmId];
            let currentEle = currentRenderData[elmId];
            let allErrors = [];
            this.state.rowDataError.forEach(row => {
                let errorValues = Object.values(row);
                errorValues = errorValues.filter(errorValue => {
                    return !!errorValue
                });
                if (!currentEle.errorMessage) {
                    currentEle.errorMessage = [];
                } else if (!Array.isArray(currentEle.errorMessage)) {
                    currentEle.errorMessage = [currentEle.errorMessage]
                }
                //console.log('errorValues', errorValues);
                allErrors = [...allErrors, ...errorValues];
            });
            // push unique error message
            currentEle.errorMessage = allErrors.filter((value, index, self) => {
                return self.indexOf(value) === index;
            });
            currentRenderData[elmId].isValid = !currentRenderData[elmId].errorMessage.length;
            this.props.setRenderData(currentRenderData);
            this.updateFormData();
            console.log('objec', validObj, tableDataError);
        });
    }

    convertTableRowsIntoValue = () => {
        const value = JSON.stringify(this.state.rowData);
        return value;
    }

    // Build the table data from the value passed onto
    // or From the addition/deletion of rows
    buildTableData = () => {

    }

    // Build Column headers
    buildTableHeaders = () => {
        const self = this;
        console.log('this.state.tableConfig',this.state.tableConfig);
        const tableHeaders = this.state.tableConfig.map((column) => {
            if (column.fieldId === 'delete') {
                return 'Actions';
            } else {
                return self.props.journeyRenderData[column.fieldId].name;
            }
        })
        this.setState({ headerData: tableHeaders });
    }


    // Build editable Table rows
    buildTableRows = () => {

    }

    updateFormData = () => {
        const value = this.convertTableRowsIntoValue();
        this.props.onChangeHandler(value, this.props.elmId);
    }

    getEmptyRowData = () => {
        const data = {};
        this.state.tableConfig.forEach((conf) => {
            if (conf.fieldId === 'delete') return;
            data[conf.fieldId] = '';
        });
        return data;
    }

    /* Table utilities */
    addRow = () => {
        const tableData = [...this.state.rowData];
        const tableDataError = [...this.state.rowDataError];
        tableData.push(this.getEmptyRowData());
        tableDataError.push(this.getEmptyRowData());
        this.setState({
            rowData: tableData,
            rowDataError: tableDataError,
        });
    }

    deleteRow = (deleteIndex) => {
        const tableData = [...this.state.rowData].filter((element, index) => deleteIndex !== index);
        const tableDataError = [...this.state.rowDataError].filter((element, index) => deleteIndex !== index);
        this.setState({
            rowData: tableData,
            rowDataError: tableDataError
        }, () => {
            this.updateFormData();
            //this.resetDeleteAction();
        });
    }

    changeHandler = (index, columnId) => {

    }

    rowDataChangeHandler = (value, fieldId, index) => {
        const tableData = [...this.state.rowData];
        if (fieldId === 'delete') {
            //tableData.splice(index, 1);
            this.deleteRow(index);
        } else {
            tableData[index][fieldId] = value;
            this.setState({ rowData: tableData }, () => {
                this.validateTable(value, fieldId, index);
            });
        }
    }

    render() {
        return null;
    }
}

RenderEditableTableAbstract.propTypes = {

};
