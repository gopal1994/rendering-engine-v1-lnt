
import styled from 'styled-components';

export const EditableTableOuterContainer = styled.div`
  > div:nth-child(2) {
    > div:nth-child(2) {
    overflow: ${(props) => props.fixedLayout ? 'hidden auto' : 'visible!important'}
    }
  }
`;
