/**
 *
 * RenderEditableTableComponent
 *
 */

import React from 'react';
import { Table, TableBody, TableFooter, TableHeader, TableHeaderColumn, TableRow, TableRowColumn } from 'material-ui/Table';
import componentProps from '../../containers/RenderStep/componentProps';
import Popover from 'material-ui/Popover';

import * as styles from './tableStyles';
import RenderButton from '../../components/RenderButton';
import { EditableTableOuterContainer } from './StyleEditableTable';
import RenderHeading from '../RenderHeading';
import MultiInstance from '../../components/MultiInstance'
import * as componentMap from "../../utilities/componentMap";
import dottie from "dottie";
import RenderError from "../RenderError";
import styledComponents from "../../configs/styledComponents";


class RenderEditableTableComponent extends MultiInstance { // eslint-disable-line react/prefer-stateless-function
    constructor(props) {
        super(props);
    }

    renderTableHeader = () => {
        const { journeyRenderData } = { ...this.props};
        const { fieldMap, emptyInstance } = this.state;
        const instanceKeys = Object.keys(emptyInstance);
        let fieldHeaders = [];
        if(instanceKeys) {
            fieldHeaders = instanceKeys.map((instanceKey, index) => {
                let headerColumnWrapperStyle = this.getHeaderCellStyle(journeyRenderData[instanceKey]);
                return (<TableHeaderColumn
                    style={headerColumnWrapperStyle}
                    key={`cell-header${index}`}>
                    {fieldMap[instanceKey]}
                </TableHeaderColumn>);  //`
            });
        }

        return [...fieldHeaders, ...this.getActionHeaders()];
    };

    getActionHeaders = () => {
        const { actions } = this.state;
        return Object.keys(actions).map((actionKey,index) => (
            actions[actionKey].shouldRender() &&
            (<TableHeaderColumn
                style={styles.headerColumnWrapperStyle}
                key={`cell-header-action-${index}`}>
                {actions[actionKey].headerLabel}
            </TableHeaderColumn>)
        ));
    };

    getActionCells = (key) => {
        const { actions } = this.state;
        return Object.keys(actions).map((actionKey,index) => (
            actions[actionKey].shouldRender() &&
            (<TableHeaderColumn
                style={styles.cellColumnWrapperStyle}
                key={`cell-header-action-${index}`}>
                <span onClick={() => actions[actionKey].onTrigger(key)}>{styledComponents.getLinkStyledText(actions[actionKey].name)}</span>
            </TableHeaderColumn>)
        ));
    };

    getFieldCellStyle = (renderData, isErrored) => {
        const { metaData: {fixedLayout, colWidth} } = renderData;
        let widthObject = !fixedLayout ?  { width: styles.colWidth * colWidth || styles.colWidth} : {};
        return {
            ...styles.cellColumnWrapperStyle,
            ...widthObject,
            ...(isErrored ? styles.cellColumnErrorStyle: {})
        };
    };

    getHeaderCellStyle = (renderData) => {
        const { metaData: {fixedLayout, colWidth} } = renderData;
        let widthObject = !fixedLayout ?  { width: styles.colWidth * colWidth || styles.colWidth} : {};
        return {
            ...styles.headerColumnWrapperStyle,
            ...widthObject,
        };
    };

    renderTableRows = () => {
        const { journeyRenderData } = { ...this.props };
        const { instancesData } = { ...this.state };
        if ( instancesData ) {
            return Object.keys(instancesData).map(
                (key, index) => (
                    <TableRow key={`tableRow-${index}`}>
                        { this.renderUnitInstance(instancesData[key], journeyRenderData, key) }
                    </TableRow> //`
                )
            );
        }
    }

    renderUnitInstance = (instance, data, instanceIndex) => {
        const instanceKeys = Object.keys(instance);
        let renderCells = [];
        instanceKeys.forEach((elm, index) => {
            const keyProps = data[elm];
            const meta = typeof keyProps.metaData === 'string' ? JSON.parse(keyProps.metaData) : keyProps.metaData;
            const instanceValue = (instance[elm] || instance[elm] === '') ? instance[elm] : meta.default;
            const formType = meta.form_type;
            const isUploadValue = (formType === 'upload' && !!instanceValue && !!instanceValue.length);
            keyProps.value = isUploadValue && instanceValue? JSON.parse(instanceValue) : instanceValue;
            const elmId = keyProps.id;
            const UnitComponent = componentMap.componentMapByFormType[formType];
            if (UnitComponent) {
                let compProps = componentProps[formType] &&
                    componentProps[formType](
                        keyProps,
                        keyProps.id,
                        keyProps.value,
                    );
                if (formType === 'dropdown' || formType === 'text' ||
                    formType === 'datepicker' || formType === 'number') {
                    compProps.underlineStyle = styles.underlineStyle;
                    compProps.floatingLabelText = '';
                    compProps.hintText = '';
                }
                const { instancesErrorData, instancesData } = this.state;
                const unitErrorObject = dottie.get(instancesErrorData, `${instanceIndex}.${elmId}`);
                const unitErrorMessage = dottie.get(unitErrorObject, 'errorMessage');
                const isErrored = !dottie.get(unitErrorObject,'isValid',true);

                const renderDataProp = { ...keyProps, ...unitErrorObject };
                const componentSection = (
                    <div
                        onClick={(event) => {
                            this.setLastTouchedFieldData(event,instanceIndex,elmId);
                        }}>
                        <UnitComponent
                            compProps={{ ...compProps, error: unitErrorMessage }}
                            key={elmId}
                            elmId={elmId}
                            renderData={keyProps}
                            formData={{ ...instancesData[instanceIndex] }}
                            onChangeHandler={(value, unitKey = elmId) => {
                                this.onChangeHandler(value, unitKey, instanceIndex);
                            }}
                            // onBlurHandler={value => this.onBlurHandler(value, elmId)}
                        />
                    </div>
                );
                const cellColumnWrapperStyle = this.getFieldCellStyle(renderDataProp,isErrored);
                renderCells.push(
                    <TableRowColumn style={cellColumnWrapperStyle} key={`cell${instanceIndex}-${index}`}>{componentSection}</TableRowColumn>
                );

            }
        });
        return [...renderCells,...this.getActionCells(instanceIndex)];
    };

    setLastTouchedFieldData = (event,lastTouchedInstanceIndex, lastTouchedFieldId) => {
        this.setState({
            lastTouchedInstanceIndex,
            lastTouchedFieldId,
            openPopover: true,
            anchorEl: event.currentTarget,
        });
    };

    closePopover = () => {
        this.setState({
            openPopover: false,
        });
    };

    render() {
        const { renderData, className } = this.props;
        const tableHeader = this.renderTableHeader();
        const tableRows = this.renderTableRows();
        console.log('fixedLayout', renderData.metaData.fixedLayout);
        const { instancesErrorData } = this.state;
        const errorMessage = dottie.get(instancesErrorData,`${this.state.lastTouchedInstanceIndex}.${this.state.lastTouchedFieldId}.errorMessage`);
        const error = errorMessage && (<RenderError errorMessage= {errorMessage} />);
        return (
            <EditableTableOuterContainer fixedLayout={renderData.metaData.fixedLayout} className={className}>
                <RenderHeading
                    compProps={{
                        label: renderData.name,
                        type: 'h3',
                    }}
                />
                <Table
                    fixedHeader
                    fixedFooter={false}>
                    <TableHeader
                        displaySelectAll={false}
                        adjustForCheckbox={false}
                        enableSelectAll={false}>
                        <TableRow>
                            { tableHeader }
                        </TableRow>
                    </TableHeader>
                    <TableBody
                        displayRowCheckbox={false}
                        showRowHover={false}
                        stripedRows={false}>
                        {tableRows}
                    </TableBody>
                </Table>
                {this.isAddAllowed() ? (
                    <div style={styles.buttonWrapperStyle}>
                        <RenderButton
                            label="+ Add Row"
                            type="primary"
                            onClick={this.addRow}/>
                    </div>) : null}
                <Popover
                    open={this.state.openPopover && !!errorMessage}
                    anchorEl={this.state.anchorEl}
                    canAutoPosition={true}
                    useLayerForClickAway={false}
                    onRequestClose={this.closePopover}
                    style={{paddingTop: 5, paddingLeft: 10,paddingBottom: 5, paddingRight: 10}}
                >
                    { error }
                </Popover>
            </EditableTableOuterContainer>
        );
    }
}

RenderEditableTableComponent.propTypes = {

};

export default RenderEditableTableComponent;
