import { colors, fonts, globalColors } from '../../configs/styleVars';

export const tableHeaderStyle = {
  margin: 10,
  color: colors.secondaryFontColor,
  fontSize: fonts.tableHead,
  ...fonts.getFontFamilyWeight('bold'),
};

export const container = {
  flex: 1,
  backgroundColor: colors.tableRowBG,
  marginBottom: 10,
};

export const text = {
  margin: 0,
  flex: 1,
};

export const row = {
  flexDirection: 'row',
};

export const cellWrapper = {
  padding: 10,
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center',
};

export const headerRow = {
  ...row,
  backgroundColor: colors.white,
};

export const tableBorderStyle = {
  borderColor: colors.journeyWrapperBGColor,
  borderWidth: 8,
};

export const cellColumnWrapperStyle = {
  borderWidth: 4,
  borderStyle: 'solid',
  borderColor: globalColors.lightGray,
  width: 200,
  display: 'flex',
  flexDirection: 'row',
  justifyContent: 'center',
};

export const headerColumnWrapperStyle = {
  justifyContent: 'center',
  flexDirection: 'column',
};

export const cellColumnErrorStyle = {
  backgroundColor: colors.tableCellErrorColor,
};

export const colWidth = 120;
