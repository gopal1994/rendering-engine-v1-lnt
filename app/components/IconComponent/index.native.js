import React from 'react';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { colors } from '../../configs/styleVars';

const IconComponent = (props) => {
  const { compProps, onChangeHandler, elmId } = { ...props };
  const color = !compProps.disabled ? colors.primaryBGColor : colors.primaryDisableColor;
  const iconStyles = { pointerEvents: compProps.disabled ? 'none' : 'auto', cursor: compProps.disabled ? 'not-allowed' : 'default' };
  switch (compProps.type) {
    case 'delete':
      return (
        <Icon
          name={'delete'}
          color={color}
          style={iconStyles}
          size={18}
          onPress={(e) => onChangeHandler(e, elmId)}
        />
      );
    default:
      return null;
  }
};

export default IconComponent;
