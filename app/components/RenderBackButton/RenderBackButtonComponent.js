/**
*
* RenderBackButtonComponent
*
*/

import React from 'react';
import RenderBackButtonAbstract from './RenderBackButtonAbstract';
// import styled from 'styled-components';


class RenderBackButtonComponent extends RenderBackButtonAbstract { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <div>This is RenderBackButton Component</div>
    );
  }
}

RenderBackButtonComponent.propTypes = {

};

export default RenderBackButtonComponent;
