/**
*
* RenderSubJourneyTrigger
*
*/

import React from 'react';

import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import * as renderActions from '../../containers/RenderJourney/actions';

import RenderSubJourneyTriggerComponent from './RenderSubJourneyTriggerComponent';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

class RenderSubJourneyTrigger extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <RenderSubJourneyTriggerComponent {...this.props} />
    );
  }
}

RenderSubJourneyTrigger.propTypes = {

};

const mapStateToProps = createStructuredSelector({
  // subJourneyData: makeSelectRenderSubJourney(),
});

function mapDispatchToProps(dispatch) {
  return {
    setSubJourneyId: (subJourneyId) => dispatch(renderActions.setSubJourneyId(subJourneyId)),
    dispatch,
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

// export default RenderSubJourneyTrigger;
export default compose(
  withConnect,
)(RenderSubJourneyTrigger);

