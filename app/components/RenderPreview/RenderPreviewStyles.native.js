import { Dimensions } from 'react-native';
import styleVars from '../../configs/styleVars';
const { colors, fonts } = styleVars;

export const containerStyle = {
  style: {
    marginTop: 0,
    flex: 1,
    position: 'relative',
    alignItems: 'center',
    paddingBottom: 10,
    paddingTop: 0,
  },
};

export const imageStyles = {
  style: {
    width: Dimensions.get('window').width - 60,
    height: (Dimensions.get('window').width - 60) * 0.625,
    borderRadius: 10,
    // borderWidth: 1
  },
};

export const iconStyle = {
  style: {
    position: 'absolute',
    right: 10,
    color: colors.secondaryFontColor,
    zIndex: 10,
    top: 0,
  },
  size: 20,
};

export const chipTextStyle = {
  style: {
    color: colors.secondaryFontColor,
  },
};
