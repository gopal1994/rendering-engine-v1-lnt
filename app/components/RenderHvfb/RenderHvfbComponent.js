/**
*
* RenderHvfbComponent
*
*/

import React from 'react';

import RenderHvfbAbstract from './RenderHvfbAbstract';

export default class RenderHvfbComponent extends RenderHvfbAbstract { // eslint-disable-line react/prefer-stateless-function

  render() {
    return (
      <div>This is web RenderHvfbComponent</div>
    );
  }
}
