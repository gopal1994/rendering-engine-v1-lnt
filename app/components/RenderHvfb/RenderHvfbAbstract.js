/**
*
* RenderHvfbAbstract
*
*/

import React from 'react';

export default class RenderHvfbAbstract extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {

    };
    this.onChangeHandler = this.onChangeHandler.bind(this);
    this.onBlurHandler = this.onBlurHandler.bind(this);
  }

  componentWillMount() {

  }

  onChangeHandler(value) {
    this.props.onChangeHandler(value, this.props.elmId);
  }

  onBlurHandler(e) {
    this.props.onBlurHandler(e.target.value, this.props.elmId);
  }

  render() {
    return null;
  }
}

/* Received props are
All the default styling
Behavioural attributes

onChange Fn
onBlur Fn
*/
