/**
*
* MultiDropDown
*
*/

import React from 'react';
import MultiDropDownComponent from './MultiDropDownComponent';

export default class MultiDropDown extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <MultiDropDownComponent {...this.props} />
    );
  }
}
