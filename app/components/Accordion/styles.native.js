import { colors, fonts } from '../../configs/styleVars';

export const labelStyles = {
  ...fonts.getFontFamilyWeight('bold'),
};

export const viewWithoutIconStyles = {
  marginLeft: 0,
  paddingBottom: 20,
};

export const viewWithIconStyles = {
  ...viewWithoutIconStyles,
  marginLeft: 38,
};
