import styleVars from '../../configs/styleVars';
const fonts = styleVars.fonts,
  colors = styleVars.colors;

export const datepickerStyles = {
  style: { // This is the root element style
    width: '100%',
  },
  container: {
    position: 'relative',
  },
  textFieldStyle: {
    marginBottom: 0,
    color: colors.basicFontColor,
    width: '100%',
  },
  floatingLabelFocusStyle: {

  },
  floatingLabelStyle: {
    fontSize: fonts.fontSize,
    ...fonts.getFontFamilyWeight(),
  },
};

export const removeDateStyles = {
  position: 'absolute',
  color: colors.basicFontColor,
  right: '5px',
  bottom: '12px',
  fontWeight: '600',
  cursor: 'pointer',
}
