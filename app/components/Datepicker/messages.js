/*
 * Datepicker Messages
 *
 * This contains all the text for the Datepicker component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.Datepicker.header',
    defaultMessage: 'This is the Datepicker component !',
  },
});
