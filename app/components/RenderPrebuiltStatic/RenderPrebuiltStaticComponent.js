/**
*
* RenderInputText
*
*/

import React from 'react';
// import styled from 'styled-components';

import RenderPrebuiltStaticAbstract from './RenderPrebuiltStaticAbstract';

export default class RenderPrebuiltStaticComponent extends RenderPrebuiltStaticAbstract { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <div className="field-container" style={{ textAlign: 'center' }} dangerouslySetInnerHTML={{ __html: this.state.messageHTML }}></div>
    );
  }
}

/* Received props are
All the default styling
Behavioural attributes

onChange Fn
onBlur Fn
*/
