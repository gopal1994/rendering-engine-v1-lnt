import appVars from '../../configs/styleVars';
import styled from 'styled-components';
const colors = appVars.colors,
  sizes = appVars.sizes;

const RadioLabel = styled.div`
  margin-bottom: 10px;
`;

export default RadioLabel;
