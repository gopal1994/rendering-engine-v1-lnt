/**
*
* RadioField
*
*/

import React from 'react';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';
import * as styles from './RadioStyles';
import RadioLabel from './RadioLabel';
import { RadioButton, RadioButtonGroup } from 'material-ui/RadioButton';

import RenderError from '../RenderError/';
import styledComponents from '../../configs/styledComponents';

export default class RadioField extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

  onChangeHandler = (e, value) => {
    this.props.onChangeHandler && this.props.onChangeHandler(e, value);
  }

  onBlurHandler = (e, value) => {
    this.props.onBlurHandler && this.props.onBlurHandler(e, value);
  }

  render() {
        // props is expected to have a componentProps object:

    const { errorMessage, componentProps, className, options, elmId, labelClassName, label } = this.props;
    let error = null,
      radioButtons = [];

    if (errorMessage) {
      error = (<RenderError errorMessage={errorMessage} />);
    }

    if (options && options.length) {
      options.forEach((option) => {
        radioButtons.push(<RadioButton
          {...this.props.radioButtonStyles}
          {...componentProps.radioBtnConfig}
          key={`radio-${elmId}-${option.id}`}
          value={option.id}
          label={option.name}
        />);
      });
    }
    const hintText = styledComponents.getStyledHintText(this.props.hintText);

    return (
      <div className={className}>
        { label ?
          <RadioLabel>{label}</RadioLabel> : ''
                }
        {hintText}
        <RadioButtonGroup
          {...this.props.radioGroupStyles}
          {...componentProps.radioGroupConfig}
          name={`radio-group-${elmId}`}
          onChange={this.onChangeHandler}
          onBlur={this.onBlurHandler}
        >
          {radioButtons}
        </RadioButtonGroup>
        {error}
      </div>
    );
  }
}

RadioField.propTypes = {

};

RadioField.defaultProps = {
  radioButtonStyles: styles.radioButtonStyles,
  radioGroupStyles: styles.radioGroupStyles,
};

