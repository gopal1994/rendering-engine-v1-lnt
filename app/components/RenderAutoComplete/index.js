/**
*
* RenderAutoComplete
*
*/

import React from 'react';
import RenderAutoCompleteComponent from './RenderAutoCompleteComponent';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import * as appActions from '../../containers/AppDetails/actions';
import { makeSelectUserDetails } from '../../containers/AppDetails/selectors';

class RenderAutoComplete extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <RenderAutoCompleteComponent {...this.props} />
    );
  }
}

RenderAutoComplete.propTypes = {

};

const mapStateToProps = createStructuredSelector({
  userDetails: makeSelectUserDetails(),
});

function mapDispatchToProps(dispatch) {
  return {
    getRequest: (reqObject) => dispatch(appActions.getRequest(reqObject)),
    postRequest: (reqObject) => dispatch(appActions.postRequest(reqObject)),
    dispatch,
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export default compose(
  withConnect,
)(RenderAutoComplete);
