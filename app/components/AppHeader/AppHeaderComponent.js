/**
*
* AppHeader
*
*/

import React from 'react';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

import AppHeaderAbstract from './AppHeaderAbstract';
import { AppHeaderWrapper, AppHeaderInnerWrapper, LogoutDiv } from './AppHeaderWrapper';

import StyledLogo from './StyledLogo';
import { brand } from '../../configs/styleVars';

export default class AppHeaderComponent extends AppHeaderAbstract { // eslint-disable-line react/prefer-stateless-function

  render() {
    let logoutdiv = null;
    if (this.props.isLoggedin) {
      logoutdiv = (<LogoutDiv onClick={this.logout}>Logout</LogoutDiv>);
    }
    const logoImg = (<StyledLogo src={brand.logo} />);
    return (<AppHeaderWrapper className="app-header">
      <AppHeaderInnerWrapper>
        {logoImg}
        {logoutdiv}
      </AppHeaderInnerWrapper>
    </AppHeaderWrapper>);
  }
}
