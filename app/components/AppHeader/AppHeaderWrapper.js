import appVars from '../../configs/styleVars';
import styled from 'styled-components';
const colors = appVars.colors,
  sizes = appVars.sizes;

export const AppHeaderWrapper = styled.div`
  padding: 5px 20px;
  height: 70px;
  display: flex;
  align-items: center;
  text-align: left;
  /*width: 100%;*/
  color: ${colors.basicFontColor};
  font-size: 18px;
  background-color: ${colors.appHeaderBG};
  box-shadow: 0px 0px 15px rgba(0,0,0,0.5);
`;

export const AppHeaderInnerWrapper = styled.div`
  width: ${sizes.containerWidth};
  margin: 0 auto;
  display: flex;
  justify-content: space-between;
`;

export const HeaderRightSubsection = styled.div`
  display: flex;
  max-width: 200px;
  width: 100%;
  justify-content: space-between;
  flex-direction: row-reverse;
`;

export const FlexSection = styled.div`
  display: flex;
  justify-content: space-between;
`;

export const LogoutDiv = styled.div`
    align-self: center;
`;
