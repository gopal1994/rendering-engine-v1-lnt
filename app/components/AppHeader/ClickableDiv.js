import styled from 'styled-components';

const ClickableDiv = styled.div`
  cursor: pointer;
`;

export default ClickableDiv;
