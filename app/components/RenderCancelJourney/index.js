/**
*
* RenderCancelJourney
*
*/

import React from 'react';
import RenderCancelJourneyComponent from './RenderCancelJourneyComponent';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

class RenderCancelJourney extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <RenderCancelJourneyComponent {...this.props} />
    );
  }
}

RenderCancelJourney.propTypes = {

};

export default RenderCancelJourney;
