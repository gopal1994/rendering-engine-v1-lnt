/*
 * RenderCancelJourney Messages
 *
 * This contains all the text for the RenderCancelJourney component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.RenderCancelJourney.header',
    defaultMessage: 'This is the RenderCancelJourney component !',
  },
});
