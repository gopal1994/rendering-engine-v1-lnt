/**
*
* RenderUpload
*
*/

import React from 'react';
import { View, Text, Platform } from 'react-native';
import RenderError from '../RenderError/';
import RenderChip from '../RenderChip/';
import RenderUploadAbstract from './RenderUploadAbstract';
import RenderHvUpload from './RenderHvUpload';
import RenderFileUpload from './RenderFileUpload';
import RenderPreview from '../RenderPreview/';
import * as styles from './RenderUploadStyles';

export default class RenderUploadComponent extends RenderUploadAbstract { // eslint-disable-line react/prefer-stateless-function

  // TODO: remove this
  updateFileList = (metaData) => {
    this.setState({ files: [...this.state.files, metaData] }, () => this.updateFormData());
  }

  getFileObj = (targetFile, mainFile) => {
    const uri = targetFile.uri;
    const uploadData = {
      fileObj: {
        uri,
        name: targetFile.fileName,
        size: targetFile.fileSize,
        type: targetFile.type,
      },
      metaData: {
        label: targetFile.fileName,
        uri,
        uploadClient: this.props.compProps.docSource,
      },
    };
    this.FILE_OBJ = { ...uploadData.fileObj };
    return uploadData;
  }

  render() {
    const fileChips = [];
    const isHVUpload = (this.props.compProps.docSource !== 'fileManager');
    const isSingleUpload = !this.props.compProps.multiple && this.state.files.length;

    const btnDisabled = !this.props.compProps.multiple && this.state.files.length;
    let buttonLabel = (isHVUpload ? 'Click ' : '') + this.props.renderData.name;
    if (btnDisabled) {
      buttonLabel = ` ${this.props.renderData.name}`;
    }
    const RenderComponent = isHVUpload ? RenderHvUpload : RenderFileUpload;
    // Preview Components (Chip/Preview)
    const ChipComponent = isHVUpload ? RenderPreview : RenderChip;
    this.state.files.forEach((file, index) => {
      const fileChip = (<ChipComponent
        key={`uploaded${index}`}
        uri={file.uri}
        label={file.label || this.props.renderData.name + (index + 1)}
        onRemoveHandler={() => this.removeFileData(index)}
      />);
      fileChips.push(fileChip);
    });
    // Error Component
    let errorMessage = null;
    if (this.props.renderData.errorMessage) {
      errorMessage = (<RenderError errorMessage={this.props.renderData.errorMessage} />);
    }

    return (
      <View
        {...styles.wrapper}
      >
        <View {...styles.chipWrapperStyles}>
          {fileChips}
          <RenderComponent
            {...this.props}
            isSingleUpload={isSingleUpload}
            buttonLabel={buttonLabel}
            getFileObj={this.getFileObj}
            uploadFiles={this.uploadFiles}
            updateFileList={this.updateFileList}
            removeFileData={this.removeFileData}
          />
          {errorMessage}
        </View>

      </View>
    );
  }
}
