/**
*
* RenderUpload
*
*/

import React from 'react';
import { View, Button, NativeModules, Platform } from 'react-native';
import Text from '../RenderText';
import Icon from 'react-native-vector-icons/MaterialIcons';
import RenderError from '../RenderError/';
import RenderChip from '../RenderChip/';
import RenderUploadAbstract from './RenderUploadAbstract';
import RenderPreview from '../RenderPreview/';
import RenderButton from '../RenderButton/';
import { DocumentPicker, DocumentPickerUtil } from 'react-native-document-picker';
import styleVars from '../../configs/styleVars';
import * as DocumentMap from '../../utilities/documentMap';
import * as styles from './RenderUploadStyles';
import { permissionConst } from '../../configs/appConstants';
import * as NativeMethods from './RenderUploadHelper';
import * as sessionUtils from '../../utilities/sessionUtils';
import ImagePicker from 'react-native-image-picker';


export default class RenderFileUpload extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  triggerUpload = (targetFile, uploadClient) => {
    if(targetFile) {
      const {fileObj, metaData} = this.props.getFileObj(targetFile);
      let fd = new FormData();
      fd.append('file', {...fileObj});
      fd.append('label', targetFile.fileName);
      this.props.uploadFiles(fd, metaData);
    }
  };

  selectDocument = async () => {
      // iPhone/Android
    DocumentPicker.show({
      filetype: [DocumentPickerUtil.pdf(), DocumentPickerUtil.images()],
    }, (error, res) => {
      // Android
      this.triggerUpload(res);
    });
  };

  pickDocument = () => {
    if (this.props.isSingleUpload) {
      this.props.removeFileData();
    }
    this.selectDocument();
  };

  pickFile = () => {
    const options = {
      title: 'Choose :',
      takePhotoButtonTitle: 'Take Photo',
      chooseFromLibraryButtonTitle: 'Choose from Library',
      customButtons: [
        { name: 'docUpload', title: 'Choose From File storage' },
      ],
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.showImagePicker(options, (response) => {
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
        switch (response.customButton) {
          case 'docUpload':
            // Case handled : [File upload as document]
            this.pickDocument();
            break;
          default: break;
        }
      } else {
        // Cases handeled : [New image, Library image]
        this.triggerUpload(response);
      }
    });
  };
  render() {
    return (
      <RenderButton
        type={'primary'}
        text={this.props.buttonLabel}
        onPress={this.pickFile}
        {...styles.uploadBtnStyles}
      />
    );
  }
}
