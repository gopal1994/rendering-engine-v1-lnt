import styleVars from '../../configs/styleVars';
const colors = styleVars.colors;

export const wrapper = {
  style: {
    paddingTop: 10,
    paddingBottom: 10,
  },
};

export const chipWrapperStyles = {
  style: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
};

export const uploadBtnStyles = {
  style: {
    buttonStyle: {
      borderRadius: 0,
      backgroundColor: colors.primaryBGColor,
      width: '100%',
      marginTop: 20,
    },
    textStyle: {
      color: colors.white,
      textAlign: 'left',
    },
  },
};
