/**
*
* RenderUpload
*
*/

import React from 'react';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';
import * as sessionUtils from '../../utilities/sessionUtils';

export default class RenderUploadAbstract extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.API_COUNTER = 0;
    this.FILE_OBJ = null;
    this.REQ_TYPE = null;
    this.CALLBACK_REQ = 'CALLBACK';
    this.UPLOAD_REQ = 'UPLOAD';
    this.state = {
      files: [],
      config: {},
    };
    this._buildRequestObject = this._buildRequestObject.bind(this);
    this.uploadFiles = this.uploadFiles.bind(this);
    this.updateFormData = this.updateFormData.bind(this);
    this.removeFileData = this.removeFileData.bind(this);
    this.triggerUpload = this.triggerUpload.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.compProps.value !== 'invalid') {
      let value = [];
      if ((nextProps.compProps && !!nextProps.compProps.value) && nextProps.compProps.value !== 'invalid') {
        if (Array.isArray(nextProps.compProps.value)) {
          value = nextProps.compProps.value;
        } else {
          value = JSON.parse(nextProps.compProps.value);
        }
        this.setState({ files: value });
      } else {
        this.setState({ files: [] });
      }
    }
  }

  _buildRequestParams = (apiMeta, data) => {
    Object.keys(apiMeta.data).forEach((key) => {
      data.append(key, apiMeta.data[key]);
    });
    return data;
  }

  // TODO: Change this functionality
  _buildRequestObject(apiMeta, data, metaData) {
    const self = this;
    const reqData = {
      url: apiMeta && apiMeta.url,
      params: this._buildRequestParams(apiMeta, data),
      userDetails: this.props.userDetails,
      contentType: 'multipart/form-data',
      successCb: (response) => {
        const uploadResponse = response.response || { ...metaData, data: response };
        this.setState({ files: [...this.state.files, uploadResponse] }, () => this.updateFormData());
      },
    };
    return reqData;
  }

  _buildDeleteRequestObject(data, deleteMetaData, apiMeta) {
    const self = this;
    const reqData = {
      url: apiMeta && apiMeta.url,
      params: data,
      userDetails: this.props.userDetails,
      successCb: (response) => {
        const updateFiles = [...self.state.files].filter((file, idx) => idx !== deleteMetaData.index);
        this.setState({ files: updateFiles }, () => this.updateFormData());
      },
      errorCb: (response) => {
        console.log('Something went wrong while deleting');
      },
    };
    return reqData;
  }

  uploadFiles(data, uploadMeta) {
    const meta = this.props.renderData.metaData;
    if (Array.isArray(meta.api)) {
      this.makeMultipleRequest(data, uploadMeta);
    } else {
      this.makeRequest(data, uploadMeta, meta.api);
    }
  }
  deleteFiles(deleteMeta) {
    const meta = this.props.renderData.metaData;
    const fileData = this.state.files[deleteMeta.index];
    const { id } = fileData;
    const api = { ...meta.api, url: `${meta.api.url + id}/` };
    this.makeDeleteRequest({}, deleteMeta, api);
  }

  makeDeleteRequest = (data, deleteMEta, apiMeta) => {
    const reqObj = this._buildDeleteRequestObject(data, deleteMEta, apiMeta);
    this.props.deleteRequest({ key: null, data: reqObj });
  };

  makeRequest = (data, uploadMeta, apiMeta) => {
    const method = apiMeta && apiMeta.method;
    const type = apiMeta && apiMeta.type;
    const reqObj = this._buildRequestObject(apiMeta, data, uploadMeta);

    if (method === 'POST') {
      this.props.postRequest({ key: null, data: reqObj });
    } else {
      this.props.getRequest({ key: null, data: reqObj });
    }
  }

  makeMultipleRequest = (data, uploadMeta) => {
    const apiMeta = this.props.renderData.metaData.api;
    if (Array.isArray(apiMeta) && this.API_COUNTER < apiMeta.length) {
      this.makeRequest(data, uploadMeta, apiMeta[this.API_COUNTER]);
      this.API_COUNTER++;
    } else if (this.API_COUNTER === apiMeta.length) {
      this.API_COUNTER = 0;
    }
  }

  updateFormData() {
    // Update the formData value for uploadDump
    // This is a single file upload requirement here,
    // For multuple files upload we would want to bring more comprehensive arrayS mechanism
    // NOTE: SINGLE UPLOAD ONLY - Multiple upload, later on add more function
    const filesArr = [];
    console.log(this.state);
    for (let i = 0; i < this.state.files.length; i++) {
      const file = this.state.files[i];
      filesArr.push(file);
    }
    const value = filesArr.length ? JSON.stringify(filesArr) : '';
    this.props.onChangeHandler(value, this.props.elmId);
  }

  removeFileData = (index = 0) => {
    // file delete request and other resets come here.
    this.deleteFiles({ index });
  };

  triggerUpload(files) {
    const targetFile = files[0];
    const fd = new FormData();
    fd.append('file', targetFile);
    fd.append('label', targetFile.name);
    this.uploadFiles(fd, {});
  }

  triggerSingleUpload(targetFile) {
    const fd = new FormData();
    fd.append('file', targetFile);
    fd.append('label', targetFile.name);
    this.uploadFiles(fd, {});
  }

  render() {
    return null;
  }
}
