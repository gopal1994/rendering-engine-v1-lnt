/**
*
* Modal
*
*/

import React from 'react';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';
import ModalComponent from './ModalComponent';

class Modal extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <ModalComponent {...this.props} />
    );
  }
}

Modal.propTypes = {

};

export default Modal;
