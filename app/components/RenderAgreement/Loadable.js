/**
 *
 * Asynchronously loads the component for RenderAgreement
 *
 */

import Loadable from 'react-loadable';

export default Loadable({
  loader: () => import('./index'),
  loading: () => null,
});
