import { colors, fonts, lineHeight } from '../../configs/styleVars';

export const wrapper = {
  style: {
    flexDirection: 'row',
    alignItems: 'center',
    flex: 1,
    borderColor: colors.primaryBGColor,
    borderWidth: 1,
    borderRadius: 6,
    paddingVertical: 25,
    marginTop: 10,
    marginBottom: 15,
  },
};

export const labelText = {
  style: {
    ...fonts.getFontFamilyWeight('bold'),
    fontSize: fonts.body,
    lineHeight: lineHeight.body,
    color: colors.primaryBGColor,
    flex: 1,
  },
};

export const imageWrapper = {
  style: {
    marginHorizontal: 20,
    width: 30,
    height: 30,
  },
};

export const arrowIcon = {
  style: {
    marginHorizontal: 20,
  },
};

export const docIcon = {
  style: {
    width: 30,
    height: 30,
  },
  resizeMode: 'contain',
};
