/*
 * RenderCheckbox Messages
 *
 * This contains all the text for the RenderCheckbox component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.RenderCheckbox.header',
    defaultMessage: 'This is the RenderCheckbox component !',
  },
});
