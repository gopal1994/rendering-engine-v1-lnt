import { fonts, colors } from '../../configs/styleVars';

export const checkboxStyles = {
  containerStyle: {
    borderWidth: 0,
    borderRadius: 0,
    backgroundColor: colors.transparentColor,
    borderColor: colors.transparentColor,
    padding: 0,
    marginTop: 10,
    marginLeft: 0,
  },
  checkedColor: colors.primaryBGColor,
  textStyle: {
    padding: 0,
    margin: 0,
    fontSize: fonts.inputSize,
    flex: 1,
    flexWrap: 'wrap',
    ...fonts.getFontFamilyWeight(),
  },
  size: 28,
};

export const checkboxFontFamily = fonts.getFontFamilyWeight().fontFamily;
