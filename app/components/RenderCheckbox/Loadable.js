/**
 *
 * Asynchronously loads the component for RenderCheckbox
 *
 */

import Loadable from 'react-loadable';

export default Loadable({
  loader: () => import('./index'),
  loading: () => null,
});
