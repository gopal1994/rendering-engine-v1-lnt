/**
*
* RenderCheckbox
*
*/

import React from 'react';
// import styled from 'styled-components';
import RenderCheckboxComponent from './RenderCheckboxComponent';

class RenderCheckbox extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <RenderCheckboxComponent {...this.props} />
    );
  }
}

RenderCheckbox.propTypes = {

};

export default RenderCheckbox;
