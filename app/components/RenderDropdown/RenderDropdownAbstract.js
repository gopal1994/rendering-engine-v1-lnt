/**
*
* RenderDropdown
*
*/

import React from 'react';
// import styled from 'styled-components';

// import { FormattedMessage } from 'react-intl';
// import messages from './messages';

export default class RenderDropdown extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {
      showList: false,
    };
    this.onChangeHandler = this.onChangeHandler.bind(this);
    this.onBlurHandler = this.onBlurHandler.bind(this);
  }

  componentWillMount() {

  }

  onChangeHandler(e, index, value) {
    this.props.onChangeHandler(value, this.props.elmId);
  }

  onBlurHandler(e, index, value) {
    this.props.onBlurHandler(value, this.props.elmId);
  }

  render() {
    return null;
  }

}

RenderDropdown.propTypes = {

};
