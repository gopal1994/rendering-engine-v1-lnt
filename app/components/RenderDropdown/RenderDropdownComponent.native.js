/**
*
* RenderInputText
*
*/

import React from 'react';
import RenderDropdownAbstract from './RenderDropdownAbstract';
import DropDown from '../DropDown/';
export default class RenderDropdownComponent extends RenderDropdownAbstract { // eslint-disable-line react/prefer-stateless-function

  onChangeHandler = (option) => {
    this.props.onChangeHandler(option.value, this.props.elmId);
  }

  render() {
    const enums = this.props.renderData.enumValues;
    const { compProps } = this.props;
    const options = enums.map((option) => ({ name: option.name, value: option.id }));
    return (
      <DropDown
        {...compProps}
        value={compProps.value}
        options={options}
        onChangeHandler={this.onChangeHandler}
      />
    );
  }
}

/* Received props are
All the default styling
Behavioural attributes

onChange Fn
onBlur Fn
*/
