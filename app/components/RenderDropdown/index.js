/**
*
* RenderDropdown
*
*/

import React from 'react';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

import RenderDropdownComponent from './RenderDropdownComponent';

class RenderDropdown extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <RenderDropdownComponent {...this.props} />
    );
  }
}

RenderDropdown.propTypes = {

};

export default RenderDropdown;
