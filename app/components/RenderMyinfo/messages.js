/*
 * RenderMyinfo Messages
 *
 * This contains all the text for the RenderMyinfo component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.RenderMyinfo.header',
    defaultMessage: 'This is the RenderMyinfo component !',
  },
});
