/**
*
* RenderInputText
*
*/

import React from 'react';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

import TextField from 'material-ui/TextField';
import { clientKeys } from '.../../configs/appConstants';
import qs from 'query-string';

export default class RenderMyinfoAbstract extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentWillReceiveProps() {
    // If there is param --> callback in the url search string, we have recieved the data back from the auth signing
    // So look for the "code" param to set it in the formField
    const searchStr = window.location.search;
    if (searchStr.indexOf('code') !== -1) {
      const searchObj = qs.parse(searchStr);
      if (searchObj.code) {
        // call the onChangeHandler with the code value
        this.props.onChangeHandler(searchObj.code, this.props.elmId);
      }
    }
    // clear the search string now
    window.history.replaceState(null, null, window.location.pathname);
  }

  initiateAuthReq = () => {
    const myInfoConfig = clientKeys.myinfo;
    const { authApiUrl, clientId, attributes, purpose, state } = myInfoConfig;
    const authoriseUrl = `${authApiUrl}?client_id=${clientId
             }&attributes=${attributes
             }&purpose=${purpose
             }&state=${state
             }&redirect_uri=` + 'http://localhost:3001/callback';

    window.location = authoriseUrl;
  }


  onBlurHandler = (e) => {
    this.props.onBlurHandler(e.target.value, this.props.elmId);
  }

  render() {
    return null;
  }
}

/* Received props are
All the default styling
Behavioural attributes

onChange Fn
onBlur Fn
*/
