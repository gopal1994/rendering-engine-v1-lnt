import AuthNavigation from '../AuthNavigation.native';
import RenderJourneyNavigation from '../RenderJourneyNavigation.native';

const MainRoutes = {
  AuthNavigation,
  RenderJourneyNavigation,
};

export default MainRoutes;
