import React from 'react';
import { BackHandler } from 'react-native';
import SplashScreen from 'react-native-splash-screen';
import { createStackNavigator } from 'react-navigation';
// import * as userUtils from '../utilities/userUtils';
import { checkSupport } from '../utilities/native/touchIDService.native';
import AuthRoutes from './routes/AuthNavigationRoutes.native';

const Navigator = createStackNavigator(AuthRoutes, {
  initialRouteName: 'Login',
  navigationOptions: {
    header: null,
  },
});

export default class AuthNavigator extends React.Component {

  static router = Navigator.router;

  constructor(props) {
    super(props);
    this.state = {
      showNav: false,
    };
  }

  componentWillMount() {
    // use this if need to change initial route on Mount
    // userUtils.getSessionUserDetails().then((userObj) => {
    //   this.checkLoggedIn(userObj);
    // });
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
  }

  onBackPress = () => true;

  toggleNav = (value) => {
    this.setState({ showNav: value });
  }

  checkLoggedIn = async (userObj) => {
    let routeName = '';
    if (userObj.mobileNumber && userObj.otp) {
      routeName = 'RenderConfirmMpin';
    }
    if (userObj.mobileNumber && userObj.otp && userObj.useTouchID === 'true') {
      const supportStatus = await checkSupport();
      if (supportStatus) {
        routeName = 'RenderTouchId';
      }
    }
    this.setState({ showNav: true }, () => {
      SplashScreen.hide();
      if (routeName) {
        this.props.navigation.navigate(routeName);
      }
    });
  }


  render() {
    return (
      <Navigator
        screenProps={this.props.screenProps}
        navigation={this.props.navigation}
      />
    );
  }
}
