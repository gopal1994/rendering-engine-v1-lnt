import React from 'react';
import { createSwitchNavigator } from 'react-navigation';
import { setTopLevelNavigator } from '../utilities/NavigationService.native';
import MainRoutes from './routes/MainNavigationRoutes.native';

const Navigator = createSwitchNavigator(MainRoutes, {
  initialRouteName: 'AuthNavigation',
});

export default class MainNavigator extends React.Component {

  static router = Navigator.router;

  render() {
    const { navigation, userDetails, ...otherProps } = this.props;
    return (
      <Navigator
        screenProps={{
          ...userDetails,
          ...otherProps,
        }}
        navigation={navigation}
        ref={(navigatorRef) => {
          setTopLevelNavigator(navigatorRef);
        }}
      />
    );
  }
}
