import { getFontFamilyWeight } from './styleConfig';
import { scale, verticalScale, normalize } from './scalingUtils';
import logo from '../images/logo.png';
import { userConfig } from './appConfig';

// Color Variable:
export const globalColors = {
  green: '#009356',
  red: '#D2000D',
  dark: '#000000',
  orange: '#F59B4A',
  darkGray: '#5E5E5E',
  gray: '#ABABAB',
  lightGray: '#DDDDDD',
  white: '#FFFFFF',
};
const primaryRed = '#E31E24';

export const colors = {
  white: '#FFF',
  black: '#000',

  basicFontColor: '#363C44',
  basicLightFontColor: '#D7D7D7',

  primaryBGColor: !userConfig.runAsPlugin ? '#FCA830' : primaryRed,
  headerColor: !userConfig.runAsPlugin ? '#FCA830' : primaryRed,

  fillColor: '#f39a52',
  // primaryBGColor: '#183883',
  primaryFontColor: '#FFF',
  primaryDisableColor: '#DDDDDD',

  secondaryBGColor: '#F5F5F5',
  secondaryFontColor: '#4A4A4A',
  lighterColor: globalColors.lightGray,
  borderLight: '#F5F5F5',

  grey96: '#969696',
  errorColor: globalColors.red,
  requiredColor: globalColors.red,
  // appHeaderBG: '#183883',
  appHeaderBG: '#FFF',
  stepWrapperBGColor: '#FFF',
  journeyWrapperBGColor: '#F1F1F1',
  appHeaderFontColor: '#5B6168',

  transparentColor: 'transparent',
  linkColor: '#3b99fc',
  disabledLinkColor: '#83c0e0',
  hintTextColor: globalColors.gray,
  descriptionColor: '#6f6f6f',
  fbColor: '#3b5998',

  // TODO: remove redundant colors
  amountHeading: '#F59B4A',
  tabHeaderColor: '#b6b6b6',
  progressUnfilled: 'rgba(0, 0, 0, 0.12)',
  overlayColor: 'rgba(0,0,0,0.6)',
  labelColor: globalColors.darkGray,
  inputColor: globalColors.dark,
  underlineColor: globalColors.lightGray,
  reminderColor: 'rgba(245, 245, 245, 0.57)',
  inProgressColor: '#75bee9',
  closedColor: '#ababab',
  tableRowBG: '#FAFAFA',
  tableActionsLinkColor: primaryRed,
  tableCellErrorColor: 'rgba(255,0,77,0.08)',
};

// TODO: move fonts to separate files for web and native
export const fonts = {
  primaryFontFamily: 'Lato-Regular, Arial',
  fontSize: normalize(16),

  // Buttons
  buttonFontSize: normalize(18),
  // buttonLineHeight: '36px',
  buttonLineHeight: normalize(2),
  labelSize: normalize(12),
  inputSize: normalize(16),
  subHeadingSize: normalize(18),
  mainHeadingSize: normalize(24),
  paraSize: normalize(14),
  headerSize: normalize(24),
  nativeButtonFontSize: normalize(14),

  mainHeader: normalize(40),
  h1: normalize(32),
  h2: normalize(24),
  h3: normalize(20),
  h4: normalize(18),
  body: normalize(16),
  description: normalize(14),
  label: normalize(12),
  tableHead: normalize(14),

  subJourneyHeading: normalize(20),

  getFontFamilyWeight,
  hintOpacity: 0.7,
  hintFontSize: normalize(12),
  hintFontStyle: 'italic',
};

// TODO: combine font and lineHeight
export const lineHeight = {
  h1: normalize(40),
  h2: normalize(32),
  h3: normalize(30),
  h4: normalize(26),
  body: normalize(24),
  description: normalize(22),
  label: normalize(18),
};

export const sizes = {
  containerWidth: '100%',
  appPaddingLeft: 20,
  appPaddingRight: 20,
  appPaddingHorizontal: 20,
  headerPadding: 50,
};

export const brand = {
  logo,
};

const vars = {
  colors,
  fonts,
  sizes,
};

export default vars;
