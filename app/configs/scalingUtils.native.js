import { Dimensions, Platform, PixelRatio } from 'react-native';
const { width, height } = Dimensions.get('window');

// Guideline sizes are based on standard ~5" screen mobile device
const guidelineBaseWidth = 350;
const guidelineBaseHeight = 680;

const scale = (size) => Platform.OS === 'ios' ? width / guidelineBaseWidth * size : size;
const verticalScale = (size) => Platform.OS === 'ios' ? height / guidelineBaseHeight * size : size;

const normalize = (size, factor = -0.3) => {
  if (Platform.OS === 'ios') {
    return size + (scale(size) - size) * factor;
  }
  return size;
};

export { scale, verticalScale, normalize };
