import styleVars from 'configs/styleVars';
const fonts = styleVars.fonts,
  colors = styleVars.colors;

export const radioComponentStyle = {
  radioGroupStyles: {
    style: { // This is the root element style
      width: '100%',
    },
  },
  radioButtonStyles: {
    labelStyle: {
      fontSize: fonts.fontSize,
      color: colors.basicFontColor,
    },
  },
};
