
/* This contains the basic styling configuration for the entire journey */


/* Style configs for Components */

// TODO: Move the render settings inside /componentStyles folder
// TODO: Move the styles away from this file to the respective styling files

// For input text/number
// basic behaviour rendering:
const globalVars = {

};
export const inputRenderSetting = {
  placeHolder: true,
  floatingText: true,
};

export const dropdownRenderSetting = {
  hintText: true,
  floatingText: true,
  // floatingLabelFixed: true,
};

const defaultFamily = 'Lato-Regular, Arial';
const fontMapping = {
  family: {
    bold: 'Lato-Semibold, Arial',
    normal: defaultFamily,
  },
  weights: {
    bold: '600',
    normal: '200',
  },

};

export const getFontFamilyWeight = (weight) => {
  const { weights, family } = fontMapping;

  return {
    fontFamily: family[weight] ? family[weight] : defaultFamily,
    fontWeight: weights[weight] ? weights[weight] : weights.normal,
  };
};

