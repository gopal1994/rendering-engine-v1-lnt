/*
*
* styledComponents
* Serving as a styling pipe.
*
*/

import React from 'react';
import { colors, fonts } from '../configs/styleVars';

const getStyledRequiredLabel = (text) => {
  const errorStyle = {
    color: colors.requiredColor,
  };
  return <span>{text}<span style={errorStyle}> *</span></span>;
};

const getLinkStyledText = (text) => {
  const linkStyle = {
    color: colors.tableActionsLinkColor,
  };
  return <span style={linkStyle}> {text}</span>;
};

const getStyledHintText = (text) => {
  const style = {
    fontStyle: fonts.hintFontStyle,
    fontSize: fonts.hintFontSize,
    opacity: fonts.hintOpacity,
  };
  return <span style={style}> {(text || '').replace(/\b\w/g, (l) => l.toUpperCase())}</span>;
};

const styledComponents = {
  getStyledRequiredLabel,
  getStyledHintText,
  getLinkStyledText,
};

export default styledComponents;
