import * as config from './configHelper';

/*
  This file contains the confuguration for the app
  This needs to be overwritten for every implementation going forward with relative fields
  APP config
  Theme selection
  Next/Back trigger
  Menu bar
  validationtriggers
  single page app or not

  ENV settings

  API urls:
    // DMS
    Form submission
    Initiate
*/

import { clientHeaders, getClientKey } from './appConstants';

/*
====  API CONFIG ===
  This config contains the API endpoints for all the different journeys
*/
//const BASE_API_URL = config.BASE_API_URL;

console.log('BASE_API_URL----->', config.BASE_API_URL);
const loginReq = true;

const dataEntry = 'lending/journey/';

// const dashboard = 'mobile-dashboard/';
const dashboard = 'customer_dashboard/';
const serviceRequest = 'request-service/';
const apiEndPoints = {
  login: 'login',
  refreshToken: 'login/refresh-token',
  generateOtp: 'otp/generate-otp',
  validateOtp: 'otp/validate-otp',
  saveMpin: loginReq ? `${dataEntry}save-mpin` : 'save-mpin',
  validateMpin: loginReq ? `${dataEntry}validate-mpin` : 'validate-mpin',
  updateTouchId: loginReq ? `${dataEntry}set-touch-id` : 'set-touch-id',
  initiate: loginReq ? `${dataEntry}initiate` : 'initiate',
  submitForm: loginReq ? `${dataEntry}submit-form` : 'submit-form',
  backScreenTrigger: loginReq ? `${dataEntry}back` : 'back',
  cancelApplication: loginReq ? `${dataEntry}cancel` : 'cancel',
  userData: `${dashboard}get-user-data`,
  loanData: `${dashboard}get-user-loan-data`,
  getDashboardData: `${dashboard}get-dashboard-data`,
  getApplicationData: `${dashboard}get-applications`,
  getProfile: `${dashboard}get-profile`,
  getAddressProfile: `${dashboard}update-info/contact`,
  getJobProfile: `${dashboard}update-info/employment`,
  updateJobProfile: `${dashboard}update-info/employment`,
  updateAddressProfile: `${dashboard}update-info/contact`,
  updatePassword: `${dashboard}update-info/password`,
  updateNotificationPreference: `${dashboard}update-info/notification-pref`,
  updatePreferedLang: `${dashboard}update-info/prefered-lang`,
  createSupportRequest: `${serviceRequest}initiate`,
  getSupportRequests: `${serviceRequest}fetch-service-requests`,
  getSettings: `${dashboard}get-settings`,
  getNotifications: `${dashboard}get-notifications`,
  getRepaymentLink: `${dashboard}get-repayment-schedule`,
  getConfig: 'api/config/get',
};

const GOOGLE_MAPS_BASE_API_URL = 'https://maps.googleapis.com/maps/api/';

const thirdPartyApiEndPoints = {
  geocode: {
    url: 'geocode/json',
    base: GOOGLE_MAPS_BASE_API_URL,
  },
};

export const getAPIEndPoint = (apiKey) => config.BASE_API_URL + apiEndPoints[apiKey];

export const getForeignHeaders = (clientType) => clientHeaders && clientHeaders[clientType];

export const getThirdPartyAPIEndPoint = (apiKey) => thirdPartyApiEndPoints[apiKey].base + thirdPartyApiEndPoints[apiKey].url;


/*
=== LAYOUT CONFIG ====
This would contain theme related + layout related configs
*/
export const layoutConfig = {
  includeHeader: true,
  defaultColumnWidth: 4,
  // Each row is divided into 12 columns
  // In native the size is given in percentage
  // So each columns will have width of:
  defaultColumnSizeForMobile: (100 / 12),
  isStepperShown: !config.IS_SIDEBAR_SHOWN,
  isSideNavShown: config.IS_SIDEBAR_SHOWN,
  defaultRadioCardsWidth: 3,
  defaultRadioCardsWrapperWidth: 12,

};


/*
=== STYLE CONFIG ===
Colors/Variables/Dimensions
*/


/*
=== BEHAVIOURAL CONFIG ====
***Subjected to behaviours
*/
export const behaveConfig = {
  validationTrigger: 'change', // 'change', 'blur', 'submit'
};


/*
=== APP CONSTANTS ===
*/
export const appConst = {
  roi: 18,
  inactivityTime: 5, // in minutes,
  maxAge: 60,
  minAge: 20,
  // locale currency string
  localCurrencyString: 'en-IN',
  currencyPrecision: 0,
  localeCurrencyType: 'INR',
  //journeyName: 'accentureMVPDataEntry',
  // journeyName: 'testv1',
  journeyName: 'testJourneyM',
};

export const userConfig = {
  phoneLogin: true,
  emailLogin: false,
  oTPSupport: true,
  configScreen: true,
  runAsPlugin: false,
  runAsPreviewForm: false,
};
