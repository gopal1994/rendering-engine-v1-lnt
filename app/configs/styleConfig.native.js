import { Platform } from 'react-native';

export const inputRenderSetting = {
  capitalizeInput: false,
};

const defaultFamily = 'Lato-Regular';
const fontMapping = {
  family: {
    bold: 'Lato-Semibold',
    normal: defaultFamily,
  },
  weights: {
    bold: '600',
    normal: '200',
  },
};

export const getFontFamilyWeight = (weight) => {
  const { weights, family } = fontMapping;

  if (Platform.OS === 'android') {
    selectedFamily = weight && family[weight] ? family[weight] : defaultFamily;

    return {
      fontFamily: selectedFamily,
    };
  }
  return {
    fontFamily: defaultFamily,
    fontWeight: weights[weight] ? weights[weight] : weights.normal,
  };
};
