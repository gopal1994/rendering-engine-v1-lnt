import { colors, fonts } from './styleVars';

export default {
  fontFamily: fonts.primaryFontFamily,
  palette: {
    textColor: colors.black,
    // primaryTextColor: '#10A1A1',
    primary1Color: colors.primaryBGColor,
      // primary2Color: cyan700,
      // primary3Color: grey400,
    // accent1Color: '#EE5F5F',
    // accent2Color: '#3BB2EE',
    // accent3Color: '#7ED321',
    // accent4Color: '#F5A122',
    // mutedAccent1Color: '#FFC5C5',
    // mutedAccent2Color: '#A2D9F5',
    // mutedAccent3Color: '#C8F0A1',
    // mutedAccent4Color: '#FFDCA6',
      // textColor: darkBlack,
      // alternateTextColor: white,
      // canvasColor: white,
      // borderColor: grey300,
      // disabledColor: fade(darkBlack, 0.3),
      // pickerHeaderColor: cyan500,
      // clockCircleColor: fade(darkBlack, 0.07),
      // shadowColor: fullBlack,
  },
//   appBar: {
//     height: 50,
//     color: '#fff',
//     textColor: '#10a1a1',
//   },
//   textField: {
//     hintColor: '#77c0b6',
//   },
  slider: {
    trackSize: 4,
    trackColor: colors.basicLightFontColor,
    trackColorSelected: colors.basicLightFontColor,
    handleFillColor: colors.basicLightFontColor,
    handleSize: 15,
    handleSizeActive: 18,
    handleSizeDisabled: 15,
    selectionColor: colors.primaryBGColor,
  },
  stepper: {
    iconColor: colors.primaryBGColor,
  },
  datePicker: {
    // color: '#FFF',
     // textColor: '#FFF',
     // calendarTextColor: '#FFF',
    selectColor: colors.primaryBGColor,
     // selectTextColor: colors.primaryBGColor,
     // calendarYearBackgroundColor: colors.primaryBGColor,
    headerColor: colors.primaryBGColor,
  },
  select: {
    padding: undefined,
    // paddingRight: theme.spacing.unit * 4,
    height: undefined,
  },
  selectMenu: {
    lineHeight: undefined,
  },
};

