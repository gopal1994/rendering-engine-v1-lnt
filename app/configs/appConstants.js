export const appBreakPoints = {
  SMALL_Width: 414,
  MEDIUM_Width: 600,
  LARGE_Width: 1024,
};

export const permissionConst = {
  GRANTED: true,
  DENIED: false,
};

let clientKeys = {
  google: 'AIzaSyDEm3_7F_Sj6tSP3ZAh13_YPhOTJan0Kfk',
  hypervergeFb: {
    appid: '',
    appkey: '',
  },
  hypervergeSnap: {
    appid: '362214',
    appkey: '8100b58a31412abfec88',
  },
};

export const setClientKey = (valueObj) => {
  clientKeys = {
    ...clientKeys,
    ...valueObj,
  };
};

export const getClientKey = (key) => clientKeys[key];

export const clientHeaders = {};

export const tabNavList = ['Dashboard', 'Loans', 'Notification', 'Support', 'Profile'];


export const defaultScreen = 'Dashboard';

export const journeyNames = {
  newLeadJourney: 'svakarmaMSME',
};


export const drawerNavListWithCategory = {
  routes: ['Dashboard', 'Loans'],
  categories: [
    {
      name: 'customerSupport',
      routes: ['Support', 'Faqs'],
    },
    {
      name: 'myAccount',
      routes: ['Profile', 'Settings'],
    },
  ],
};
export const languages = [
  {
    value: 'en-US',
    label: 'EN',
    name: 'English',
    icon: 'en',
  },
  {
    value: 'vi-VN',
    label: 'VN',
    name: 'Tiếng Việt',
    icon: 'vi',
  },
];

export const defaultLocale = 'en-US';

export const customerSupportNumber = '02839333888';
