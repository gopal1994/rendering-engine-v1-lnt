

const guidelineBaseWidth = 350;
const guidelineBaseHeight = 680;

const scale = (size) => size;
const verticalScale = (size) => size;
const normalize = (size, factor = 0.5) => size;


export { scale, verticalScale, normalize };
