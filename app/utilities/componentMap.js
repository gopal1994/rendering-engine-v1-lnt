// This mapping of components based on their form type
// This does not include the prebuilt/static components at the moment
// Purely based on form type - unit components

import RenderInputText from '../components/RenderInputText/';
import RenderInputHidden from '../components/RenderInputHidden/';
import RenderRestInput from '../components/RenderRestInput/';
import RenderCheckbox from '../components/RenderCheckbox/';
import RenderDatepicker from '../components/RenderDatepicker/';
import RenderRadio from '../components/RenderRadio/';
import RenderDropdown from '../components/RenderDropdown/';
import RenderSlider from '../components/RenderSlider/';
import RenderUpload from '../components/RenderUpload/';
import RenderRestDropdown from '../components/RenderRestDropdown/';
import RenderMultiDropDown from '../components/RenderMultiDropdown/';
import RenderPrebuiltStatic from '../components/RenderPrebuiltStatic/';
import RenderAutoComplete from '../components/RenderAutoComplete/';
// import RenderSocialLogin from '../components/RenderSocialLogin/';
import RenderEmiCalculator from '../components/RenderEmiCalculator/';
import RenderPopup from '../components/RenderPopupForm/';
import RenderDownload from '../components/RenderDownload/';
import RenderElasticSearch from '../components/RenderElasticSearch/';
import RenderHvfb from '../components/RenderHvfb';
import RenderAgreement from '../components/RenderAgreement';
import RenderHeading from '../components/RenderHeading';
import RenderJourneySuccess from '../components/RenderJourneySuccess';
import RenderAccordion from '../components/RenderAccordion/';
import RenderRadioAccordion from '../components/RenderRadioAccordion';
import RenderLoanApproval from '../components/RenderLoanApproval';
import RenderCongrats from '../components/RenderCongrats';
import RenderSubJourneyTrigger from '../components/RenderSubJourneyTrigger';
import RenderEditableTable from '../components/RenderEditableTable';
import EditProfile from '../components/EditProfile';
import RenderGeotracking from '../components/RenderGeotracking';
import IconComponent from '../components/IconComponent';
import RenderExpression from '../components/RenderExpression';
import RenderCloning from '../components/RenderCloning';


export const componentMapByFormType = {
  text: RenderInputText,
  number: RenderInputText,
  rest: RenderRestInput,
  hidden: RenderInputHidden,
  checkbox: RenderCheckbox,
  datepicker: RenderDatepicker,
  radio: RenderRadio,
  dropdown: RenderDropdown,
  'multiselect-dropdown': RenderMultiDropDown,
  slider: RenderSlider,
  upload: RenderUpload,
  'rest-dropdown': RenderRestDropdown,
  autocomplete: RenderAutoComplete,
  prebuilt: RenderPrebuiltStatic,
  // 'social-facebook': RenderSocialLogin,
  'social-facebook': RenderHvfb,
  popup: RenderPopup,
  download: RenderDownload,
  elasticAutocomplete: RenderElasticSearch,
  docuSign: RenderAgreement,
  heading: RenderHeading,
  accordion: RenderAccordion,
  radioAccordion: RenderRadioAccordion,
  subJourney: RenderSubJourneyTrigger,
  editableTable: RenderEditableTable,
  cloning: RenderCloning,
  locator: RenderGeotracking,
  icon: IconComponent,
  expression: RenderExpression,
};

export const singleComponentMapByScreen = {
  emiCalculator: RenderEmiCalculator,
  loanRequirements: RenderEmiCalculator,
  loanRequirementsCopy: RenderEmiCalculator,
  stepSuccess: RenderJourneySuccess,
  statusScreen: RenderJourneySuccess,
  rejectMessageRetry: RenderJourneySuccess,
  rejectionMessage: RenderJourneySuccess,
  loanoffer: RenderLoanApproval,
  congratsScreen: RenderCongrats,
  editProfile: EditProfile,
};
