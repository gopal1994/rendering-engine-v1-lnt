/*
This file contains the validation call for each element
called using one of the keys of available validators in Validators.js
*/

import validators from './validators';
import * as commonUtils from '../commonUtils';


const singleKeyValidation = (id, value, meta, formData) => validators[id] && validators[id](value, meta, formData);

const validateCustomObj = (validationObj, value, meta, formData) => {
  if (!validationObj.type || !validationObj.expression) {
    return {isValid: true, errorMessage: ''};
  }
  const errorMessage = validationObj.errorMessage;
  if (validationObj.type === 'regex') {
    // perform regex test with given regex expression
    return validators.checkRegexValidation(validationObj.expression, value, errorMessage);
  }
  if (validationObj.type === 'expression') {
    // perform evalution of expression with formData to evaluate the validation
    // this expression is always supposed to output boolean
    const isValid = commonUtils.parseEval(validationObj.expression, formData);
    return validators.validationResultObject(isValid, errorMessage);
  }
}

export const getValidationObj = (validators, value, meta, formData) => {
  // Validators could be 
  // - a string (a single id for special validations in the system)
  // - an object (a single cutom validation object)
  // - an array of (one of or both the following)
  //      -- an array of strings (array of special validation ids)
  //      -- an array of validation objects ({type: 'regex'/'expression', expression: '', errorMessage: ''});
  // Call the validator with unique identification for a particular Validation
  // This will return the "first" invalid condition for provided identifiers

  let returnObj = {};
  if (typeof validators === 'string') {
    returnObj = singleKeyValidation(validators, value, meta, formData);
  } else if (typeof validators === 'object' && !Array.isArray(validators)) {
    returnObj = validateCustomObj(validators, value, meta, formData);
  }
  else if (Array.isArray(validators)) {
    // validator  array could be an array of validator ids(special designed validations
    // Or it could be array of objects (validation objects) or a combination of both
    const isInvalid = validators.some((item) => {
      if (typeof item === 'string') {
        returnObj = singleKeyValidation(item, value, meta, formData);
      } else if (typeof item === 'object') {
        returnObj = validateCustomObj(item, value, meta, formData);
      }
      if (returnObj) {
        return !returnObj.isValid;
      }
      window.logger.error('META', `${id} is not a valid validator for ${meta.form_type}`, { formData, meta });
      return false;
    });
  }

  return returnObj;
};


// Validate step element
export const validateElm = (elmData, value, formData) => {
  const meta = elmData && elmData.metaData;
  const isFieldRequired = meta.required || meta.required === 'true';
  const isMinMax = (meta.min || meta.max) && meta.form_type === 'number';
  const validatorKeys = meta && meta.validator && JSON.parse(meta.validator);

  if (meta.customValidators && Array.isArray(meta.customValidators)) {
    Array.prototype.push.apply(validatorKeys, meta.customValidators);
  }
  if (Array.isArray(validatorKeys) && isFieldRequired) {
    validatorKeys.unshift('isRequired');
  }

  let validObj = {isValid: true, errorMessage: ''};
  if ((meta.form_type === 'editableTable' || meta.form_type === 'cloning') && (Object.keys(elmData).indexOf('isValid') > -1 && !elmData.isValid)){
    return {
      isValid: elmData.isValid,
      errorMessage: elmData.errorMessage,
    };
  }
  if (validatorKeys && validatorKeys.length) {
    if (isFieldRequired) {
      validObj = getValidationObj(validatorKeys, value, meta, formData) || validObj;
    } else if (!isFieldRequired && !!value) {
      validObj = getValidationObj(validatorKeys, value, meta, formData) || validObj;
    }
    // TODO: remove this after updating getValidationObj function
    if (isMinMax && validObj.isValid) {
      validObj = validators.isMinMax(value, meta);
    }
  }

  return validObj;
};
