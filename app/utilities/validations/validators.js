/*
This is the validation library
  Contains the conditions against a unique key for a validation condition
*/
import { strings } from '../../../locales/i18n';
import validationConstants from './validationConstants';
import * as common from '../commonUtils';

const validationResultObject = (resultBool, errorMessage) => ({
  isValid: resultBool,
  errorMessage: !resultBool ? errorMessage : '',
});


const checkRegexValidation = (expr, value, errorMessage) => {
  const regex = new RegExp(expr);
  const error = errorMessage;
  return validationResultObject(regex.test(value), error);
}

// A regexValidation function shorcut
// Use it wherever regex validation condition is supposed to be met
const regexValidation = (key, value) => {
  let regex = new RegExp(validationConstants.regexList[key]),
    error = validationConstants.getErrorMessage(key);
  return validationResultObject(regex.test(value), error);
};

// A required validation check
// Use it wherever required condition is supposed to be met
const requiredValidation = (value, meta) => {
  const error = validationConstants.getErrorMessage('isRequired');
  const formType = meta && meta.form_type;
  if(formType === 'editableTable') {
    let parsedValue = value && JSON.parse(value);
    for(let i=0;i < parsedValue.length; i++){
      let rowValues = Object.values(parsedValue[i]);
      let isValid = rowValues.some((value) => {
        return !!value || value === 0;
      });
      if(isValid) {
        return validationResultObject(isValid, error);
      }
    }
    return validationResultObject(false, error);
  } else {
    return validationResultObject(!!value || value === 0, error);
  }
};

const minMaxValidation = (value, meta) => {
  let errorMessage,
    inValidArr = [];
  if (meta.min && value < meta.min) {
    inValidArr.push('min');
  }
  if (meta.max && value > meta.max) {
    inValidArr.push('max');
  }

  const minErrorLabel = meta.isCurrency ? common.convertCurrencyFormat(meta.min) : meta.min;
  const maxErrorLabel = meta.isCurrency ? common.convertCurrencyFormat(meta.max) : meta.max;
  if (inValidArr.length === 2) {
    errorMessage = strings('errors.betweenValue', { min: minErrorLabel, max: maxErrorLabel });
  } else if (inValidArr.indexOf('min') !== -1) {
    errorMessage = strings('errors.minValue', { min: minErrorLabel });
  } else if (inValidArr.indexOf('max') !== -1) {
    errorMessage = strings('errors.maxValue', { max: maxErrorLabel });
  }
  return validationResultObject(!inValidArr.length, errorMessage);
};

const validDataValidation = (value, meta) => {
  const error = validationConstants.errorMessages.inValidObj[meta.form_type];
  return validationResultObject(value !== 'invalid', error);
};

// Keys related validation conditions
const checkMobileNumber = (value) => regexValidation('mobileNumber', value);

const checkPan = (value) => regexValidation('pan', value);

const checkPersonalPan = (value) => regexValidation('personalPan', value);

const checkBusinessPan = (value) => regexValidation('businessPan', value);

const checkEmail = (value) => regexValidation('email', value);

const checkAadhar = (value) => regexValidation('aadhar', value);

const checkPinCode = (value) => regexValidation('pin-code', value);

const checkIfscCode = (value) => regexValidation('ifsc-code', value);

// Deprecated Validator. Refer to checkExpiryDate validator
const checkDOB = (value, meta) => {
  const age = value && common.calculateAge(value);
  const minAge = meta.minAge != undefined ? meta.minAge : 20;
  const maxAge = meta.maxAge || 60;
  const isAgeValid = (age >= minAge && age < maxAge) || (age === maxAge && common.isBirthday(value));
  let error = validationConstants.getErrorMessage('dob');
  error = (error.replace('20', minAge)).replace('60', maxAge);
  return validationResultObject(isAgeValid, error);
};

const checkTin = (value) => regexValidation('business-tin', value);

const checkOtp = (value) => regexValidation('otp', value);

const checkMpin = (value) => regexValidation('mpin', value);

const checkNationalId = (value) => regexValidation('nationalId', value);

const checkPercentage = (value) => {
  const isValid = value >= 0 && value <= 100 && typeof (parseFloat(value)) === 'number';
  const error = validationConstants.getErrorMessage('percentage');
  return validationResultObject(isValid, error);
};

const customSecurityInvalidate = (value, meta) => validationResultObject(false, meta.invalidationMessage);

const checkLength = (value, meta) => {
  const length = value.toString().length;
  let errorMessage,
    inValidArr = [];
  if (meta.minLength && length < meta.minLength) {
    inValidArr.push('min');
  }
  if (meta.maxLength && length > meta.maxLength) {
    inValidArr.push('max');
  }

  if (inValidArr.length === 2) {
    errorMessage = `Characters length must be between ${meta.minLength} and ${meta.maxLength}`;
  } else if (inValidArr.indexOf('min') !== -1) {
    errorMessage = `Characters length must be more than ${meta.minLength}.`;
  } else if (inValidArr.indexOf('max') !== -1) {
    errorMessage = `Characters length must be less than ${meta.maxLength}.`;
  }

  return validationResultObject(!inValidArr.length, errorMessage);
};

const checkNationalIdUploadVerify = (value, meta) => {
  // let inValidValArray = [1, 2, 3, false];
  const isValid = value !== '' && typeof (value) === 'string';
  const error = validationConstants.getErrorMessage('nationalIdUploadVerify');
  return validationResultObject(isValid, error);
};

const checkConfirm = (value, meta, formData) => {
  const isValid = (value === formData[meta.confirmField]);
  const error = validationConstants.getErrorMessage('mismatchValue');
  return validationResultObject(isValid, error);
};

const checkExpiryDate = (value, meta) => {
  const minimumDate = common.changeDateFormat(common.getExpiryDate(meta.maxDate));
  const maximumDate = common.changeDateFormat(common.getExpiryDate(meta.minDate));
  let error = '';
  let isValid = true;
  if (value) {
    if (minimumDate && maximumDate) {
      isValid = common.isDateBetween(value, minimumDate, maximumDate);
      error = strings('errors.dateBetween', { min: minimumDate, max: maximumDate });
    } else if (minimumDate) {
      isValid = common.compareDates(value, minimumDate);
      error = strings('errors.dateAfter', { min: minimumDate });
    } else if (maximumDate) {
      error = strings('errors.dateBefore', { max: maximumDate });
      isValid = common.compareDates(maximumDate, value);
    }
  }
  return validationResultObject(isValid, error);
};

export default {
  isRequired: requiredValidation,
  isMinMax: minMaxValidation,
  length: checkLength,
  isValidData: validDataValidation,
  mobileNumber: checkMobileNumber,
  mobile: checkMobileNumber,
  pan: checkPan,
  personalPan: checkPersonalPan,
  businessPan: checkBusinessPan,
  email: checkEmail,
  dob: checkDOB,
  aadhar: checkAadhar,
  'pin-code': checkPinCode,
  'ifsc-code': checkIfscCode,
  'business-tin': checkTin,
  otp: checkOtp,
  mpin: checkMpin,
  nationalId: checkNationalId,
  nationalIdUploadVerify: checkNationalIdUploadVerify,
  confirm: checkConfirm,
  percentage: checkPercentage,
  date: checkExpiryDate,
  customSecurityInvalidate: customSecurityInvalidate,

  // generic functions exports
  checkRegexValidation: checkRegexValidation,
  validationResultObject: validationResultObject,
};
