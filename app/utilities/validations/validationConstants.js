/*

This file contains all the constants information related to Validations:

Type of constants (as of now):
  Regex


These are constants which are supposedly does not import any other dependency from the code
*/
import { strings } from '../../../locales/i18n';


const regexList = {
  textOnly: /^([^0-9.,]*)$/,
  pan: /^[A-Za-z]{3}[P|H]{1}[A-Za-z]{1}\d{4}[A-Za-z]{1}$/,
  personalPan: /^[A-Z]{3}[P|H]{1}[A-Z]{1}\d{4}[A-Z]{1}$/,
  businessPan: /^[A-Z]{5}\d{4}[A-Z]{1}$/,
  aadhar: /^\d{12}$/,
  name: /^[A-Za-z ]+$/,
  mobileNumber: /^[6789]\d{9}$/,
  'pin-code': /^[1-9][0-9]{5}$/,
  'business-tin': /^\d{11}$/,
  'ifsc-code': /^[a-zA-Z]{4}[0][a-zA-Z0-9]{6}$/,
  email: /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
  otp: /^\d{6}$/,
  mpin: /^\d{4}$/,
  nationalId: /^(?=[0-9]*$)(?:.{9}|.{12})$/,
  percentage: /^100$|^\d{0,2}(\.\d{1,2})? *%?$/,
}

const errorMessages = {

  isRequired: strings('errors.required'),
  isValidAutoComplete: strings('errors.option'),
  inValidObj: {
    upload: strings('errors.document'),
    autocomplete: strings('errors.option'),
    elasticAutocomplete: strings('errors.option'),
  },
  pan: 'Pan Number is invalid',
  personalPan: 'Pan Number is invalid',
  businessPan: 'Pan Number is invalid',
  mobileNumber: strings('errors.mobile'),
  email: strings('errors.email'),
  'pin-code': 'Invalid Pincode',
  dob: 'Age should be between 19 and 60',
  'business-tin': 'Invalid Business TIN Number',
  'ifsc-code': 'Invalid IFSC code',
  aadhar: 'Invalid Aadhar number',
  otp: 'Invalid OTP',
  mpin: strings('errors.mpin'),
  nationalId: 'Invalid National ID',
  mismatchValue: 'Value Mismatch',
  percentage: 'Percentage must be between 0% and 100%.',
};

getErrorMessage = (key) => {
  const dummyErrors = {
    isRequired: strings('errors.required'),
    isValidAutoComplete: strings('errors.option'),
    inValidObj: {
      upload: strings('errors.document'),
      autocomplete: strings('errors.option'),
      elasticAutocomplete: strings('errors.option'),
    },
    pan: 'Pan Number is invalid',
    personalPan: 'Pan Number is invalid',
    businessPan: 'Pan Number is invalid',
    mobileNumber: strings('errors.mobile'),
    email: strings('errors.email'),
    'pin-code': 'Invalid Pincode',
    dob: strings('errors.age'),
    'business-tin': 'Invalid Business TIN Number',
    'ifsc-code': 'Invalid IFSC code',
    aadhar: 'Invalid Aadhar number',
    otp: 'Invalid OTP',
    mpin: strings('errors.mpin'),
    nationalId: strings('errors.nationalId'),
    nationalIdUploadVerify: strings('errors.required'),
    mismatchValue: 'Value Mismatch',
    percentage: 'Percentage must be between 0% and 100%.',
  };
  return dummyErrors[key];
};

export default {
  regexList,
  errorMessages,
  getErrorMessage,
};
