/*
  This file contains the common utility functions across the application
*/

import { appConst } from '../configs/appConfig';
import currencyFormatter from 'currency-formatter';
import validationConstants from './validations/validationConstants';
import moment from 'moment';
import sha256 from 'sha256';
import 'intl';
import 'intl/locale-data/jsonp/en-IN';
import { strings } from '../../locales/i18n';
import expr from 'expr-eval';

export const currencySeparators = {
  separator: '',
  fractionSeparator: '',
};

export const setCurrencySeparators = () => {
  const sampleCurrency = new Intl.NumberFormat(appConst.localCurrencyString, { minimumFractionDigits: 0 }).format(1234.1);
  currencySeparators.separator = sampleCurrency[1];
  currencySeparators.fractionSeparator = sampleCurrency[sampleCurrency.length - 2];
};
setCurrencySeparators();

const mimeMap = {
  pdf: 'application/pdf',
  png: 'image/png',
};

export const formatDateTime = (dateTime) => {
  const formatedDateTime = moment(dateTime).format('YYYY-MM-DD HH:mm:ss');
  return formatedDateTime;
};

export const formatDefaultDate = (date) => {
  const monthNames = [
    'Jan', 'Feb', 'Mar',
    'Apr', 'May', 'Jun', 'Jul',
    'Aug', 'Sep', 'Oct',
    'Nov', 'Dec',
  ];

  appendIndex = (index) => index < 10 ? `0${index}` : index;

  const day = appendIndex(date.getDate());
  const monthIndex = appendIndex(date.getMonth() + 1);
  const year = date.getFullYear();

  return `${day}-${monthIndex}-${year}`;
};

export const unformatDateStr = (dateStr) => {
  const tempDateArr = dateStr.split('-');
  return `${tempDateArr[1]}/${tempDateArr[0]}/${tempDateArr[2]}`;
};

export const formatDateToDisplay = (dateStr) => {
  const date = new Date(dateStr);
  const monthNames = [
    'Jan', 'Feb', 'Mar',
    'Apr', 'May', 'Jun', 'Jul',
    'Aug', 'Sep', 'Oct',
    'Nov', 'Dec',
  ];

  const appendIndex = (index) => index < 10 ? `0${index}` : index;

  const day = appendIndex(date.getDate());
  const month = monthNames[date.getMonth()];
  const year = date.getFullYear();

  return `${day} ${month} ${year}`;
};

export const calculateAge = (dateStr) => {
  const tempDate = unformatDateStr(dateStr);
  let birthDate = new Date(tempDate),
    today = new Date();

  return moment().diff(moment(tempDate), 'years');
};

export const isBirthday = (dateStr) => {
  const tempDate = unformatDateStr(dateStr);
  const birthDate = new Date(tempDate);
  const today = new Date();
  return birthDate.getDate() === today.getDate() && birthDate.getMonth() === today.getMonth();
};

export const getDateFromAge = (age) => {
  const alterMonths = [1, 3, 5, 7, 8, 10, 12];
  let today = new Date(),
    year = today.getFullYear(),
    day = today.getDate(),
    month = (today.getMonth() + 1);

  let ageYear = year - age,
    ageDate = day - 1,
    ageMonth = (day !== 1) ? month : (month - 1);
  if (ageMonth === 0) {
    ageMonth = 12;
  }
  if (ageDate === 0) {
    if (alterMonths.indexOf(ageMonth) !== -1) {
      ageDate = 31;
    } else {
      ageDate = 30;
    }
  }
  return new Date(`${ageMonth}/${ageDate}/${ageYear}`);
};

export const convertCurrencyFormat = (value, removeSymbol = false, disablePrecision) => {
  if (value === '') {
    return '';
  }
  const number = Number(value);
  const formatObj = {
    locale: appConst.localCurrencyString,
    // symbol: strings('currency.symbol'),
    precision: disablePrecision ? 0 : appConst.currencyPrecision,
    // format: removeSymbol && '%v',
  };

  if (!isNaN(number)) {
    return currencyFormatter.format(number, formatObj);
    // return number.toLocaleString(appConst.localCurrencyString);
  }
  return undefined;
};

export const unformatCurrencyFormat = (value) => {
  const number = currencyFormatter.unformat(value, {
    locale: appConst.localCurrencyString,
  });
  if (!value.replace(/[^0-9.,-]+/g, '')) {
    return '';
  }
  return Number(number);
};

export const convertCurrency = (value, removeSymbol = false) => {
  const { fractionSeparator } = currencySeparators;
  if (isNaN(value) || value === '' || (typeof value === 'string' && value.trim() === '')) {
    return '';
  }
  const currencyOption = !removeSymbol ? {
    style: 'currency',
    currency: appConst.localeCurrencyType,
  } : {};
  let currencyString = new Intl.NumberFormat(appConst.localCurrencyString, { ...currencyOption, minimumFractionDigits: 0 }).format(value);
  const valueStr = value && value.toString();
  if (valueStr && valueStr.indexOf(fractionSeparator) === value.length - 1) {
    currencyString += fractionSeparator;
  }
  return currencyString;
};

// export const revertCurrency = (value) => Number(value.replace(/[^0-9.-]+/g, ''));
export const revertCurrency = (value, removeFractionSeparator = false) => {
  const { separator, fractionSeparator } = currencySeparators;
  const currencyRegex = new RegExp(`${separator}|${fractionSeparator}|[0-9]`, 'g');
  let valueClone = value;
  if (typeof value === 'number') {
    valueClone = valueClone.toString();
  }
  let fractionCounter = 0;
  return valueClone.replace(currencyRegex, (match) => {
    if (match === fractionSeparator && !fractionCounter) {
      fractionCounter += 1;
      if (removeFractionSeparator && valueClone.indexOf(fractionSeparator) === valueClone.length - 1) {
        return '';
      }
      return '.';
    } else if (match && !isNaN(match)) {
      return match;
    }
    return '';
  });
};

export const debounce = (func, wait, immediate) => {
  let timeout;
  return () => {
    const context = this,
      args = arguments;
    console.log('arguments: ', arguments);
    const later = function () {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };
    const callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) func.apply(context, args);
  };
};

export const restrictInput = (input, type = 'textOnly') => {
  const regex = new RegExp(validationConstants.regexList[type]);
  return regex.test(input);
};

export const getMimetype = (type) => type && mimeMap[type];

export const getShortCurrency = (number) => {
  const str = convertNumber(number);
  const currencyString = convertCurrencyFormat(0, false, true);
  return currencyString.replace('0', str);
};

export const convertNumber = (number) => {
  if (!number) {
    return 0;
  }
  if (appConst.localCurrencyString === 'en-IN') {
    if (number >= 10000000) {
      return `${parseFloat(number / 10000000).toFixed(2)} crores`;
    } else if (number >= 100000) {
      return `${parseFloat(number / 100000).toFixed(2)} lakhs`;
    } else if (number >= 1000) {
      return `${parseFloat(number / 1000).toFixed(2)} thousand`;
    }
  } else {
    if (number >= 1000000000) {
      return `${parseFloat(number / 1000000000).toFixed(2)} billion`;
    } else if (number >= 1000000) {
      return `${parseFloat(number / 1000000).toFixed(2)} million`;
    } else if (number >= 1000) {
      return `${parseFloat(number / 1000).toFixed(2)} thousand`;
    }
  }

  return number;
};

export const isJSON = (str) => {
  try {
    return (JSON.parse(str) && !!str);
  } catch (e) {
    return false;
  }
};

export const isValidDate = (d) => {
  const dateObj = new Date(d);
  return dateObj instanceof Date && !isNaN(dateObj);
};

export const encryptSHA = (str) => sha256(str);

export const isJson = (str) => {
  try {
    JSON.parse(str);
  } catch (e) {
    return false;
  }
  return true;
};

const frac = (f) => f % 1;

const convert_number = (number) => {
  if ((number < 0)) {
    return 'Zero';
  }
  const Gn = Math.floor(number / 10000000);  /* Crore */
  number -= Gn * 10000000;
  const kn = Math.floor(number / 100000);     /* lakhs */
  number -= kn * 100000;
  const Hn = Math.floor(number / 1000);      /* thousand */
  number -= Hn * 1000;
  var Dn = Math.floor(number / 100);       /* Tens (deca) */
  number = number % 100;               /* Ones */
  var tn = Math.floor(number / 10);
  var one = Math.floor(number % 10);
  var res = '';

  if (Gn > 0) {
    res += (convert_number(Gn) + ' CRORE');
  }
  if (kn > 0) {
    res += (((res === '') ? '' : ' ') + convert_number(kn) + ' LAKH');
  }
  if (Hn > 0) {
    res += (((res === '') ? '' : ' ') + convert_number(Hn) + ' THOUSAND');
  }

  if (Dn) {
    res += (((res === '') ? '' : ' ') + convert_number(Dn) + ' HUNDRED');
  }


  var ones = Array('', 'ONE', 'TWO', 'THREE', 'FOUR', 'FIVE', 'SIX', 'SEVEN', 'EIGHT', 'NINE', 'TEN', 'ELEVEN', 'TWELVE', 'THIRTEEN', 'FOURTEEN', 'FIFTEEN', 'SIXTEEN', 'SEVENTEEN', 'EIGHTEEN', 'NINETEEN');
  var tens = Array('', '', 'TWENTY', 'THIRTY', 'FOURTY', 'FIFTY', 'SIXTY', 'SEVENTY', 'EIGHTY', 'NINETY');

  if (tn > 0 || one > 0) {
    if (!(res === '')) {
      res += ' AND ';
    }
    if (tn < 2) {
      res += ones[tn * 10 + one];
    } else {
      res += tens[tn];
      if (one > 0) {
        res += (`-${ones[one]}`);
      }
    }
  }

  if (res == '') {
    res = 'zero';
  }
  return res;
};

export const number2text = (value) => {
  var fraction = Math.round(frac(value)*100);
  var f_text = '';

  if (fraction > 0) {
    f_text = 'AND '+convert_number(fraction)+' PAISE';
  }

  return convert_number(value)+' RUPEE '+f_text+' ONLY';
};

export const getExpiryDate = (date) => {
  if (date) {
    const regex = /^([+-])\/(.*?)days\/(.*?)months\/(.*?)years$/g;
    try {
      const [, ordinal, days, months, years] = regex.exec(date);
      const daysToAdd = parseInt(days, 10) + (parseInt(months, 10) * 30) + (parseInt(years, 10) * 365);
      let dateVal = '';
      if (ordinal === '+') {
        dateVal = moment().add(daysToAdd, 'days').toDate();
      } else {
        dateVal = moment().subtract(daysToAdd, 'days').toDate();
      }
      return dateVal;
    } catch (error) {
      if (error.name === 'TypeError') {
        console.log('Invalid Date validation format. Should be P/Xdays/Ymonths/Zyears');
      } else {
        console.log('error: ', error);
      }
    }
  }
  return false;
};

export const changeDateFormat = (date) => {
  if (date) {
    moment(date).format('DD-MM-YYYY');
  }
};

export const isDateBetween = (date, minDate, maxDate) => {
  moment(date).isBetween(minDate, maxDate, null, []);
};

// compares if a date > b date
export const compareDates = (a, b) => moment(a) > moment(b);


// Eval exrpression with an provider object
export const parseEval = (expression, obj) => {
  let Parser = expr.Parser;
  return Parser.evaluate(expression, obj);
}
