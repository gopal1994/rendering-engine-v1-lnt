import I18n from '../../locales/i18n';
import * as common from './commonUtils';

import moment from 'moment';
import 'moment/locale/vi';


export const getAllLanguages = () => Object.keys(I18n.translations);

export const fallbackLanguage = 'en-US';

export const changeLanguage = (language, callback) => {
  // ["en-US", "vi-VN"]
  const allLocales = getAllLanguages();
  if (allLocales.includes(language)) {
    I18n.locale = language;
    if (language === 'vi-VN') {
      moment.locale('vi');
    } else {
      moment.locale('en');
    }
    callback(true);
  } else {
    I18n.locale = fallbackLanguage;
    callback(true);
    // console.error(`Localization ${language} not found in locales`);
  }
};

export const getRestProps = (obj) => {
  if (common.isJSON(obj)) {
    const langObj = JSON.parse(obj);
    const selectedLanguage = currentLocale();
    if (langObj.lang) {
      const { lang, ...restProps } = langObj;
      if (restProps) {
        return restProps;
      }
    }
    return null;
  }
};

export const getTranslation = (obj) => {
  if (common.isJSON(obj) || typeof obj === 'object') {
    const langObj = common.isJSON(obj) ? JSON.parse(obj) : obj;
    const selectedLanguage = currentLocale();
    if (langObj.lang) {
      const { lang, ...restProps } = langObj;
      if (typeof (langObj.lang[selectedLanguage]) === 'object') {
        return Object.assign({}, restProps, langObj.lang[selectedLanguage]);
      }
      return langObj.lang[selectedLanguage];
    }
    return langObj;
  }

  return obj;
};

export const currentLocale = () => I18n.currentLocale();

moment.updateLocale('vi', {
  meridiem: {
    am: 'am',
    AM: 'AM',
    pm: 'pm',
    PM: 'PM',
  },
});

