export const populateOptionsCallback = (response, meta) => {
  let optionObjs = response.response.values,
    // This optionMap contains the index of values to be displayed for "name" of the option
    // && the identifier key for "id" of the option
    optionMap = meta.api && meta.api['options-mapping'];
  const options = [];
  const dependentFields = meta.api.dependentFields ? meta.api.dependentFields : {};
  optionObjs.forEach((val) => {
    const optionObj = {
      id: typeof(optionMap.id) === 'string' && optionMap.id === 'id' ? val[optionMap.id] : val.values[parseInt(optionMap['id'])].toString(),
      name: val.values[parseInt(optionMap['name-index'])].toString(),
      dependentFields: {},
    };
    Object.keys(dependentFields).forEach((field) => {
      optionObj.dependentFields[field] = val.values[dependentFields[field]] ? val.values[dependentFields[field]] : '';
    });
    options.push(optionObj);
  });

  return options;
};

export const onChangeHandler = (self, value, dependentFields) => {
  self.setState({ inputVal: value, showList: true });
  if (!self.props.compProps.allowKeyInput && value) {
    self.props.onChangeHandler('invalid', self.props.elmId);
  } else {
    self.props.onChangeHandler(value, self.props.elmId);
  }
  window.setTimeout(() => {
    if (dependentFields) {
      Object.keys(dependentFields).forEach((field) => {
        if (self.props.formData && self.props.formData[field] !== '') {
          self.props.updateFormData(field, '');
        }
      });
    }
  });
};

export const updateOption = (self, option) => {
  self.setState({ inputVal: option.name, showList: false });
  self.props.onChangeHandler(option.id, self.props.elmId);
  window.setTimeout(() => { self.populateDependantFields(option); });
  self.props.toggleScroll && self.props.toggleScroll({ scrollEnabled: true });
};

export const populateDependantFields = (self, option) => {
  Object.keys(option.dependentFields).forEach((field) => {
    option.dependentFields[field] && self.props.updateFormData(field, option.dependentFields[field]);
  });
};

export const increaseHeight = (isListVisible, listHeight) => {
  const modifiedStyle = {};
  if (isListVisible) {
    modifiedStyle.height = (listHeight + 50);
  }
  return modifiedStyle;
};
