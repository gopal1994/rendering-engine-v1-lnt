import dottie from 'dottie';

export const _restSuccessCallback = (response, api) => {
  let data = response,
    self = this;
  const map = api && api.mapping;

  function _extractSubProps(propStr) {
    return propStr.trim().split('.');
  }

  const deltaFormObject = {};

  if (map) {
    Object.keys(map).forEach((key) => {
      // let responsePropsArray = _extractSubProps(map[key]), // returns an array of series nested props
      //   value = { ...data };
      //     // loop through the props Array to get the right nested value
      // while (responsePropsArray.length >= 1) {
      //   value = value[responsePropsArray.shift()];
      // }
      value = dottie.get(data, map[key]);
      deltaFormObject[key] = value;
      // window.setTimeout(() => {self.props.updateFormData(key, value)}, 0);
    });
  }
  return deltaFormObject;
}

export const getNewMastersOptions = (response, api) => {
  let optionObjs = response.data;
  let optionMap = api && api['options-mapping'];
  const dependentFields = api.dependentFields ? api.dependentFields : {};

  const options = [];

  Array.isArray(optionObjs) && optionObjs.forEach((val) => {
    const optionObj = {
      id: val[optionMap.id],
      name: val[optionMap['name-index']],
      dependentFields: {}
    }

    Object.keys(dependentFields).forEach((field) => {
      optionObj.dependentFields[field] = val[dependentFields[field]] ? val[dependentFields[field]] : '';
    });

    options.push(optionObj);
  })

  return options;

}

export const getOldMasterOptions = (response, api) => {
  let optionObjs = response.response && response.response.values,
    // This optionMap contains the index of values to be displayed for "name" of the option
    // && the identifier key for "id" of the option
    optionMap = api && api['options-mapping'];
  const options = [];
  const dependentFields = api.dependentFields ? api.dependentFields : {};
  Array.isArray(optionObjs) && optionObjs.forEach((val) => {
    const optionObj = {
      id: typeof(optionMap.id) === 'string' && optionMap.id === 'id' ? val[optionMap.id] : val.values[parseInt(optionMap['id'])],
      name: val.values[parseInt(optionMap['name-index'])],
      dependentFields: {},
    };
    Object.keys(dependentFields).forEach((field) => {
      optionObj.dependentFields[field] = val.values[dependentFields[field]] ? val.values[dependentFields[field]] : '';
    });
    options.push(optionObj);
  });

  return options;
}



export const populateOptionsCallback = (response, api) => {
  let options = [];

  if (api.apiSource === 'newMasters') {
    options = getNewMastersOptions(response, api);
  } else {
    options = getOldMasterOptions(response, api);
  }
  // Setting up the unique options in dropdown
  // Filter it on the unique ids
  const idMap = {};
  options.forEach((option) => {
    idMap[option.id] = option;
  });
  let optionsL = Object.keys(idMap).map((id) => idMap[id]);

  return optionsL;
}


export const buildValueObject = (data, formData, userDetails) => {
  let payloadSample = data,
      payloadData = {};
  let formDataKeys = Object.keys(formData);

  Object.keys(payloadSample).forEach((key) => {
    if (typeof payloadSample[key] === 'object') {
      payloadData[key] = buildValueObject(payloadSample[key], formData, userDetails);
    } else {
      if (formDataKeys.indexOf(payloadSample[key]) !== -1) {
        const formDataValue = formData[payloadSample[key]];
        payloadData[key] = formDataValue || '';
      }
      else if (key === 'process_instance_id' || key === 'processInstanceId') {
        payloadData[key] = userDetails && userDetails.processInstanceId;
      } else {
        // In this case, pass the hard-coded value provided in meta.api.data itself
        payloadData[key] = payloadSample[key];
      }
    }
  });

  return payloadData;
}

export const buildNewMastersPayload = (data, formData, userDetails) => {
  let payloadSample = data,
      payloadData = Array.isArray(data) ? [] : {};
  let formDataKeys = Object.keys(formData);
  /*
    Expected payload for the masters API (for filtering) is supposed to be in the below format
    {
      "keyList": [
          {
              "isCaseSensitive": false,
              "key": "city",
              "value": "'%ch%'",
              "isLimit": true,
              "range": 5,
              "comparator": "like",
              "operator": ""
          }
      ]
    }
  */

  Object.keys(payloadSample).forEach((key) => {
    if (typeof payloadSample[key] === 'object') {
      payloadData[key] = buildNewMastersPayload(payloadSample[key], formData, userDetails);
    } else {
      if (formDataKeys.indexOf(payloadSample[key]) !== -1) {
        const formDataValue = formData[payloadSample[key]];
        if (key === 'value') {
          if (payloadSample['comparator'] && payloadSample['comparator'] === '=') {
            // In case of '=' comparator the value is supposed to go as it is
            payloadData[key] = formDataValue ? "'" + formDataValue + "'" : "'" + "'";
          } else {
            // In case of "like" comparator
            // the backend will do a pattern check with the value provided inside '%%' 
            payloadData[key] = formDataValue ? "'%" + formDataValue + "%'" : "'%" + "%'";
          }
        } else {
          payloadData[key] = formDataValue || '';
        }
      }
      else if (key === 'process_instance_id' || key === 'processInstanceId') {
        payloadData[key] = userDetails && userDetails.processInstanceId;
      } else {
        // In this case, pass the hard-coded value provided in meta.api.data itself
        payloadData[key] = payloadSample[key];
      }
    }
  });

  return payloadData;
}

export const _buildPayloadData = (api, formData, userDetails) => {
  if (api) {
    let payloadSample = api.data,
        payloadData = {};
    let formDataKeys = Object.keys(formData);

    if (api.apiSource === 'newMasters') {
      payloadData = buildNewMastersPayload(payloadSample, formData, userDetails);
    } else {
      payloadData = buildValueObject(payloadSample, formData, userDetails);
    }
    return payloadData;
  }

  return undefined;
}


export const findPayloadFormDataKeys = (data, formData) => {
  let formDataKeys = Object.keys(formData);
  const keys = [];
  Object.keys(data).forEach((key) => {
    if (typeof data[key] === 'object') {
      let childrenFormDataKeys = findPayloadFormDataKeys(data[key], formData);
      Array.prototype.push.apply(keys, childrenFormDataKeys);
    } else {
      // If the payload is dependent on some other form key data
      if (formDataKeys.indexOf(data[key]) !== -1) {
        keys.push(data[key]);
      }
    }
  });

  return [...keys];
}