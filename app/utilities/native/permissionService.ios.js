import { NativeModules } from 'react-native';
import { permissionConst as constants } from '../../configs/appConstants';

const permissionsMap = {

};

export const getPermission = async (permissions) => {
  const permissionResult = constants.GRANTED;
  try {
    return permissionResult;
  } catch (err) {
    console.warn(err);
  }
};
