import { consoleConfig } from 'js-console-logger';
import store from '../utils/store';
import { dumbLog } from '../containers/AppDetails/actions';

/**
 * console.reportErrorsAsExceptions to configure red error box in react native
 * https://stackoverflow.com/questions/44774247/how-to-disable-red-screen-in-debug-mode
 * https://github.com/facebook/react-native/blob/1e8f3b11027fe0a7514b4fc97d0798d3c64bc895/Libraries/Core/ExceptionsManager.js#L97
*/

console.reportErrorsAsExceptions = false;  // eslint-disable-line no-console

const reportFn = (obj) => {
  store.dispatch(dumbLog(obj));
};

consoleConfig.setReportFn(reportFn);

consoleConfig.setDisableReport(__DEV__); // eslint-disable-line no-undef
