/**
 * app.js
 *
 * This is the entry file for the application, only setup and boilerplate
 * code.
 */

// Needed for redux-saga es6 generator support
if (!window._babelPolyfill) {
  require('babel-polyfill');
}

// Import all the third party stuff
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'react-router-redux';
import FontFaceObserver from 'fontfaceobserver';
import createHistory from 'history/createBrowserHistory';
import { userConfig } from 'configs/appConfig';

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

// Import root app
import App from 'containers/App';

// Import Language Provider
import LanguageProvider from 'containers/LanguageProvider';

// Load the favicon, the manifest.json file and the .htaccess file
/* eslint-disable import/no-webpack-loader-syntax */
import '!file-loader?name=[name].[ext]!./images/favicon.ico';
import '!file-loader?name=[name].[ext]!./images/icon-72x72.png';
import '!file-loader?name=[name].[ext]!./images/icon-96x96.png';
import '!file-loader?name=[name].[ext]!./images/icon-120x120.png';
import '!file-loader?name=[name].[ext]!./images/icon-128x128.png';
import '!file-loader?name=[name].[ext]!./images/icon-144x144.png';
import '!file-loader?name=[name].[ext]!./images/icon-152x152.png';
import '!file-loader?name=[name].[ext]!./images/icon-167x167.png';
import '!file-loader?name=[name].[ext]!./images/icon-180x180.png';
import '!file-loader?name=[name].[ext]!./images/icon-192x192.png';
import '!file-loader?name=[name].[ext]!./images/icon-384x384.png';
import '!file-loader?name=[name].[ext]!./images/icon-512x512.png';
import '!file-loader?name=[name].[ext]!./manifest.json';
import 'file-loader?name=[name].[ext]!./.htaccess'; // eslint-disable-line import/extensions
/* eslint-enable import/no-webpack-loader-syntax */

import configureStore from './configureStore';

// Import i18n messages
import { translationMessages } from './i18n';

// Import CSS reset and Global Styles
import './global-styles';
import materialUiTheme from './configs/materialUiTheme';
import getMuiTheme from 'material-ui/styles/getMuiTheme';

// Observe loading of Open Sans (to remove open sans, remove the <link> tag in
// the index.html file and this observer)
const openSansObserver = new FontFaceObserver('Open Sans', {});

// When Open Sans is loaded, add a font-family using Open Sans to the body
openSansObserver.load().then(() => {
  document.body.classList.add('fontLoaded');
}, () => {
  document.body.classList.remove('fontLoaded');
});

// Create redux store with history
const initialState = {};
// const baseObj = !userConfig.runAsPlugin ? {} : { basename: '/build' };
const history = createHistory();
// const store = configureStore(initialState, history);
const MOUNT_NODE = document.getElementById('app');

const render = (containerID, userId, pid, token, formRenderingJson, journeyName, baseUrl) => {
  const MOUNT_NODE_L = document.getElementById(containerID);
  const store = configureStore(initialState, history);
  ReactDOM.render(
    <MuiThemeProvider muiTheme={getMuiTheme(materialUiTheme)}>
      <Provider store={store}>
        <LanguageProvider messages={translationMessages}>
          <ConnectedRouter history={history}>
            <App processInstanceId={pid} bpm_token={token} userId={userId} formRenderingJson={formRenderingJson} journeyName={journeyName} baseUrl={baseUrl}/>
          </ConnectedRouter>
        </LanguageProvider>
      </Provider>
    </MuiThemeProvider>,
    MOUNT_NODE_L
  );
};


// Install ServiceWorker and AppCache in the end since
// it's not most important operation and if main code fails,
// we do not want it installed
if (process.env.NODE_ENV === 'production') {
  require('offline-plugin/runtime').install(); // eslint-disable-line global-require
}

if (!userConfig.runAsPlugin) {
  render('app');
}

// Below two function are to be used if Journey is being plugged in somewhere else
// And we need to render the journey inside a container of other portals/webistes
const renderJourneyInContainer = (containerId, userId, pid, token, formRenderingJson, journeyName, baseUrl) => {
  // formRenderingJson will be provided just in case of rendering the form preview
  // In Preview case, formRenderingJson will always be in form of response that comes from the 'initiate' request
  // In other case the application will work as it is supposed to (fetching the rendering data from an API defined in plugin build)
  render(containerId, userId, pid, token, formRenderingJson, journeyName, baseUrl);
};

const cancelJourneyInContainer = (containerID) => {
  const MOUNT_NODE_L = document.getElementById(containerID);
  ReactDOM.unmountComponentAtNode(MOUNT_NODE_L);
};

window.renderJourneyIntoComponent = window.renderJourneyIntoComponent ? window.renderJourneyIntoComponent : renderJourneyInContainer;
window.cancelJourneyInContainer = window.cancelJourneyInContainer ? window.cancelJourneyInContainer : cancelJourneyInContainer;
