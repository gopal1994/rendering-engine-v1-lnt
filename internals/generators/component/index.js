/**
 * Component Generator
 */

/* eslint strict: ["off"] */

'use strict';

const componentExists = require('../utils/componentExists');

module.exports = {
  description: 'Add an unconnected component',
  prompts: [{
    type: 'list',
    name: 'type',
    message: 'Select the type of component',
    default: 'Stateless Function',
    choices: () => ['Stateless Function', 'React.PureComponent', 'React.Component'],
  }, {
    type: 'input',
    name: 'name',
    message: 'What should it be called?',
    default: 'Button',
    validate: (value) => {
      if ((/.+/).test(value)) {
        return componentExists(value) ? 'A component or container with this name already exists' : true;
      }

      return 'The name is required';
    },
  }, {
    type: 'confirm',
    name: 'wantMessages',
    default: true,
    message: 'Do you want i18n messages (i.e. will this component use text)?',
  }, {
    type: 'confirm',
    name: 'wantLoadable',
    default: false,
    message: 'Do you want to load the component asynchronously?',
  }, {
    type: 'confirm',
    name: 'wantNativeComponents',
    default: true,
    message: 'Do you want add Abstract/Native Component?',
  },
  ],
  actions: (data) => {
    // Generate index.js and index.test.js
    let componentTemplate;

    switch (data.type) {
      case 'Stateless Function': {
        componentTemplate = './component/stateless.js.hbs';
        break;
      }
      default: {
        componentTemplate = './component/class.js.hbs';
      }
    }

    const actions = [{
      type: 'add',
      path: '../../app/components/{{properCase name}}/tests/index.test.js',
      templateFile: './component/test.js.hbs',
      abortOnFail: true,
    }];

    if (data.type !== 'Stateless Function' && data.wantNativeComponents) {
      actions.push(
        {
          type: 'add',
          path: '../../app/components/{{properCase name}}/index.js',
          templateFile: './component/class.index.js.hbs',
          abortOnFail: true,
        },
        {
          type: 'add',
          path: '../../app/components/{{properCase name}}/{{properCase name}}Abstract.js',
          templateFile: './component/abstract.js.hbs',
          abortOnFail: true,
        },
        {
          type: 'add',
          path: '../../app/components/{{properCase name}}/{{properCase name}}Component.js',
          templateFile: './component/component.js.hbs',
          abortOnFail: true,
        },
        {
          type: 'add',
          path: '../../app/components/{{properCase name}}/{{properCase name}}Component.native.js',
          templateFile: './component/component.native.js.hbs',
          abortOnFail: true,
        }
      );
    } else {
      actions.push({
        type: 'add',
        path: '../../app/components/{{properCase name}}/index.js',
        templateFile: componentTemplate,
        abortOnFail: true,
      });
    }

    // If they want a i18n messages file
    if (data.wantMessages) {
      actions.push({
        type: 'add',
        path: '../../app/components/{{properCase name}}/messages.js',
        templateFile: './component/messages.js.hbs',
        abortOnFail: true,
      });
    }

    // If want Loadable.js to load the component asynchronously
    if (data.wantLoadable) {
      actions.push({
        type: 'add',
        path: '../../app/components/{{properCase name}}/Loadable.js',
        templateFile: './component/loadable.js.hbs',
        abortOnFail: true,
      });
    }
    return actions;
  },
};
